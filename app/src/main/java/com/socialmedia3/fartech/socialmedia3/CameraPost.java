package com.socialmedia3.fartech.socialmedia3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.otaliastudios.cameraview.CameraListener;
import com.otaliastudios.cameraview.CameraView;
import com.otaliastudios.cameraview.Flash;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class CameraPost extends AppCompatActivity {

    CameraView cameraView ;

    ImageView imgC,imgDisplay;

    Bitmap bitmap;

    ImageView imgFlash,imgFlashOff,imgFlashAuto,imgTorch;


    String type="capture";

    ProgressBar pp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_post);

        Intent intent = getIntent();

        if(intent.getStringExtra("type") != null){
            type = intent.getStringExtra("type");
        }

        cameraView = findViewById(R.id.camera);

        imgC = findViewById(R.id.img_capture);

        imgFlash = findViewById(R.id.img_flash);
        imgFlashOff = findViewById(R.id.img_flash_off);
        imgFlashAuto = findViewById(R.id.img_flash_auto);
        imgTorch = findViewById(R.id.img_torch);


        pp = findViewById(R.id.progress_circular);

        imgC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraView.setVisibility(View.VISIBLE);
                cameraView.capturePicture();
            }
        });

        imgFlashAuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraView.setFlash(Flash.AUTO);
            }
        });

        imgFlashOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraView.setFlash(Flash.OFF);
            }
        });

        imgFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraView.setFlash(Flash.ON);
            }
        });
        imgTorch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cameraView.setFlash(Flash.TORCH);
                System.out.println("only e    oooooooooo");
            }
        });

        cameraView.addCameraListener(new CameraListener() {
            @Override
            public void onPictureTaken(byte[] picture) {
                System.out.println("only e Bitmap   ");

                CaptureImage captureImage = new CaptureImage();
                captureImage.setByte(picture);
                captureImage.execute();

                // Create a bitmap or a file...
                // CameraUtils will read EXIF orientation for you, in a worker thread.
                //CameraUtils.decodeBitmap(picture);
            }
        });

    }

    class CaptureImage extends AsyncTask<Void,Void,Void> {
        byte[] pic;
        Bitmap bmp;

        public void setByte(byte[] bytes){
            this.pic=bytes;
        }


        @Override
        protected Void doInBackground(Void... voids) {




            Bitmap decoded2;
            decoded2 = compressImage(pic);

            Bitmap bitmapImage = decoded2;
            int nh = (int) ( bitmapImage.getHeight() * (720.0 / bitmapImage.getWidth()) );
            Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 720, nh, true);
            System.out.println("only e step1   "+sizeOf(decoded2)/(1024));

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            scaled.compress(Bitmap.CompressFormat.JPEG, 100, out);

            bmp = scaled;



            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pp.setVisibility(View.VISIBLE);
        }




        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            cameraView.stop();
            cameraView.destroy();

            pp.setVisibility(View.GONE);

            bitmap = bmp;


            float size = bitmap.getByteCount()/(1024*1024);
            System.out.println("only e step2   "+size);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
            byte[] byteArray = stream.toByteArray();



            if(type.equals("capture")){
                Intent intent = new Intent(CameraPost.this,PostDisplay.class);
                intent.putExtra("Bitmap",byteArray);
                intent.putExtra("type","cameraPhoto");
                startActivity(intent);


            }else if(type.equals("reCapture")){
                Intent intent = new Intent();
                intent.putExtra("Bitmap",byteArray);
                intent.putExtra("type","cameraPhoto");
                setResult(98 , intent);
            }

            finish();

        }
    }


    public Bitmap compressImage( final byte[] data ) {


        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;

        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);


        int actualHeight = bmp.getHeight();
        int actualWidth = bmp.getHeight();

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 1280.0f;
        float maxWidth = 720.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];


        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out1);


        return bmp;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        System.out.println(inSampleSize);

        return inSampleSize;
    }

    protected int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        cameraView.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cameraView.destroy();
    }



}
