package com.socialmedia3.fartech.socialmedia3;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder>  {

    private ArrayList<statuseslistitem> profilepicture = new ArrayList<>();


    public static Context mcontext;

    private MyRecyclerViewAdapter.ItemClickListener mClickListener;


    private LayoutInflater mInflater;
    private  AlertDialog.Builder alertDialogBuilder;
    private  LayoutInflater layoutInflater2;



    // data is passed into the constructor
    public MyRecyclerViewAdapter(Context context, ArrayList<statuseslistitem> profilepicture) {
        this.mInflater = LayoutInflater.from(context);
        this.profilepicture = profilepicture;






    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycleview_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }


    // binds the data to the view and textview in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {



        holder.profp.setImageBitmap(profilepicture.get(position).bitmap);




    }



    // total number of rows
    @Override
    public int getItemCount() {
        return profilepicture.size();
    }



    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView profp;


        public ViewHolder(View itemView) {
            super(itemView);
            profp = itemView.findViewById(R.id.circle_friend2);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }


    }




    // convenience method for getting data at click position
//    public Bitmap getItem(int id) {
//        return profilepicture.get(id).bitmap;
//    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    //parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);


    }


}
