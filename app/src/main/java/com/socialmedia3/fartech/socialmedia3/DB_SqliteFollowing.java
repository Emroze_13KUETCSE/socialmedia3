package com.socialmedia3.fartech.socialmedia3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DB_SqliteFollowing extends SQLiteOpenHelper {
    public static final String BDname = "Following.db";

    public DB_SqliteFollowing(Context context) {
        super(context, BDname, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table following (id INTEGER PRIMARY KEY AUTOINCREMENT,uid TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS following");
        onCreate(db);
    }

    public Boolean Inserttoalertlist(String userid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("uid", userid);
        long result = db.insert("following", null, contentValues);

        if (result == -1)
            return false;
        else
            return true;

    }


    public ArrayList getlikelist() {
        ArrayList<String> arraylist = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from following", null);
        rs.moveToFirst();
        while (!rs.isAfterLast()) {

            String postid = rs.getString(rs.getColumnIndex("uid"));


            arraylist.add(postid);
            rs.moveToNext();
        }
        return arraylist;
    }

    public boolean updateData (String id, String userid ) {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("uid",userid);



        db.update("mytable",contentValues,"id= ?",new  String[]{id});

        return true;
    }

    public int get_check_List_Favorite(String userid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from following Where uid Like'" + userid + "'", null);
        int count = rs.getCount();
        rs.close();
        return count;
    }

    public Integer Delete(String userid) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("following", "uid = ?", new String[]{String.valueOf(userid)});
    }


    public int getcounts(){
        String counttt="SELECT * FROM "+ "following";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(counttt,null);
        int count=cursor.getCount();
        cursor.close();
        return count;
    }


}

