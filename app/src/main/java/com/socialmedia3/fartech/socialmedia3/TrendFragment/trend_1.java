package com.socialmedia3.fartech.socialmedia3.TrendFragment;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.socialmedia3.fartech.socialmedia3.DB_Post;
import com.socialmedia3.fartech.socialmedia3.PostItem;
import com.socialmedia3.fartech.socialmedia3.PostItem2;
import com.socialmedia3.fartech.socialmedia3.R;
import com.socialmedia3.fartech.socialmedia3.TrendingAdapter2;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link trend_1.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link trend_1#newInstance} factory method to
 * create an instance of this fragment.
 */
public class trend_1 extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    String lang;
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    public ArrayList<PostItem> itemArrayList = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList2 = new ArrayList<>();

    Map<String, PostItem> map = new TreeMap<>();

    String useridd;

    SwipeRefreshLayout mSwipeRefreshLayout;


    GridView gridView;
    int counter = 0;


    TrendingAdapter2 adapter;
    private LinearLayoutManager mLayoutManager;
    public ArrayList<PostItem2> postItem2ArrayList = new ArrayList<>();
    RecyclerView recyclerView;

    JSONArray postJSON,p_JSON;


    DB_Post db_post;

    public trend_1() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment trend_1.
     */
    // TODO: Rename and change types and number of parameters
    public static trend_1 newInstance(String param1, String param2) {
        trend_1 fragment = new trend_1();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    String strText="";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preference = getContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        db_post = new DB_Post(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View vvv = inflater.inflate(R.layout.fragment_trend_1, container, false);


        recyclerView = vvv.findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);

        useridd = preference.getString("uid","");
        lang = preference.getString("lang","");

        if(lang.equals("hindi")){
            strText = "सिनेमा";
        }
        else if(lang.equals("tamil")){
            strText = "சினிமா";
        }
        else if(lang.equals("malayalam")){
            strText = "സിനിമ";
        }
        else if(lang.equals("telugu")){
            strText = "సినిమా";
        }
        else if(lang.equals("kannada")){
            strText = "ಸಿನೆಮಾ";
        }
        else if(lang.equals("bengali")){
            strText = "সিনেমা";
        }

        System.out.println("emmii "+itemArrayList.size());
        String post = db_post.getPost("0");
        try {
            postJSON = new JSONArray(post);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        itemArrayList.clear();
        itemArrayList2.clear();

        System.out.println("emmii "+itemArrayList.size());
        for(int i = 0 ; i < postJSON.length();i++){
            PostItem item = new PostItem(getContext());

            JSONObject object= null;
            try {
                object = postJSON.getJSONObject(i);

                String txt = object.getString("text");
                if(txt.contains(strText)){
                    item.setUsername(object.getString("userName"));
                    item.setUid(object.getString("uid"));
                    item.setPostid(object.getString("postid"));
                    item.setTime(object.getString("time"));
                    item.setImagelocation(object.getString("imgUrl"));
                    item.setVideoUrl(object.getString("videoUrl"));
                    item.setText(object.getString("text"));
                    item.setPpurl(object.getString("ppurl"));
                    item.setDownloadsno(object.getInt("downloads"));
                    item.setLikes(object.getInt("likes"));
                    item.setShares(object.getInt("shares"));
                    item.setCommentsno(object.getInt("comments"));
                    item.setLiked(object.getBoolean("isLiked"));
                    item.setFollowing(object.getBoolean("isFollowing"));

                    itemArrayList.add(item);
                    itemArrayList2.add(item);
                }



            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        System.out.println("emmii "+itemArrayList.size());
        populateCarList();

        return vvv;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void populateCarList(){


        postItem2ArrayList.clear();
        for(int i = 0; i < itemArrayList.size(); i+=3){
            PostItem p1 = null;
            PostItem p2 = null;
            PostItem p3 = null;

            PostItem2 postItem2 = new PostItem2();

            if(i < itemArrayList.size()){
                p1 = itemArrayList.get(i);
                postItem2.setPostItem1(p1);
            }
            if(i+1 < itemArrayList.size()){
                p2 = itemArrayList.get(i+1);
                postItem2.setPostItem2(p2);
            }
            if(i+2 < itemArrayList.size()){
                p3 = itemArrayList.get(i+2);
                postItem2.setPostItem3(p3);
            }

            postItem2ArrayList.add(postItem2);


        }


        adapter = new TrendingAdapter2(getContext(),postItem2ArrayList);
        adapter.setOnCarItemClickListener(listener);
        /*GridLayoutManager mng_layout = new GridLayoutManager(this.getActivity(), 3*//*In your case 4*//*);

        mng_layout.setSpanSizeLookup( new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return 1;
            }
        });

        recyclerView.setLayoutManager(mng_layout);*/
        recyclerView.setAdapter(adapter);


        //gridView.setAdapter(adapter);

    }

    TrendingAdapter2.OnCarItemClickListener listener = new TrendingAdapter2.OnCarItemClickListener() {
        @Override
        public void onImg1Click(int position, PostItem item) {
            System.out.println("emmii   "+position);


        }

        @Override
        public void onImg2Click(int position, PostItem item) {
            System.out.println("emmii   "+position+1);


        }

        @Override
        public void onImg3Click(int position, PostItem item) {
            System.out.println("emmii   "+position+2);


        }
    };
}
