package com.socialmedia3.fartech.socialmedia3;

public class PostItem2 {
    PostItem postItem1=null;
    PostItem postItem2=null;
    PostItem postItem3=null;

    public PostItem2() {
    }

    public PostItem getPostItem1() {
        return postItem1;
    }

    public void setPostItem1(PostItem postItem1) {
        this.postItem1 = postItem1;
    }

    public PostItem getPostItem2() {
        return postItem2;
    }

    public void setPostItem2(PostItem postItem2) {
        this.postItem2 = postItem2;
    }

    public PostItem getPostItem3() {
        return postItem3;
    }

    public void setPostItem3(PostItem postItem3) {
        this.postItem3 = postItem3;
    }

}
