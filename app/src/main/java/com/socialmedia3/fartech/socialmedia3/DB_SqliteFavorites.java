package com.socialmedia3.fartech.socialmedia3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DB_SqliteFavorites extends SQLiteOpenHelper {
    public static final String BDname = "Favorite.db";

    public DB_SqliteFavorites(Context context) {
        super(context, BDname, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table favorite (id INTEGER PRIMARY KEY AUTOINCREMENT,post TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS favorite");
        onCreate(db);
    }

    public Boolean Inserttoalertlist(String postid) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("post", postid);
        long result = db.insert("favorite", null, contentValues);

        if (result == -1)
            return false;
        else
            return true;

    }


    public ArrayList getlikelist() {
        ArrayList<String> arraylist = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from favorite", null);
        rs.moveToFirst();
        while (!rs.isAfterLast()) {

            String postid = rs.getString(rs.getColumnIndex("post"));


            arraylist.add(postid);
            rs.moveToNext();
        }
        return arraylist;
    }

    public boolean updateData (String id, String postid ) {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("post",postid);



        db.update("mytable",contentValues,"id= ?",new  String[]{id});

        return true;
    }

    public int get_check_List_Favorite(String pp) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from favorite Where post Like'" + pp + "'", null);
        int count = rs.getCount();
        rs.close();
        return count;
    }

    public Integer Delete(String pp) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("favorite", "post = ?", new String[]{String.valueOf(pp)});
    }



}

