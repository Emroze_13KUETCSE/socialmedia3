package com.socialmedia3.fartech.socialmedia3;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

public class uploadphotopost extends StringRequest {

    private static final String Send_Data_URL = "http://heyapp.in/postphoto.php";
    private Map<String,String> MapData;

    public uploadphotopost (String username , String uid ,String fileid, String time,String postnumber,String text,String ppurl,String language, Response.Listener<String> listener) {
        super(Method.POST,Send_Data_URL,listener,null);


        MapData=new HashMap<>();
        MapData.put("username",username);
        MapData.put("Uid",uid);
        MapData.put("fileid",fileid);
        MapData.put("time",time);
        MapData.put("postnumber",postnumber);
        MapData.put("text",text);
        MapData.put("ppurl",ppurl);
        MapData.put("language",language);




    }



    @Override
    public Map<String,String> getParams() {
        return MapData;
    }
}