package com.socialmedia3.fartech.socialmedia3;

import android.content.Context;
import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

public class PostItem {
    public Bitmap userphoto = null;
    public Bitmap postphoto=null;
    public String username;
    public String uid;
    public String time;
    public String postid;
    public String text;
    public Integer likes;
    public Integer shares;
    public String imagelocation;
    public String videoUrl;
    public String ppurl;
    public Integer commentsno;
    public Integer downloadsno;
    public Integer views=0;

    boolean isPlaying;

    boolean isMoreShown=true;

    Boolean playWhenReady = false;
    Boolean isPrepared = true;
    int currentWindow = 0;
    long playbackPosition = 0;

    boolean isFollowing=false;
    boolean isLiked=false;

    Context context;

    List<String> word_list=new ArrayList<String>();


    public PostItem(Context context) {
        this.context = context;
        this.postphoto=null;


    }

    public Integer getViews() {
        return views;
    }

    public void setViews(Integer views) {
        this.views = views;
    }

    public List<String> getWord_list() {
        return word_list;
    }

    public void setWord_list(List<String> word_list) {
        this.word_list = word_list;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setFollowing(boolean following) {
        isFollowing = following;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    public boolean isMoreShown() {
        return isMoreShown;
    }

    public void setMoreShown(boolean moreShown) {
        isMoreShown = moreShown;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public Boolean getPrepared() {
        return isPrepared;
    }

    public void setPrepared(Boolean prepared) {
        isPrepared = prepared;
    }

    public Boolean getPlayWhenReady() {
        return playWhenReady;
    }

    public void setPlayWhenReady(Boolean playWhenReady) {
        this.playWhenReady = playWhenReady;
    }

    public int getCurrentWindow() {
        return currentWindow;
    }

    public void setCurrentWindow(int currentWindow) {
        this.currentWindow = currentWindow;
    }

    public long getPlaybackPosition() {
        return playbackPosition;
    }

    public void setPlaybackPosition(long playbackPosition) {
        this.playbackPosition = playbackPosition;
    }


    public Bitmap getUserphoto() {
        return userphoto;
    }

    public void setUserphoto(Bitmap userphoto) {
        this.userphoto = userphoto;
    }

    public Bitmap getPostphoto() {
        return postphoto;
    }

    public void setPostphoto(Bitmap postphoto) {
        this.postphoto = postphoto;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPostid() {
        return postid;
    }

    public void setPostid(String postid) {
        this.postid = postid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getShares() {
        return shares;
    }

    public void setShares(Integer shares) {
        this.shares = shares;
    }

    public String getImagelocation() {
        return imagelocation;
    }

    public void setImagelocation(String imagelocation) {
        this.imagelocation = imagelocation;
    }

    public String getPpurl() {
        return ppurl;
    }

    public void setPpurl(String ppurl) {
        this.ppurl = ppurl;
    }

    public Integer getCommentsno() {
        return commentsno;
    }

    public void setCommentsno(Integer commentsno) {
        this.commentsno = commentsno;
    }

    public Integer getDownloadsno() {
        return downloadsno;
    }

    public void setDownloadsno(Integer downloadsno) {
        this.downloadsno = downloadsno;
    }

    public boolean isPlaying() {
        return isPlaying;
    }

    public void setPlaying(boolean playing) {
        isPlaying = playing;
    }

}
