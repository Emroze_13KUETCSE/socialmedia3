package com.socialmedia3.fartech.socialmedia3;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthProvider;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

public class EditProfileBottomSheet extends BottomSheetDialogFragment {
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    String pppurl="";

    Context context;


    ImageView imageClose;


    View rootView;

    private int lastVisibleItem = 0;
    private int firstY = 0;

    BottomSheetBehavior bottomSheetBehavior;


    EditText etName,etNumber,etCountryCode,etOtp;
    TextView tvNext,tvVerify;

    RelativeLayout rlRoot,rlVerify,rlInfo;
    LinearLayout ll;

    boolean isOpen=false;
    boolean isOpen2=false;


    PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;

    String mVerificationId;
    PhoneAuthProvider.ForceResendingToken mResendToken;

    CircleImageView proPic;

    private static final int SELECT_PHOTO = 100;
    Uri selectedImageURI = null;
    public Bitmap yourSelectedImage;
    EditText etName2;
    String gender="";
    RadioButton radioMale,radioFemale;
    RadioGroup radioGroupGender;


    byte[] byteArrayImage;

    String urll="http://heyapp.in/mediausers.php";

    String accountId = "0023d17fbbe68840000000001"; // Obtained from your B2 account page.
    String applicationKey = "K002OQPFP320nWvEwH+hRuSZerk2jBI"; // Obtained from your B2 account page.
    HttpURLConnection connectionn = null;
    String headerForAuthorizeAccount = "Basic " + Base64.encodeToString((accountId + ":" + applicationKey).getBytes(),Base64.DEFAULT);

    String apiUrl = ""; // Provided by b2_authorize_account
    String accountAuthorizationToken = ""; // Provided by b2_authorize_account
    String bucketId = ""; // The ID of the bucket you want to upload your file to
    String postParams="";
    byte postData[];
    HttpURLConnection connectionuploadurl = null;
    String currenttime;
    String uploadUrl = ""; // Provided by b2_get_upload_url
    String uploadAuthorizationToken = ""; // Provided by b2_get_upload_url
    String fileName = ""; // The name of the file you are uploading
    String contentType = ""; // The content type of the file
    String useridd = ""; // SHA1 of the file you are uploading
    byte[] fileData;
    HttpURLConnection connectionupload = null;
    String json = null;
    String jsonResponse,fileid,gend;

    ProgressBar progressBar;

    private static final String Send_Data_URL = "http://heyapp.in/sendpp.php";


    TextView tvComplete;
    private DisplayImageOptions options;

    String userName="",ppurl="",uid="";


    public static EditProfileBottomSheet getInstance() {
        return new EditProfileBottomSheet();
    }

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {
        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
            if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                if(firstY == 0){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_DRAGGING);
                }else if(firstY == 1){
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                }

            }

            System.out.println("djhfgjhsgfjhgsdjh");
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {

        }
    };

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(final Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.edit_profile, null);
        dialog.setContentView(contentView);

        rootView = contentView;

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent())
                .getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(android.R.color.transparent));


        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        View parent = (View) contentView.getParent();
        parent.setFitsSystemWindows(true);
        bottomSheetBehavior = BottomSheetBehavior.from(parent);
        contentView.measure(0, 0);
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int screenHeight = displaymetrics.heightPixels;
        bottomSheetBehavior.setPeekHeight(screenHeight);

        if (params.getBehavior() instanceof BottomSheetBehavior) {
            ((BottomSheetBehavior)params.getBehavior()).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        params.height = screenHeight-convertDpIntoPx(getContext(),0);

        parent.setLayoutParams(params);

        imageClose = contentView.findViewById(R.id.img_close);

        imageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();


            }
        });

        preference = getContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_profile2)
                .showImageForEmptyUri(R.mipmap.ic_profile2)
                .showImageOnFail(R.mipmap.ic_profile2)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();

        ppurl = preference.getString("ppurl","");
        userName = preference.getString("name","");
        useridd = preference.getString("uid","");

        gender = preference.getString("gender","");




        rlRoot = contentView.findViewById(R.id.rl_root);
        rlInfo = contentView.findViewById(R.id.rl_userInfo);


        proPic = contentView.findViewById(R.id.img_pro_pic);
        etName2 = contentView.findViewById(R.id.et_name2);
        progressBar = contentView.findViewById(R.id.progress_bar);
        radioGroupGender = contentView.findViewById(R.id.radioGroupGender);
        tvComplete = contentView.findViewById(R.id.tv_complete);
        radioMale = contentView.findViewById(R.id.radioMale);
        radioFemale = contentView.findViewById(R.id.radioFemale);


        if(gender.equals("male")){
            radioMale.setChecked(true);
        }else {
            radioFemale.setChecked(true);
        }

        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                if(checkedId == R.id.radioMale){
                    gender = "male";
                }
                else if(checkedId == R.id.radioFemale){
                    gender = "female";
                }
            }
        });



        etName2.setText(userName);

        ImageLoader.getInstance()
                .displayImage(ppurl,proPic, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        //Toast.makeText(context, "Complete", Toast.LENGTH_SHORT).show();
                        proPic.setImageBitmap(loadedImage);
                        //carsViewHolder.mExoPlayer.getLayoutParams().height = carsViewHolder.imgCover.getHeight();
                        //carsViewHolder.mExoPlayer.requestFocus();

                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                    }
                });








        proPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
            }
        });


        tvComplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(getActivity());

                if(yourSelectedImage == null){
                    progressBar.setVisibility(View.VISIBLE);
                    Bitmap decoded = ((BitmapDrawable)proPic.getDrawable()).getBitmap();;

                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    decoded.compress(Bitmap.CompressFormat.JPEG, 80, out);
                    yourSelectedImage = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

                    byteArrayImage = out.toByteArray();

                    fileData = byteArrayImage;

                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
                    String currenttime=simpleDateFormat.format(new Date());

                    fileName=useridd+currenttime+".jpg";
                    contentType="image/jpeg";

                    pppurl="https://static.heyapp.in/file/app-media/"+useridd+currenttime+".jpg";

                    new EditProfileBottomSheet.connecttt().execute();
                }else {
                    progressBar.setVisibility(View.VISIBLE);
                    new EditProfileBottomSheet.connecttt().execute();
                }



            }
        });



    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {
            case SELECT_PHOTO:
                if(resultCode == RESULT_OK){
                    selectedImageURI = data.getData();
                    InputStream imageStream = null;
                    try {
                        imageStream = getContext().getContentResolver().openInputStream(selectedImageURI);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    Bitmap decoded = BitmapFactory.decodeStream(imageStream);

                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    decoded.compress(Bitmap.CompressFormat.JPEG, 50, out);
                    yourSelectedImage = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));

                    proPic.setImageBitmap(yourSelectedImage);

                    byteArrayImage = out.toByteArray();

                    fileData = byteArrayImage;

                    SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyyMMddHHmmss");
                    String currenttime=simpleDateFormat.format(new Date());

                    fileName=useridd+currenttime+".jpg";
                    contentType="image/jpeg";

                    pppurl="https://static.heyapp.in/file/app-media/"+useridd+currenttime+".jpg";

                    //String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);


                }
        }


    }



    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static int convertDpIntoPx(Context mContext, float yourdpmeasure) {
        if (mContext == null) {
            return 0;
        }
        Resources r = mContext.getResources();
        int px = (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, yourdpmeasure, r.getDisplayMetrics());
        return px;
    }


    class connecttt extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new EditProfileBottomSheet.connecttt2().execute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL urll = new URL("https://api.backblazeb2.com/b2api/v2/b2_authorize_account");
                connectionn = (HttpURLConnection)urll.openConnection();
                connectionn.setRequestMethod("GET");
                connectionn.setRequestProperty("Authorization", headerForAuthorizeAccount);
                InputStream in = new BufferedInputStream(connectionn.getInputStream());
                jsonResponse = myInputStreamReader(in);
                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("bucketId")){
                        String[] splitter=s.split("\"");
                        bucketId=splitter[3];
                        postParams = "{\"bucketId\":\"" + bucketId + "\"}";
                        postData= postParams.getBytes(StandardCharsets.UTF_8);


                    }else if (s.contains("apiUrl")){
                        String[] splitter=s.split("\"");
                        apiUrl=splitter[3];
                    }else if (s.contains("authorizationToken")){
                        String[] splitter=s.split("\"");
                        accountAuthorizationToken=splitter[3];
                    }
                }





            } catch (Exception e) {
                e.printStackTrace();


            } finally {
                connectionn.disconnect();
            }

            return null;
        }
    }


    class connecttt2 extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            new EditProfileBottomSheet.connecttt3().execute();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL(apiUrl + "/b2api/v2/b2_get_upload_url");
                connectionuploadurl = (HttpURLConnection)url.openConnection();
                connectionuploadurl.setRequestMethod("POST");
                connectionuploadurl.setRequestProperty("Authorization", accountAuthorizationToken);
                connectionuploadurl.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connectionuploadurl.setRequestProperty("charset", "utf-8");
                connectionuploadurl.setRequestProperty("Content-Length", Integer.toString(postData.length));
                connectionuploadurl.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connectionuploadurl.getOutputStream());
                writer.write(postData);
                String jsonResponse = myInputStreamReader(connectionuploadurl.getInputStream());


                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("uploadUrl")){
                        String[] splitter=s.split("\"");
                        uploadUrl=splitter[3];


                    }else if (s.contains("authorizationToken")){
                        String[] splitter=s.split("\"");
                        uploadAuthorizationToken=splitter[3];
                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connectionuploadurl.disconnect();
            }

            return null;
        }
    }

    class connecttt3 extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            //uploadpp send_data=new uploadpp( username.getText().toString(),useridd,gend,fileid,responseListener);
            final String username = etName2.getText().toString();

            System.out.println("rrrrrrrrrrrrrrrrrrr**************");
            StringRequest stringRequest = new StringRequest(Request.Method.POST, Send_Data_URL,
                    new Response.Listener<String>()
                    {
                        @Override
                        public void onResponse(String response) {
                            // response

                            System.out.println("only rrrrrrrrrrrrrrrrrrr**************    okkkkkk "+response);

                            System.out.println("only url "+pppurl);

                            progressBar.setVisibility(View.GONE);


                            editor.putString("uid",useridd);
                            editor.putString("name",username);
                            editor.putString("ppurl",pppurl);
                            editor.commit();

                            dismiss();

                            Intent intent = new Intent(getContext(),Profile.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                            System.out.println("only rrrrrrrrrrrrrrrrrrr**************"+error.toString());
                        }
                    }
            ) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", ACCESS_TOKEN);
                return params;
            }*/

                @Override
                protected Map<String, String> getParams()
                {
                    Map<String, String> params = new HashMap<String, String>();

                    params.put("username",username);
                    params.put("uid",useridd);
                    params.put("gender",gender);
                    params.put("ppurl",pppurl);

                    return params;
                }
                @Override
                protected Response<String> parseNetworkResponse(NetworkResponse response) {
                    //statusCode = response.statusCode;
                    return super.parseNetworkResponse(response);
                }

            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    1*2000,
                    2,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(stringRequest);



        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL(uploadUrl);
                connectionupload = (HttpURLConnection)url.openConnection();
                connectionupload.setRequestMethod("POST");
                connectionupload.setRequestProperty("Authorization", uploadAuthorizationToken);
                connectionupload.setRequestProperty("Content-Type", contentType);
                connectionupload.setRequestProperty("X-Bz-File-Name", fileName);
                connectionupload.setRequestProperty("X-Bz-Content-Sha1", "do_not_verify");
                connectionupload.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connectionupload.getOutputStream());
                writer.write(fileData);
                String jsonResponse = myInputStreamReader(connectionupload.getInputStream());


                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("fileId")){
                        String[] splitter=s.split("\"");
                        fileid=splitter[3];


                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connectionupload.disconnect();
            }

            return null;
        }


    }

    public static String myInputStreamReader(InputStream in) throws IOException {
        InputStreamReader reader = new InputStreamReader(in);
        StringBuilder sb = new StringBuilder();
        int c = reader.read();
        while (c != -1) {
            sb.append((char)c);
            c = reader.read();
        }
        reader.close();


        return sb.toString();
    }

}
