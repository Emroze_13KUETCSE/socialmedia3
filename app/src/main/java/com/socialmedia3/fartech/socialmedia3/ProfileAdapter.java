package com.socialmedia3.fartech.socialmedia3;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.CarsViewHolder>{
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    public ArrayList<ProfileItem> horizontalCarList;
    Context context;
    private OnCarItemClickListener listener;

    private DisplayImageOptions options,options2;

    public SimpleExoPlayer exoPlayer;

    private int height;

    DB_SqliteFollowing db_sqliteFollowing;
    DB_SqliteFavorites db_sqliteFavorites;

    String useridd="";

    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onllClicked(int position, ProfileItem item,CarsViewHolder holder);
        public void onFollowClicked(int position, ProfileItem item,CarsViewHolder holder);

    }
    public void setOnCarItemClickListener(OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public ProfileAdapter(ArrayList<ProfileItem> horizontalGrocderyList, Context context){
        this.horizontalCarList= horizontalGrocderyList;
        this.context = context;

        preference = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        useridd = preference.getString("uid","");

        db_sqliteFollowing = new DB_SqliteFollowing(context);
        db_sqliteFavorites = new DB_SqliteFavorites(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_profile2)
                .showImageForEmptyUri(R.mipmap.ic_profile2)
                .showImageOnFail(R.mipmap.ic_profile2)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();

    }

    public void setItemlist(ArrayList<ProfileItem> list) {
        horizontalCarList = list;
        //notifyDataSetChanged();
    }
    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CarsViewHolder carsViewHolder, final int position) {

        carsViewHolder.ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onllClicked(position,horizontalCarList.get(position),carsViewHolder);
            }
        });

        carsViewHolder.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onFollowClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });


        useridd = preference.getString("uid","");



        if (horizontalCarList.get(position).getUid().equals(useridd)){
            carsViewHolder.imgAdd.setVisibility(View.GONE);
            System.out.println("only ======================== select "+useridd+"   "+horizontalCarList.get(position).getUid());
        }else {
            carsViewHolder.imgAdd.setVisibility(View.VISIBLE);
            carsViewHolder.imgAdd.setImageResource(R.drawable.plus2);
            int count3 = db_sqliteFollowing.get_check_List_Favorite(horizontalCarList.get(position).getUid());
            if (count3 == 0) {
                carsViewHolder.imgAdd.setImageResource(R.drawable.plus2);
            } else {
                carsViewHolder.imgAdd.setImageResource(R.drawable.added);
            }
        }

        carsViewHolder.txtview.setText(horizontalCarList.get(position).getUserrname());



        /*ImageLoader.getInstance()
                .displayImage(horizontalCarList.get(position).getPpurl(),carsViewHolder.proPic, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                        carsViewHolder.proPic.setImageResource(R.mipmap.ic_profile2);

                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                        carsViewHolder.proPic.setImageResource(R.mipmap.ic_profile2);

                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        //Toast.makeText(context, "Complete", Toast.LENGTH_SHORT).show();
                        carsViewHolder.proPic.setImageBitmap(loadedImage);

                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                    }
                });*/






    }






    @Override
    public void onViewRecycled(@NonNull CarsViewHolder holder) {
        int position = holder.getAdapterPosition();
        System.out.println("only recycled "+position);


        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        if (horizontalCarList != null) {
            return horizontalCarList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {
        ImageView imgAdd;
        TextView txtview;
        RelativeLayout ll;
        CircleImageView proPic;



        public CarsViewHolder(View view) {
            super(view);
            proPic=view.findViewById(R.id.img_pro_pic);
            txtview=view.findViewById(R.id.name);
            imgAdd = view.findViewById(R.id.img_add);
            ll = view.findViewById(R.id.ll);
        }
    }
}
