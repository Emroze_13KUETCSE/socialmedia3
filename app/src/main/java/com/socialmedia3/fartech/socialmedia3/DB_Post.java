package com.socialmedia3.fartech.socialmedia3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class DB_Post extends SQLiteOpenHelper {
    public static final String BDname = "Post.db";

    public DB_Post(Context context) {
        super(context, BDname, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table Post (id INTEGER PRIMARY KEY AUTOINCREMENT,post TEXT,post_id TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Post");
        onCreate(db);
    }

    public Boolean Insertlist(String post,String post_id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        Cursor rs = db.rawQuery("select * from Post", null);
        rs.moveToFirst();
        int i = 0;
        while (!rs.isAfterLast()) {
            String id = rs.getString(rs.getColumnIndex("post_id"));

            if(id.equals(post_id)){
                i=1;
                break;
            }

            rs.moveToNext();
        }
        long result= -2;
        if(i == 0){
            contentValues.put("post_id",post_id);
            contentValues.put("post", post);
            result = db.insert("Post", null, contentValues);
            System.out.println("only v     insert  ");

        }else {
            contentValues.put("post",post);
            result = db.update("Post",contentValues,"post_id= ?",new  String[]{post_id});
            System.out.println("only v     update  ");
        }

        if (result == -1)
            return false;
        else
            return true;

    }


    public ArrayList getlikelist() {
        ArrayList<String> arraylist = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from Post", null);
        rs.moveToFirst();
        while (!rs.isAfterLast()) {

            String post = rs.getString(rs.getColumnIndex("post"));

            arraylist.add(post);
            rs.moveToNext();
        }
        return arraylist;
    }

    public String getPost(String key) {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from Post", null);
        rs.moveToFirst();
        String post="";
        while (!rs.isAfterLast()) {
            String id = rs.getString(rs.getColumnIndex("post_id"));

            if(id.equals(key)){
                post = rs.getString(rs.getColumnIndex("post"));
                break;
            }

            rs.moveToNext();
        }
        return post;
    }

    public boolean updateData (String id, String userid ) {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues=new ContentValues();
        contentValues.put("post",userid);



        db.update("Post",contentValues,"id= ?",new  String[]{id});

        return true;
    }

    public int get_check_List_Favorite(String userid) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor rs = db.rawQuery("select * from Post Where post Like'" + userid + "'", null);
        int count = rs.getCount();
        rs.close();
        return count;
    }

    public Integer Delete(String userid) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete("Post", "post = ?", new String[]{String.valueOf(userid)});
    }


    public int getcounts(){
        String counttt="SELECT * FROM "+ "Post";
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(counttt,null);
        int count=cursor.getCount();
        cursor.close();
        return count;
    }

}
