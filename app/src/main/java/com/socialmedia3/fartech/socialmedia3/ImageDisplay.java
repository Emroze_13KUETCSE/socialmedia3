package com.socialmedia3.fartech.socialmedia3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class ImageDisplay extends AppCompatActivity {

    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    String userName="",ppurl="",uid="",lang="",coverUrl="",otherId="",postId="",txt;

    int likes,comments,shares,downloads;
    CircleImageView pro1;
    TextView tvName1;

    TextView tvAdd;
    TextView text,tvWhatsApp,tvLikes,tvComments,tvDownloads,tvViews;
    ImageView imgCover,imgLike;
    DB_SqliteFollowing db_sqliteFollowing;
    DB_SqliteFavorites db_sqliteFavorites;
    private DisplayImageOptions options,options2;

    LinearLayout ll_WhatsApp,ll_likes,ll_comments,ll_downloads;

    int progresss,timeinsec;
    ProgressBar progressBar_down;

    ImageView back,imageView77;

    float views = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_display);


        db_sqliteFollowing = new DB_SqliteFollowing(getApplicationContext());
        db_sqliteFavorites = new DB_SqliteFavorites(getApplicationContext());

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        Intent intent = getIntent();
        ppurl = intent.getStringExtra("imgUrl");
        userName = intent.getStringExtra("userName");
        otherId = intent.getStringExtra("otherId");
        coverUrl = intent.getStringExtra("coverUrl");
        postId = intent.getStringExtra("postId");
        likes = intent.getIntExtra("likes",0);
        comments = intent.getIntExtra("comments",0);
        shares = intent.getIntExtra("shares",0);
        downloads = intent.getIntExtra("downloads",0);
        txt = intent.getStringExtra("txt");
        views = intent.getIntExtra("views",0);

        lang = preference.getString("lang","");
        uid = preference.getString("uid","");

        tvWhatsApp = findViewById(R.id.tv_whats_app);
        tvLikes = findViewById(R.id.tv_like);
        tvComments = findViewById(R.id.tv_comments);
        tvDownloads = findViewById(R.id.tv_downloads);
        imgLike = findViewById(R.id.img_like);
        tvViews = findViewById(R.id.tv_views);

        ll_WhatsApp = findViewById(R.id.ll_whats_app);
        ll_likes = findViewById(R.id.ll_likes);
        ll_comments = findViewById(R.id.ll_comments);
        ll_downloads = findViewById(R.id.ll_downloads);
        imgCover = findViewById(R.id.cover);
        progressBar_down = findViewById(R.id.progress_circular);
        back=(ImageView)findViewById(R.id.imv_back);

        pro1 = findViewById(R.id.img_pro_pic1);
        tvName1 = findViewById(R.id.tv_name);
        tvAdd = findViewById(R.id.tv_add);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageDisplay.super.onBackPressed();
            }
        });



        String vvv="";

        views=views+1;
        if(views<=999){
            int vvvvv = (int)views;
            vvv = String.valueOf(vvvvv)+" views";
        }
        else if(views >= 1000){
            views = views/1000;
            vvv = String.valueOf(views)+"k views";
        }else if(views >= 1000000){
            views = views/1000000;
            vvv = String.valueOf(views)+"M views";
        }

        tvViews.setText(vvv);



        postViews(0);



        tvComments.setText(String.valueOf(comments));
        tvLikes.setText(String.valueOf(likes));
        tvDownloads.setText(String.valueOf(downloads));
        tvWhatsApp.setText(String.valueOf(shares));

        int count2 = db_sqliteFavorites.get_check_List_Favorite(postId);
        if (count2 == 0) {
            imgLike.setImageResource(R.drawable.dislike);
        } else {
            imgLike.setImageResource(R.drawable.love2);
        }
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_profile2)
                .showImageForEmptyUri(R.mipmap.ic_profile2)
                .showImageOnFail(R.mipmap.ic_profile2)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();

        options2 = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.border)
                .showImageForEmptyUri(R.drawable.no_img3)
                .showImageOnFail(R.drawable.no_img3)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();

        ImageLoader.getInstance()
                .displayImage(coverUrl,imgCover, options2, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                        imgCover.setImageBitmap(loadedImage);

                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                    }
                });

        ImageLoader.getInstance()
                .displayImage(ppurl,pro1, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

                        pro1.setImageBitmap(loadedImage);

                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                    }
                });

        tvName1.setText(userName);



        int count3 = db_sqliteFollowing.get_check_List_Favorite(otherId);
        if (count3 == 0) {
            tvAdd.setText("Follow");
        } else {
            tvAdd.setText("Unfollow");
        }

        ll_WhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!txt.equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, txt+"\nDownload this App here: "+getString(R.string.app_url));
                }
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Title", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }


                shares++;
                tvWhatsApp.setText(String.valueOf(shares));


                StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }

                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("postid", postId);

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringrequest);
            }
        });


        ll_likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if (currentUser==null) {
                    LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                    logInBottomSheet.show(getSupportFragmentManager(),"main");
                }else {

                    int count = db_sqliteFavorites.get_check_List_Favorite(postId);
                    if (count == 0) {
                        imgLike.setImageResource(R.drawable.love2);

                        db_sqliteFavorites.Inserttoalertlist(postId);

                        ++likes;

                        tvLikes.setText(String.valueOf(likes));

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/sociallikes.php",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {


                                    }
                                }

                                , new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("postid", postId);

                                return params;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                1*2000,
                                2,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        ));

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);
                    } else {
                        imgLike.setImageResource(R.drawable.dislike);
                        db_sqliteFavorites.Delete(postId);


                        --likes;

                        tvLikes.setText(String.valueOf(likes));


                        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialdislikes.php",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {


                                    }
                                }

                                , new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("postid", postId);

                                return params;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                1*2000,
                                2,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        ));

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);

                    }
                }
            }
        });

        ll_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth mAuth = FirebaseAuth.getInstance();
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if (currentUser==null) {
                    LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                    logInBottomSheet.show(getSupportFragmentManager(),"main");
                }
                else {

                    Intent intent=new Intent(getApplicationContext(),Comments.class);
                    intent.putExtra("postid",postId);
                    startActivityForResult(intent,11);
                }
            }
        });

        ll_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    Bitmap bm = ((BitmapDrawable)imgCover.getDrawable()).getBitmap();

                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Flash/");

                    boolean doSave = true;
                    if (!f.exists()) {
                        doSave = f.mkdirs();
                    }

                    if (doSave) {
                        saveBitmapToFile(f,postId+".png",bm,Bitmap.CompressFormat.PNG,100);
                    }
                    else {
                        Log.e("app","Couldn't create target directory.");
                    }


                    ++downloads;
                    tvDownloads.setText(String.valueOf(downloads));

                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {




                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("postid",postId);

                            return params;
                        }
                    };

                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                }catch (NullPointerException e){

                }
            }
        });

        tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!uid.equals("")){
                    if (tvAdd.getText().toString().equals("Follow")) {
                        db_sqliteFollowing.Inserttoalertlist(otherId);
                        tvAdd.setText("Unfollow");


                        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/followings.php",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {



                                    }
                                }

                                , new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Uid",uid);
                                params.put("Following", otherId);


                                return params;
                            }
                        };

                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                1*2000,
                                2,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        ));

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);

                    }else {
                        db_sqliteFollowing.Delete(otherId);
                        tvAdd.setText("Follow");
                        tvAdd.setVisibility(View.VISIBLE);

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/unfollow.php",
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {



                                    }
                                }

                                , new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }) {

                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<String, String>();
                                params.put("Uid",uid);
                                params.put("Following", otherId);


                                return params;
                            }
                        };
                        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                                1*2000,
                                2,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                        ));

                        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                        requestQueue.add(stringRequest);

                    }

                }else {
                    LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                    logInBottomSheet.show(getSupportFragmentManager(),"main");
                }
            }
        });


    }

    public void postViews(final int position){
        StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/videoviews.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {


                    }
                }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("postid", postId);

                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringrequest);
    }


    boolean saveBitmapToFile(File dir, String fileName, Bitmap bm,
                             Bitmap.CompressFormat format, int quality) {

        File imageFile = new File(dir,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);

            bm.compress(format,quality,fos);

            fos.close();

            return true;
        }
        catch (IOException e) {
            Log.e("app",e.getMessage());
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }
}
