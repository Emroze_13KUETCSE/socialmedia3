package com.socialmedia3.fartech.socialmedia3;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.greenfrvr.hashtagview.HashtagView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

public class ExoAdapterProfile extends RecyclerView.Adapter<ExoAdapterProfile.CarsViewHolder>{
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    public ArrayList<PostItem> horizontalCarList;
    Context context;
    private OnCarItemClickListener listener;

    private DisplayImageOptions options,options2;

    public SimpleExoPlayer exoPlayer;

    private int height;

    DB_SqliteFollowing db_sqliteFollowing;
    DB_SqliteFavorites db_sqliteFavorites;

    String useridd="";

    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onllClicked(int position, PostItem item,CarsViewHolder holder);
        public void onCoverClicked(int position, PostItem item,CarsViewHolder holder);
        public void onWhatsAppClicked(int position, PostItem item,CarsViewHolder holder);
        public void onLikeClicked(int position, PostItem item,CarsViewHolder holder);
        public void onCommentsClicked(int position, PostItem item,CarsViewHolder holder);
        public void onDownloadsClicked(int position, PostItem item,CarsViewHolder holder);
        public void onMoreClicked(int position, PostItem item,CarsViewHolder holder);
        public void onShareClicked(int position, PostItem item,CarsViewHolder holder);
        public void onReportClicked(int position, PostItem item,CarsViewHolder holder);
        public void onAddClicked(int position, PostItem item,CarsViewHolder holder);
        public void onProPicClicked(int position, PostItem item,CarsViewHolder holder);

    }

    public void setOnCarItemClickListener(OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public ExoAdapterProfile(ArrayList<PostItem> horizontalGrocderyList, Context context,int height){
        this.horizontalCarList= horizontalGrocderyList;
        this.context = context;
        this.height = height;

        preference = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        useridd = preference.getString("uid","");

        /*db_sqliteFollowing = new DB_SqliteFollowing(context);
        db_sqliteFavorites = new DB_SqliteFavorites(context);

        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_profile2)
                .showImageForEmptyUri(R.mipmap.ic_profile2)
                .showImageOnFail(R.mipmap.ic_profile2)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();

        options2 = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.border)
                .showImageForEmptyUri(R.drawable.no_img3)
                .showImageOnFail(R.drawable.no_img3)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();*/
    }

    public void setItemlist(ArrayList<PostItem> list) {
        horizontalCarList = list;
        //notifyDataSetChanged();
    }

    public void setItemlist3(boolean isLiked,int pos) {

        horizontalCarList.get(pos).setLiked(isLiked);

        //notifyDataSetChanged();
    }

    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.post_item2, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CarsViewHolder carsViewHolder, final int position) {
        String name = horizontalCarList.get(position).getUsername();

        carsViewHolder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onllClicked(position,horizontalCarList.get(position),carsViewHolder);
            }
        });

        carsViewHolder.imgCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCoverClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });


        carsViewHolder.ll_WhatsApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onWhatsAppClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });

        carsViewHolder.ll_likes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onLikeClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });

        carsViewHolder.ll_comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onCommentsClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });

        carsViewHolder.ll_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onDownloadsClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });

        carsViewHolder.tvAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onAddClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });

        carsViewHolder.imgMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onMoreClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);
            }
        });

        carsViewHolder.ll_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onShareClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });
        carsViewHolder.ll_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onReportClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });

        carsViewHolder.proPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onProPicClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });

        useridd = preference.getString("uid","");


        if (!horizontalCarList.get(position).isLiked()) {
            carsViewHolder.imgLike.setImageResource(R.drawable.dislike);
        } else {
            carsViewHolder.imgLike.setImageResource(R.drawable.love2);
        }

        if (horizontalCarList.get(position).getUid().equals(useridd)){
            carsViewHolder.tvAdd.setVisibility(View.GONE);
            System.out.println("only ======================== select "+useridd+"   "+horizontalCarList.get(position).getUid());
        }else {
            carsViewHolder.tvAdd.setVisibility(View.VISIBLE);

            if (horizontalCarList.get(position).isFollowing()) {
                carsViewHolder.tvAdd.setText("Unfollow");
            } else {
                carsViewHolder.tvAdd.setText("Follow");
            }
        }

        String url = horizontalCarList.get(position).getVideoUrl();

        carsViewHolder.progressBar.setVisibility(View.GONE);

        int likes=0;
        int downloads=0;
        int share=0;
        int comments=0;

        try{
            likes = horizontalCarList.get(position).getLikes();
            downloads = horizontalCarList.get(position).getDownloadsno();
            share = horizontalCarList.get(position).getShares();
            comments = horizontalCarList.get(position).getCommentsno();

        }catch (NullPointerException e){

        }

        if(share > 0){
            carsViewHolder.tvWhatsApp.setText(String.valueOf(share));
        }
        else {
            carsViewHolder.tvWhatsApp.setText("WhatsApp");
        }

        if(comments > 0){
            carsViewHolder.tvComments.setText(String.valueOf(comments));
        }
        else {
            carsViewHolder.tvComments.setText("Comments");
        }

        if(likes > 0){
            carsViewHolder.tvLikes.setText(String.valueOf(likes));
        }
        else {
            carsViewHolder.tvLikes.setText("Likes");
        }

        if(downloads > 0){
            carsViewHolder.tvDownloads.setText(String.valueOf(downloads));
        }
        else {
            carsViewHolder.tvDownloads.setText("Save");
        }

        float views = 0;
        String vvv="";
        views = horizontalCarList.get(position).getViews();

        if(views<=999){
            int vvvvv = (int)views;
            vvv = String.valueOf(vvvvv)+" views";
        }
        else if(views >= 1000){
            views = views/1000;
            vvv = String.valueOf(views)+"k views";
        }else if(views >= 1000000){
            views = views/1000000;
            vvv = String.valueOf(views)+"M views";
        }
        carsViewHolder.tvViews.setText(vvv);

        carsViewHolder.hashText.setVisibility(View.VISIBLE);

        carsViewHolder.hashText.setData(horizontalCarList.get(position).getWord_list());


        carsViewHolder.text.setVisibility(View.VISIBLE);

        carsViewHolder.text.setText(horizontalCarList.get(position).getText());


        carsViewHolder.hashText.addOnTagClickListener(new HashtagView.TagsClickListener() {
            @Override
            public void onItemClicked(Object item) {
                System.out.println("only >> hash "+item.toString());

                Intent intent = new Intent(context,HashTag.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("text",item.toString());
                context.startActivity(intent);
            }
        });

        if (!url.equals("https://static.heyapp.in/file/app-media/")){

            carsViewHolder.imgCover.setVisibility(View.VISIBLE);
            carsViewHolder.play.setVisibility(View.VISIBLE);
            carsViewHolder.mExoPlayer.setVisibility(View.GONE);
            carsViewHolder.tvViews.setVisibility(View.VISIBLE);
        }else {
            carsViewHolder.mExoPlayer.setVisibility(View.GONE);
            carsViewHolder.play.setVisibility(View.GONE);
            carsViewHolder.imgCover.setVisibility(View.VISIBLE);
            carsViewHolder.tvViews.setVisibility(View.GONE);
        }


        if(!horizontalCarList.get(position).getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

            Glide.with(context).load(horizontalCarList.get(position).getImagelocation()).into(carsViewHolder.imgCover);

            /*if(horizontalCarList.get(position).getPostphoto() == null){
                ImageLoader.getInstance()
                        .displayImage(horizontalCarList.get(position).getImagelocation(),carsViewHolder.imgCover, options2, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                //Toast.makeText(context, "Complete", Toast.LENGTH_SHORT).show();
                                carsViewHolder.imgCover.setImageBitmap(loadedImage);

                                horizontalCarList.get(position).setPostphoto(loadedImage);

                                //carsViewHolder.mExoPlayer.getLayoutParams().height = carsViewHolder.imgCover.getHeight();
                                //carsViewHolder.mExoPlayer.requestFocus();

                            }
                        }, new ImageLoadingProgressListener() {
                            @Override
                            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                                //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                            }
                        });
            }
            else {
                carsViewHolder.imgCover.setImageBitmap(horizontalCarList.get(position).getPostphoto());
            }*/
        }



        carsViewHolder.txtview.setText(horizontalCarList.get(position).getUsername());

        Glide.with(context).load(horizontalCarList.get(position).getPpurl()).into(carsViewHolder.proPic);


        /*if(horizontalCarList.get(position).getUserphoto() == null){
            ImageLoader.getInstance()
                    .displayImage(horizontalCarList.get(position).getPpurl(),carsViewHolder.proPic, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            //Toast.makeText(context, "Complete", Toast.LENGTH_SHORT).show();
                            carsViewHolder.proPic.setImageBitmap(loadedImage);

                            horizontalCarList.get(position).setUserphoto(loadedImage);

                            //carsViewHolder.mExoPlayer.getLayoutParams().height = carsViewHolder.imgCover.getHeight();
                            //carsViewHolder.mExoPlayer.requestFocus();

                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                            //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                        }
                    });
        }
        else {
            carsViewHolder.proPic.setImageBitmap(horizontalCarList.get(position).getUserphoto());
        }*/



    }






    @Override
    public void onViewRecycled(@NonNull CarsViewHolder holder) {
        int position = holder.getAdapterPosition();
        System.out.println("only recycled "+position);
        if(position >= 0){
            String url = horizontalCarList.get(position).getVideoUrl();

            if (!url.equals("https://static.heyapp.in/file/app-media/")){

                holder.imgCover.setVisibility(View.VISIBLE);
                holder.play.setVisibility(View.VISIBLE);
                holder.mExoPlayer.setVisibility(View.GONE);


            }else {
                holder.mExoPlayer.setVisibility(View.GONE);

                holder.play.setVisibility(View.GONE);
                holder.imgCover.setVisibility(View.VISIBLE);
            }
        }


        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        if (horizontalCarList != null) {
            return horizontalCarList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {
        ImageView imgCover,play,imgMore,imgLike;
        TextView txtview,tvAdd;
        LinearLayout ll;
        CircleImageView proPic;
        //VideoPlayView bigVideoView;
        public PlayerView mExoPlayer;
        ProgressBar progressBar;

        TextView text, tvWhatsApp,tvLikes, tvComments, tvDownloads,tvViews;

        LinearLayout ll_WhatsApp,ll_likes,ll_comments,ll_downloads;

        ProgressBar progressBar_down,progressBar_share;

        CardView cardmore;
        LinearLayout ll_share,ll_report;
        HashtagView hashText;


        public CarsViewHolder(View view) {
            super(view);
            proPic=view.findViewById(R.id.img_pro_pic);
            txtview=view.findViewById(R.id.name);

            mExoPlayer = view.findViewById(R.id.video_view);

            imgCover = view.findViewById(R.id.cover);
            play = view.findViewById(R.id.play);

            progressBar = view.findViewById(R.id.progress);
            progressBar_down = view.findViewById(R.id.progress_circular);

            text = view.findViewById(R.id.text);
            hashText = view.findViewById(R.id.hash_text);

            tvWhatsApp = view.findViewById(R.id.tv_whats_app);
            tvLikes = view.findViewById(R.id.tv_like);
            tvComments = view.findViewById(R.id.tv_comments);
            tvDownloads = view.findViewById(R.id.tv_downloads);
            tvViews = view.findViewById(R.id.tv_views);


            ll_WhatsApp = view.findViewById(R.id.ll_whats_app);
            ll_likes = view.findViewById(R.id.ll_likes);
            ll_comments = view.findViewById(R.id.ll_comments);
            ll_downloads = view.findViewById(R.id.ll_downloads);

            ll_share = view.findViewById(R.id.ll_share);
            ll_report = view.findViewById(R.id.ll_report);


            cardmore = view.findViewById(R.id.cardmore);
            imgMore = view.findViewById(R.id.img_more);

            progressBar_share = view.findViewById(R.id.progress_circular_share);
            tvAdd = view.findViewById(R.id.tv_add);

            imgLike = view.findViewById(R.id.img_like);

        }
    }

}
