package com.socialmedia3.fartech.socialmedia3;

import android.content.ClipData;
import android.icu.util.ULocale;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Locale;

public class DataWrapper implements Parcelable {

    private ArrayList<? extends WhatsAppItem> parliaments;

    public DataWrapper() {
    }

    public DataWrapper(ArrayList<WhatsAppItem> data) {
        this.parliaments = data;
    }

    public ArrayList<WhatsAppItem> getParliaments() {
        return (ArrayList<WhatsAppItem>) this.parliaments;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        Bundle b = new Bundle();
        b.putParcelableArrayList("items", (ArrayList<? extends Parcelable>) parliaments);
        dest.writeBundle(b);

    }

    public static final Parcelable.Creator<DataWrapper> CREATOR =
            new Parcelable.Creator<DataWrapper>() {
                public DataWrapper createFromParcel(Parcel in) {
                    DataWrapper category = new DataWrapper();
                    Bundle b = in.readBundle(WhatsAppItem.class.getClassLoader());

                    category.parliaments = b.getParcelableArrayList("items");

                    return category;
                }

                @Override
                public DataWrapper[] newArray(int size) {
                    return new DataWrapper[size];
                }
            };
}