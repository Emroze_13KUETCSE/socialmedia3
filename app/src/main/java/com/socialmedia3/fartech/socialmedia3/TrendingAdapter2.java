package com.socialmedia3.fartech.socialmedia3;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

public class TrendingAdapter2 extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    public ArrayList<PostItem2> horizontalCarList;
    Context context;
    private OnCarItemClickListener listener;

    String useridd="";

    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onImg1Click(int position, PostItem item );
        public void onImg2Click(int position, PostItem item );
        public void onImg3Click(int position, PostItem item );

    }
    public void setOnCarItemClickListener(OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public TrendingAdapter2(Context context, ArrayList<PostItem2> list){
        this.horizontalCarList= list;
        this.context = context;

        preference = context.getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        useridd = preference.getString("uid","");

    }

    public void setItemlist(ArrayList<PostItem2> list) {
        horizontalCarList = list;

    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View view0 = LayoutInflater.from(parent.getContext()).inflate(R.layout.trend_item0, parent, false);
        View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.trend_item1, parent, false);
        View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.trend_item2, parent, false);
        View view3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.trend_item3, parent, false);

        switch (viewType){
            case 0: return new CarsViewHolder(view0);
            case 1: return new CarsViewHolder1(view1);
            case 2: return new CarsViewHolder2(view2);
            default: return new CarsViewHolder3(view3);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {

        final PostItem p1 = horizontalCarList.get(position).getPostItem1();
        final PostItem p2 = horizontalCarList.get(position).getPostItem2();
        final PostItem p3 = horizontalCarList.get(position).getPostItem3();


        int type = holder.getItemViewType();
        switch (type) {
            case 0:
                final CarsViewHolder viewHolder0 = (CarsViewHolder)holder;







                viewHolder0.imgCover1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg1Click(position,p1);
                    }
                });
                viewHolder0.imgCover2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg2Click(position,p2);
                    }
                });
                viewHolder0.imgCover3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg3Click(position,p3);
                    }
                });

                if(p1 != null){
                    String url = p1.getVideoUrl();
                    viewHolder0.imgSrc1.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder0.imgSrc1.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder0.imgSrc1.setImageResource(R.drawable.image_small);
                    }

                    viewHolder0.imgCover1.setVisibility(View.VISIBLE);
                    if(!p1.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p1.getImagelocation()).into(viewHolder0.imgCover1);

                    }

                    float views1 = 0;
                    String vvv1="";
                    views1 = p1.getViews();

                    if(views1<=999){
                        int vvvvv = (int)views1;
                        vvv1 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views1 >= 1000){
                        views1 = views1/1000;
                        vvv1 = String.valueOf(views1)+"k views";
                    }else if(views1 >= 1000000){
                        views1 = views1/1000000;
                        vvv1 = String.valueOf(views1)+"M views";
                    }

                    viewHolder0.tv1.setText(vvv1);
                }
                else {
                    viewHolder0.imgCover1.setVisibility(View.INVISIBLE);
                    viewHolder0.imgSrc1.setVisibility(View.GONE);
                }

                if(p2 != null){
                    String url = p2.getVideoUrl();
                    viewHolder0.imgSrc2.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder0.imgSrc2.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder0.imgSrc2.setImageResource(R.drawable.image_small);
                    }
                    viewHolder0.imgCover2.setVisibility(View.VISIBLE);
                    if(!p2.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p2.getImagelocation()).into(viewHolder0.imgCover2);

                    }

                    float views2 = 0;
                    String vvv2="";
                    views2 = p2.getViews();

                    if(views2<=999){
                        int vvvvv = (int)views2;
                        vvv2 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views2 >= 1000){
                        views2 = views2/1000;
                        vvv2 = String.valueOf(views2)+"k views";
                    }else if(views2 >= 1000000){
                        views2 = views2/1000000;
                        vvv2 = String.valueOf(views2)+"M views";
                    }

                    viewHolder0.tv2.setText(vvv2);


                }
                else {
                    viewHolder0.imgCover2.setVisibility(View.GONE);
                    viewHolder0.imgSrc2.setVisibility(View.GONE);
                }

                if(p3 != null){
                    String url = p3.getVideoUrl();
                    viewHolder0.imgSrc3.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder0.imgSrc3.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder0.imgSrc3.setImageResource(R.drawable.image_small);
                    }
                    viewHolder0.imgCover3.setVisibility(View.VISIBLE);
                    if(!p3.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p3.getImagelocation()).into(viewHolder0.imgCover3);


                    }

                    float views = 0;
                    String vvv="";
                    views = p3.getViews();


                    if(views<=999){
                        int vvvvv = (int)views;
                        vvv = String.valueOf(vvvvv)+" views";
                    }
                    else if(views >= 1000){
                        views = views/1000;
                        vvv = String.valueOf(views)+"k views";
                    }else if(views >= 1000000){
                        views = views/1000000;
                        vvv = String.valueOf(views)+"M views";
                    }

                    viewHolder0.tv3.setText(vvv);

                }
                else {
                    viewHolder0.imgCover3.setVisibility(View.GONE);
                    viewHolder0.imgSrc3.setVisibility(View.GONE);
                }


                break;

            case 1:
                final CarsViewHolder1 viewHolder1 = (CarsViewHolder1)holder;






                viewHolder1.imgCover1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg1Click(position,p1);
                    }
                });
                viewHolder1.imgCover2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg2Click(position,p2);
                    }
                });
                viewHolder1.imgCover3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg3Click(position,p3);
                    }
                });
                if(p1 != null){
                    String url = p1.getVideoUrl();
                    viewHolder1.imgSrc1.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder1.imgSrc1.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder1.imgSrc1.setImageResource(R.drawable.image_small);
                    }
                    viewHolder1.imgCover1.setVisibility(View.VISIBLE);
                    if(!p1.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p1.getImagelocation()).into(viewHolder1.imgCover1);

                    }
                    float views12 = 0;
                    String vvv12="";
                    views12 = p1.getViews();

                    if(views12<=999){
                        int vvvvv = (int)views12;
                        vvv12 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views12 >= 1000){
                        views12 = views12/1000;
                        vvv12 = String.valueOf(views12)+"k views";
                    }else if(views12 >= 1000000){
                        views12 = views12/1000000;
                        vvv12 = String.valueOf(views12)+"M views";
                    }

                    viewHolder1.tv1.setText(vvv12);

                }
                else {
                    viewHolder1.imgCover1.setVisibility(View.GONE);
                    viewHolder1.imgSrc1.setVisibility(View.GONE);
                }

                if(p2 != null){
                    String url = p2.getVideoUrl();
                    viewHolder1.imgSrc2.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder1.imgSrc2.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder1.imgSrc2.setImageResource(R.drawable.image_small);
                    }
                    viewHolder1.imgCover2.setVisibility(View.VISIBLE);
                    if(!p2.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p2.getImagelocation()).into(viewHolder1.imgCover2);

                    }

                    float views22 = 0;
                    String vvv22="";
                    views22 = p2.getViews();

                    if(views22<=999){
                        int vvvvv = (int)views22;
                        vvv22 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views22 >= 1000){
                        views22 = views22/1000;
                        vvv22 = String.valueOf(views22)+"k views";
                    }else if(views22 >= 1000000){
                        views22 = views22/1000000;
                        vvv22 = String.valueOf(views22)+"M views";
                    }

                    viewHolder1.tv2.setText(vvv22);

                }
                else {
                    viewHolder1.imgCover2.setVisibility(View.GONE);
                    viewHolder1.imgSrc2.setVisibility(View.GONE);
                }

                if(p3 != null){
                    String url = p3.getVideoUrl();
                    viewHolder1.imgSrc3.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder1.imgSrc3.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder1.imgSrc3.setImageResource(R.drawable.image_small);
                    }
                    viewHolder1.imgCover3.setVisibility(View.VISIBLE);
                    if(!p3.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p3.getImagelocation()).into(viewHolder1.imgCover3);

                    }

                    float views32 = 0;
                    String vvv32="";
                    views32 = p3.getViews();


                    if(views32<=999){
                        int vvvvv = (int)views32;
                        vvv32 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views32 >= 1000){
                        views32 = views32/1000;
                        vvv32 = String.valueOf(views32)+"k views";
                    }else if(views32 >= 1000000){
                        views32 = views32/1000000;
                        vvv32 = String.valueOf(views32)+"M views";
                    }

                    viewHolder1.tv3.setText(vvv32);

                }
                else {
                    viewHolder1.imgCover3.setVisibility(View.GONE);
                    viewHolder1.imgSrc3.setVisibility(View.GONE);
                }
                break;
            case 2:
                final CarsViewHolder2 viewHolder2 = (CarsViewHolder2)holder;







                viewHolder2.imgCover1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg1Click(position,p1);
                    }
                });
                viewHolder2.imgCover2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg2Click(position,p2);
                    }
                });
                viewHolder2.imgCover3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg3Click(position,p3);
                    }
                });
                if(p1 != null){
                    String url = p1.getVideoUrl();
                    viewHolder2.imgSrc1.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder2.imgSrc1.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder2.imgSrc1.setImageResource(R.drawable.image_small);
                    }
                    viewHolder2.imgCover1.setVisibility(View.VISIBLE);
                    if(!p1.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p1.getImagelocation()).into(viewHolder2.imgCover1);

                    }
                    float views13 = 0;
                    String vvv13="";
                    views13 = p1.getViews();

                    if(views13<=999){
                        int vvvvv = (int)views13;
                        vvv13 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views13 >= 1000){
                        views13 = views13/1000;
                        vvv13 = String.valueOf(views13)+"k views";
                    }else if(views13 >= 1000000){
                        views13 = views13/1000000;
                        vvv13 = String.valueOf(views13)+"M views";
                    }

                    viewHolder2.tv1.setText(vvv13);

                }
                else {
                    viewHolder2.imgCover1.setVisibility(View.GONE);
                    viewHolder2.imgSrc1.setVisibility(View.GONE);
                }

                if(p2 != null){
                    String url = p2.getVideoUrl();
                    viewHolder2.imgSrc2.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder2.imgSrc2.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder2.imgSrc2.setImageResource(R.drawable.image_small);
                    }
                    viewHolder2.imgCover2.setVisibility(View.VISIBLE);
                    if(!p2.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p2.getImagelocation()).into(viewHolder2.imgCover2);

                    }
                    float views23 = 0;
                    String vvv23="";
                    views23 = p2.getViews();

                    if(views23<=999){
                        int vvvvv = (int)views23;
                        vvv23 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views23 >= 1000){
                        views23 = views23/1000;
                        vvv23 = String.valueOf(views23)+"k views";
                    }else if(views23 >= 1000000){
                        views23 = views23/1000000;
                        vvv23 = String.valueOf(views23)+"M views";
                    }

                    viewHolder2.tv2.setText(vvv23);

                }
                else {
                    viewHolder2.imgCover2.setVisibility(View.GONE);
                    viewHolder2.imgSrc2.setVisibility(View.GONE);
                }

                if(p3 != null){
                    String url = p3.getVideoUrl();
                    viewHolder2.imgSrc3.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder2.imgSrc3.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder2.imgSrc3.setImageResource(R.drawable.image_small);
                    }
                    viewHolder2.imgCover3.setVisibility(View.VISIBLE);
                    if(!p3.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p3.getImagelocation()).into(viewHolder2.imgCover3);

                    }
                    float views33 = 0;
                    String vvv33="";
                    views33 = p3.getViews();


                    if(views33<=999){
                        int vvvvv = (int)views33;
                        vvv33 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views33 >= 1000){
                        views33 = views33/1000;
                        vvv33 = String.valueOf(views33)+"k views";
                    }else if(views33 >= 1000000){
                        views33 = views33/1000000;
                        vvv33 = String.valueOf(views33)+"M views";
                    }

                    viewHolder2.tv3.setText(vvv33);

                }
                else {
                    viewHolder2.imgCover3.setVisibility(View.GONE);
                    viewHolder2.imgSrc3.setVisibility(View.GONE);
                }
                break;
            default:
                final CarsViewHolder3 viewHolder3 = (CarsViewHolder3)holder;





                viewHolder3.imgCover1.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg1Click(position,p1);
                    }
                });
                viewHolder3.imgCover2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg2Click(position,p2);
                    }
                });
                viewHolder3.imgCover3.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        listener.onImg3Click(position,p3);
                    }
                });
                if(p1 != null){
                    String url = p1.getVideoUrl();
                    viewHolder3.imgSrc1.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder3.imgSrc1.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder3.imgSrc1.setImageResource(R.drawable.image_small);
                    }
                    viewHolder3.imgCover1.setVisibility(View.VISIBLE);
                    if(!p1.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p1.getImagelocation()).into(viewHolder3.imgCover1);

                    }

                    float views14 = 0;
                    String vvv14="";
                    views14 = p1.getViews();

                    if(views14<=999){
                        int vvvvv = (int)views14;
                        vvv14 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views14 >= 1000){
                        views14 = views14/1000;
                        vvv14 = String.valueOf(views14)+"k views";
                    }else if(views14 >= 1000000){
                        views14 = views14/1000000;
                        vvv14 = String.valueOf(views14)+"M views";
                    }

                    viewHolder3.tv1.setText(vvv14);

                }
                else {
                    viewHolder3.imgCover1.setVisibility(View.GONE);
                    viewHolder3.imgSrc1.setVisibility(View.GONE);
                }

                if(p2 != null){
                    String url = p2.getVideoUrl();
                    viewHolder3.imgSrc2.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder3.imgSrc2.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder3.imgSrc2.setImageResource(R.drawable.image_small);
                    }
                    viewHolder3.imgCover2.setVisibility(View.VISIBLE);
                    if(!p2.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p2.getImagelocation()).into(viewHolder3.imgCover2);

                    }

                    float views24 = 0;
                    String vvv24="";
                    views24 = p2.getViews();

                    if(views24<=999){
                        int vvvvv = (int)views24;
                        vvv24 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views24 >= 1000){
                        views24 = views24/1000;
                        vvv24 = String.valueOf(views24)+"k views";
                    }else if(views24 >= 1000000){
                        views24 = views24/1000000;
                        vvv24 = String.valueOf(views24)+"M views";
                    }

                    viewHolder3.tv2.setText(vvv24);


                }
                else {
                    viewHolder3.imgCover2.setVisibility(View.GONE);
                    viewHolder3.imgSrc2.setVisibility(View.GONE);
                }

                if(p3 != null){
                    String url = p3.getVideoUrl();
                    viewHolder3.imgSrc3.setVisibility(View.VISIBLE);
                    if (!url.equals("https://static.heyapp.in/file/app-media/")){
                        viewHolder3.imgSrc3.setImageResource(R.drawable.video_small);

                    }else {
                        viewHolder3.imgSrc3.setImageResource(R.drawable.image_small);
                    }
                    viewHolder3.imgCover3.setVisibility(View.VISIBLE);
                    if(!p3.getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

                        Glide.with(context).load(p3.getImagelocation()).into(viewHolder3.imgCover3);

                    }

                    float views34 = 0;
                    String vvv34="";
                    views34 = p3.getViews();


                    if(views34<=999){
                        int vvvvv = (int)views34;
                        vvv34 = String.valueOf(vvvvv)+" views";
                    }
                    else if(views34 >= 1000){
                        views34 = views34/1000;
                        vvv34 = String.valueOf(views34)+"k views";
                    }else if(views34 >= 1000000){
                        views34 = views34/1000000;
                        vvv34 = String.valueOf(views34)+"M views";
                    }

                    viewHolder3.tv3.setText(vvv34);

                }
                else {
                    viewHolder3.imgCover3.setVisibility(View.GONE);
                    viewHolder3.imgSrc3.setVisibility(View.GONE);

                }
                break;
        }


/*

        String url = horizontalCarList.get(position).getVideoUrl();


        useridd = preference.getString("uid","");



        if (!url.equals("https://static.heyapp.in/file/app-media/")){
            carsViewHolder.imgSrc.setImageResource(R.drawable.vv);

        }else {
            carsViewHolder.imgSrc.setImageResource(R.drawable.no_img2);
        }
        int likes=0;

        try{
            likes = horizontalCarList.get(position).getLikes();

        }catch (NullPointerException e){

        }


        if(likes > 0){
            carsViewHolder.tvLikes.setText(String.valueOf(likes));
        }
        else {
            carsViewHolder.tvLikes.setText("Likes");
        }


        if(!horizontalCarList.get(position).getImagelocation().equals("https://static.heyapp.in/file/app-media/")){

            if(horizontalCarList.get(position).getPostphoto() == null){
                ImageLoader.getInstance()
                        .displayImage(horizontalCarList.get(position).getImagelocation(),carsViewHolder.imgCover, options2, new SimpleImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {
                                //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                //Toast.makeText(context, "Complete", Toast.LENGTH_SHORT).show();
                                carsViewHolder.imgCover.setImageBitmap(loadedImage);

                                horizontalCarList.get(position).setPostphoto(loadedImage);

                                //carsViewHolder.mExoPlayer.getLayoutParams().height = carsViewHolder.imgCover.getHeight();
                                //carsViewHolder.mExoPlayer.requestFocus();

                            }
                        }, new ImageLoadingProgressListener() {
                            @Override
                            public void onProgressUpdate(String imageUri, View view, int current, int total) {
                                //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                            }
                        });
            }
            else {
                carsViewHolder.imgCover.setImageBitmap(horizontalCarList.get(position).getPostphoto());
            }
        }


        if(horizontalCarList.get(position).getUserphoto() == null){
            ImageLoader.getInstance()
                    .displayImage(horizontalCarList.get(position).getImagelocation(),carsViewHolder.proPic, options, new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                            //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                            //Toast.makeText(context, "Complete", Toast.LENGTH_SHORT).show();
                            carsViewHolder.proPic.setImageBitmap(loadedImage);

                            horizontalCarList.get(position).setUserphoto(loadedImage);

                            //carsViewHolder.mExoPlayer.getLayoutParams().height = carsViewHolder.imgCover.getHeight();
                            //carsViewHolder.mExoPlayer.requestFocus();

                        }
                    }, new ImageLoadingProgressListener() {
                        @Override
                        public void onProgressUpdate(String imageUri, View view, int current, int total) {
                            //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                        }
                    });
        }
        else {
            carsViewHolder.proPic.setImageBitmap(horizontalCarList.get(position).getUserphoto());
        }


*/

    }




    @Override
    public int getItemCount() {
        if (horizontalCarList != null) {
            return horizontalCarList.size();
        } else {
            return 0;
        }
    }

    @Override
    public int getItemViewType(int position) {

        return position % 4;

    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {
        ImageView imgCover1,imgCover2,imgCover3;
        ImageView imgSrc1,imgSrc2,imgSrc3;

        LinearLayout ll;
        CircleImageView proPic;
        TextView tvLikes;
        TextView tv1,tv2,tv3;
        public CarsViewHolder(View view) {
            super(view);

            //tvLikes = view.findViewById(R.id.tv_like);
            //proPic=view.findViewById(R.id.img_pro_pic);

            imgCover1 = view.findViewById(R.id.cover1);
            imgCover2 = view.findViewById(R.id.cover2);
            imgCover3 = view.findViewById(R.id.cover3);

            imgSrc1 = view.findViewById(R.id.img_src1);
            imgSrc2 = view.findViewById(R.id.img_src2);
            imgSrc3 = view.findViewById(R.id.img_src3);

            //tvLikes = view.findViewById(R.id.tv_likes);

            tv1 = view.findViewById(R.id.tv1);
            tv2 = view.findViewById(R.id.tv2);
            tv3 = view.findViewById(R.id.tv3);


        }
    }

    public class CarsViewHolder1 extends RecyclerView.ViewHolder {
        ImageView imgCover1,imgSrc,imgCover2,imgCover3;
        LinearLayout ll;
        CircleImageView proPic;
        TextView tv1,tv2,tv3;
        ImageView imgSrc1,imgSrc2,imgSrc3;

        public CarsViewHolder1(View view) {
            super(view);

            //tvLikes = view.findViewById(R.id.tv_like);
            //proPic=view.findViewById(R.id.img_pro_pic);

            imgCover1 = view.findViewById(R.id.cover1);
            imgCover2 = view.findViewById(R.id.cover2);
            imgCover3 = view.findViewById(R.id.cover3);
            imgSrc1 = view.findViewById(R.id.img_src1);
            imgSrc2 = view.findViewById(R.id.img_src2);
            imgSrc3 = view.findViewById(R.id.img_src3);

            //imgSrc = view.findViewById(R.id.imgSrc);

            tv1 = view.findViewById(R.id.tv1);
            tv2 = view.findViewById(R.id.tv2);
            tv3 = view.findViewById(R.id.tv3);




        }
    }

    public class CarsViewHolder2 extends RecyclerView.ViewHolder {
        ImageView imgCover1,imgSrc,imgCover2,imgCover3;
        LinearLayout ll;
        CircleImageView proPic;
        TextView tvLikes;
        ImageView imgSrc1,imgSrc2,imgSrc3;
        TextView tv1,tv2,tv3;
        public CarsViewHolder2(View view) {
            super(view);

            //tvLikes = view.findViewById(R.id.tv_like);
            //proPic=view.findViewById(R.id.img_pro_pic);

            imgCover1 = view.findViewById(R.id.cover1);
            imgCover2 = view.findViewById(R.id.cover2);
            imgCover3 = view.findViewById(R.id.cover3);
            imgSrc1 = view.findViewById(R.id.img_src1);
            imgSrc2 = view.findViewById(R.id.img_src2);
            imgSrc3 = view.findViewById(R.id.img_src3);

            //imgSrc = view.findViewById(R.id.imgSrc);

            //tvLikes = view.findViewById(R.id.tv_likes);

            tv1 = view.findViewById(R.id.tv1);
            tv2 = view.findViewById(R.id.tv2);
            tv3 = view.findViewById(R.id.tv3);



        }
    }

    public class CarsViewHolder3 extends RecyclerView.ViewHolder {
        ImageView imgCover1,imgSrc,imgCover2,imgCover3;
        LinearLayout ll;
        CircleImageView proPic;
        TextView tvLikes;
        ImageView imgSrc1,imgSrc2,imgSrc3;
        TextView tv1,tv2,tv3;
        public CarsViewHolder3(View view) {
            super(view);

            //tvLikes = view.findViewById(R.id.tv_like);
            //proPic=view.findViewById(R.id.img_pro_pic);

            imgCover1 = view.findViewById(R.id.cover1);
            imgCover2 = view.findViewById(R.id.cover2);
            imgCover3 = view.findViewById(R.id.cover3);
            imgSrc1 = view.findViewById(R.id.img_src1);
            imgSrc2 = view.findViewById(R.id.img_src2);
            imgSrc3 = view.findViewById(R.id.img_src3);

            //imgSrc = view.findViewById(R.id.imgSrc);

            //tvLikes = view.findViewById(R.id.tv_likes);


            tv1 = view.findViewById(R.id.tv1);
            tv2 = view.findViewById(R.id.tv2);
            tv3 = view.findViewById(R.id.tv3);


        }
    }

}
