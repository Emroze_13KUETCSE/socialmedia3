package com.socialmedia3.fartech.socialmedia3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class Profile extends AppCompatActivity {

    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    String lang;
    public ArrayList<PostItem> itemArrayList = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList2 = new ArrayList<>();

    Map<String, PostItem> map = new TreeMap<>();
    private LinearLayoutManager mLayoutManager,mLayoutManager_following,mLayoutManager_followers;
    int counter = 0;
    int REQUEST_STORAGE_PERMISSIONS_CODE = 1;
    private View currentFocusedLayout, oldFocusedLayout;
    private LinearLayoutManager layoutmanager;
    public SimpleExoPlayer exoPlayer;
    int adapterClickedPos = 0;
    ExoAdapterProfile.CarsViewHolder carsViewHolder;
    private int height;
    ExoAdapterProfile adapter;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    JSONArray postJSON;

    private DisplayImageOptions options;


    CollapsingToolbarLayout collapsingToolbar;
    Toolbar toolbar;

    String userName="",ppurl="",uid="";

    LinearLayout llToolbar;

    CircleImageView pro1,pro2;
    TextView tvName1,tvName2;

    ImageView imgEmpty,imgEdit;

    int progresss,timeinsec;
    File loggo;

    DB_SqliteFollowing db_sqliteFollowing;
    String useridd;

    DB_SqliteFavorites db_sqliteFavorites;


    ArrayList<ProfileItem> followerslistFull = new ArrayList<>();
    HashSet<ProfileItem> HashSet_followers = new HashSet<>();
    ArrayList<String> followerslist = new ArrayList<>();


    ArrayList<ProfileItem> followingslistFull = new ArrayList<>();
    ArrayList<String> followingslist = new ArrayList<>();


    LinearLayout llFollowers,llFollowings,llPost;
    TextView tvFollowers,tvFollowings,tvPost;

    ViewPager viewPager;

    RecyclerView lvFollowers,lvFollowings;

    View view0,view1,view2;

    ProfileAdapter followersAdapter;
    ProfileAdapter followingsAdapter;

    static int itemForComment = 0;
    ExoAdapterProfile.CarsViewHolder carsViewHolder_comments;

    String whatsAppText="";

    boolean saveBitmapToFile(File dir, String fileName, Bitmap bm,
                             Bitmap.CompressFormat format, int quality) {

        File imageFile = new File(dir,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);

            bm.compress(format,quality,fos);

            fos.close();

            return true;
        }
        catch (IOException e) {
            Log.e("app",e.getMessage());
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        System.out.println(ppurl);
        ppurl = preference.getString("ppurl","");
        userName = preference.getString("name","");
        uid = preference.getString("uid","");
        lang = preference.getString("lang","");

        useridd = uid;

        System.out.println("only >>>>> uid 6   "+uid);
        db_sqliteFollowing = new DB_SqliteFollowing(getApplicationContext());
        db_sqliteFavorites = new DB_SqliteFavorites(getApplicationContext());
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_profile2)
                .showImageForEmptyUri(R.mipmap.ic_profile2)
                .showImageOnFail(R.mipmap.ic_profile2)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .displayer(new CircleBitmapDisplayer(Color.WHITE, 1))
                .build();


        pro1 = findViewById(R.id.img_pro_pic1);
        pro2 = findViewById(R.id.img_pro_pic2);
        tvName1 = findViewById(R.id.tv_name);
        tvName2 = findViewById(R.id.tv_name2);
        imgEdit = findViewById(R.id.img_edit);

        tvName2.setText(userName);
        tvName1.setText(userName);

        progressBar = findViewById(R.id.progress_bar);


        if(lang.equals("hindi")){

            whatsAppText = getString(R.string.w_hindi);
        }
        else if(lang.equals("tamil")){

            whatsAppText = getString(R.string.w_tamil);
        }
        else if(lang.equals("malayalam")){
             whatsAppText = getString(R.string.w_mala);
        }
        else if(lang.equals("telugu")){
           whatsAppText = getString(R.string.w_telegu);
        }
        else if(lang.equals("kannada")){
             whatsAppText = getString(R.string.w_kannada);
        }
        else if(lang.equals("bengali")){
            whatsAppText = getString(R.string.w_bangla);
        }


        llFollowers = findViewById(R.id.ll3);
        llFollowings = findViewById(R.id.ll2);
        tvFollowers = findViewById(R.id.tv_followers_no);
        tvFollowings = findViewById(R.id.tv_following_no);
        llPost = findViewById(R.id.ll1);
        viewPager = findViewById(R.id.viewpager);
        tvPost = findViewById(R.id.tv_post_no);

        view1 = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.followings, null, false);
        view2 = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.followers, null, false);

        view0 = LayoutInflater.from(
                getBaseContext()).inflate(R.layout.post, null, false);


        lvFollowings = view1.findViewById(R.id.followings);
        lvFollowers = view2.findViewById(R.id.followers);

        recyclerView = view0.findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);

        imgEmpty = view0.findViewById(R.id.img_empty);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);

        ImageLoader.getInstance()
                .displayImage(ppurl,pro1, options, new SimpleImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        //Toast.makeText(context, "Started", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        //Toast.makeText(context, "Failed", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        //Toast.makeText(context, "Complete", Toast.LENGTH_SHORT).show();
                        pro1.setImageBitmap(loadedImage);
                        pro2.setImageBitmap(loadedImage);

                        //carsViewHolder.mExoPlayer.getLayoutParams().height = carsViewHolder.imgCover.getHeight();
                        //carsViewHolder.mExoPlayer.requestFocus();

                    }
                }, new ImageLoadingProgressListener() {
                    @Override
                    public void onProgressUpdate(String imageUri, View view, int current, int total) {
                        //holder.progressBar.setProgress(Math.round(100.0f * current / total));
                    }
                });
        llToolbar = findViewById(R.id.ll_toolbar);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle("");


        llToolbar.setVisibility(View.VISIBLE);
        llToolbar.animate().alpha(0);
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);


        //collapsingToolbar.setTitle("Your Order");
        AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.appbar);

        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
                if (Math.abs(offset) == appBarLayout.getTotalScrollRange()) {

                    System.out.println("only >>>>>>>>> collaps  " + offset);

                    llToolbar.animate().alpha(1);
                }
                else {

                    float oo = offset*(-1);
                    float alpha = oo/appBarLayout.getTotalScrollRange();
                    llToolbar.setVisibility(View.VISIBLE);
                    collapsingToolbar.setTitle("");
                    llToolbar.animate().alpha(alpha);
                    System.out.println("only >>>>>>>>> ex   "+alpha);
                    // Somewhere in between
                }
            }
        });

        jsonParse();

        tvName1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditProfileBottomSheet editProfileBottomSheet = EditProfileBottomSheet.getInstance();
                editProfileBottomSheet.show(getSupportFragmentManager(),"jkds");
            }
        });

        pro1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditProfileBottomSheet editProfileBottomSheet = EditProfileBottomSheet.getInstance();
                editProfileBottomSheet.show(getSupportFragmentManager(),"jkds");
            }
        });

        imgEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditProfileBottomSheet editProfileBottomSheet = EditProfileBottomSheet.getInstance();
                editProfileBottomSheet.show(getSupportFragmentManager(),"jkds");
            }
        });



        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.loooo);

        loggo = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "SocialTest/Logo");

        boolean doSave = true;
        if (!loggo.exists()) {
            doSave = loggo.mkdirs();
        }

        if (doSave) {
            saveBitmapToFile(loggo,"logo.png",bm,Bitmap.CompressFormat.PNG,100);
        }
        else {
            Log.e("app","Couldn't create target directory.");
        }

        //followingslist=db_sqliteFollowing.getlikelist();


        mLayoutManager_followers = new LinearLayoutManager(getApplicationContext());
        lvFollowers.setHasFixedSize(true);
        lvFollowers.setLayoutManager(mLayoutManager_followers);

        mLayoutManager_following = new LinearLayoutManager(getApplicationContext());
        lvFollowings.setHasFixedSize(true);
        lvFollowings.setLayoutManager(mLayoutManager_following);

        viewPager.setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public boolean isViewFromObject(final View view, final Object object) {
                return view.equals(object);
            }

            @Override
            public void destroyItem(final View container, final int position, final Object object) {
                ((ViewPager) container).removeView((View) object);
            }

            @Override
            public Object instantiateItem(final ViewGroup container, final int position) {

                if(position == 0){

                    container.addView(view0);

                    return view0;
                }
                else if(position == 1){
                    container.addView(view1);

                    return view1;
                }
                else{
                    container.addView(view2);

                    return view2;
                }

            }
        });

        llPost.setBackgroundResource(R.drawable.border6);
        llFollowers.setBackgroundColor(Color.parseColor("#ffffff"));
        llFollowings.setBackgroundColor(Color.parseColor("#ffffff"));

        viewPager.setCurrentItem(0);

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 0){


                    llPost.setBackgroundResource(R.drawable.border6);
                    llFollowers.setBackgroundColor(Color.parseColor("#ffffff"));
                    llFollowings.setBackgroundColor(Color.parseColor("#ffffff"));


                }
                else if(position == 1){

                    llFollowings.setBackgroundResource(R.drawable.border6);
                    llPost.setBackgroundColor(Color.parseColor("#ffffff"));
                    llFollowers.setBackgroundColor(Color.parseColor("#ffffff"));


                }
                else{

                    llFollowers.setBackgroundResource(R.drawable.border6);
                    llPost.setBackgroundColor(Color.parseColor("#ffffff"));
                    llFollowings.setBackgroundColor(Color.parseColor("#ffffff"));


                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        llPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                viewPager.setCurrentItem(0,true);
                llPost.setBackgroundResource(R.drawable.border6);
                llFollowers.setBackgroundColor(Color.parseColor("#ffffff"));
                llFollowings.setBackgroundColor(Color.parseColor("#ffffff"));

            }
        });



        llFollowings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llFollowings.setBackgroundResource(R.drawable.border6);
                llPost.setBackgroundColor(Color.parseColor("#ffffff"));
                llFollowers.setBackgroundColor(Color.parseColor("#ffffff"));

                viewPager.setCurrentItem(1,true);
            }
        });


        llFollowers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                llFollowers.setBackgroundResource(R.drawable.border6);
                llPost.setBackgroundColor(Color.parseColor("#ffffff"));
                llFollowings.setBackgroundColor(Color.parseColor("#ffffff"));

                viewPager.setCurrentItem(2,true);
            }
        });

        getFollowers();
    }

    ProfileAdapter.OnCarItemClickListener listenerFollowers = new ProfileAdapter.OnCarItemClickListener() {
        @Override
        public void onllClicked(int position, ProfileItem item, ProfileAdapter.CarsViewHolder holder) {

        }

        @Override
        public void onFollowClicked(final int position, ProfileItem item, ProfileAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                if (viewHolder.imgAdd.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.plus2).getConstantState()) {
                    db_sqliteFollowing.Inserttoalertlist(followerslistFull.get(position).getUid());
                    viewHolder.imgAdd.setImageResource(R.drawable.added);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/followings.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", followerslistFull.get(position).getUid());


                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }else {
                    db_sqliteFollowing.Delete(followerslistFull.get(position).uid);
                    viewHolder.imgAdd.setImageResource(R.drawable.plus2);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/unfollow.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", followerslistFull.get(position).getUid());


                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }

        }
    };

    ProfileAdapter.OnCarItemClickListener listenerFollowings = new ProfileAdapter.OnCarItemClickListener() {
        @Override
        public void onllClicked(int position, ProfileItem item, ProfileAdapter.CarsViewHolder holder) {

        }

        @Override
        public void onFollowClicked(final int position, ProfileItem item, ProfileAdapter.CarsViewHolder viewHolder) {

            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                if (viewHolder.imgAdd.getDrawable().getConstantState() == getResources().getDrawable(R.drawable.plus2).getConstantState()) {
                    db_sqliteFollowing.Inserttoalertlist(followingslistFull.get(position).getUid());
                    viewHolder.imgAdd.setImageResource(R.drawable.added);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/followings.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", followingslistFull.get(position).getUid());


                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }else {
                    db_sqliteFollowing.Delete(followingslistFull.get(position).uid);
                    viewHolder.imgAdd.setImageResource(R.drawable.plus2);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/unfollow.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", followingslistFull.get(position).getUid());


                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }

        }
    };

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    public void populateCarList(){
        counter = 0;
        for(int i = counter ; i < counter+10; i++){
            if(i == itemArrayList.size()){
                break;
            }
            itemArrayList.get(i).setPrepared(true);
            itemArrayList2.add(itemArrayList.get(i));
        }

        counter=counter+10;
        adapter = new ExoAdapterProfile(itemArrayList2,getApplicationContext(),height);
        adapter.setOnCarItemClickListener(listener);


        recyclerView.setAdapter(adapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(final RecyclerView recyclerView, int scrollState) {

                if (!recyclerView.canScrollVertically(1)) {

                    progressBar.setVisibility(View.VISIBLE);
                    final Handler handler=new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            progressBar.setVisibility(View.GONE);


                            if(counter <= itemArrayList.size()){
                                for(int i = counter ; i < counter+10; i++){
                                    if(i == itemArrayList.size()){
                                        System.out.println("only size"+itemArrayList.size());
                                        break;
                                    }
                                    else {
                                        itemArrayList2.add(itemArrayList.get(i));
                                    }

                                }

                                counter=counter+10;

                                adapter.setItemlist(itemArrayList2);
                                adapter.notifyDataSetChanged();
                            }


                        }
                    },500);
                }

                if (scrollState == RecyclerView.SCROLL_STATE_IDLE) {

                    layoutmanager = (LinearLayoutManager)recyclerView.getLayoutManager();

                    int firstVisiblePosition = layoutmanager.findFirstCompletelyVisibleItemPosition();

                    int ddd = layoutmanager.findFirstVisibleItemPosition();
                    int eee = layoutmanager.findLastCompletelyVisibleItemPosition();
                    int fff = layoutmanager.findLastVisibleItemPosition();

                    System.out.println("only ======================================= "+firstVisiblePosition);
                    System.out.println("only 1st full visible "+firstVisiblePosition);
                    System.out.println("only 1st partial visible "+ddd);
                    System.out.println("only last full visible "+eee);
                    System.out.println("only last partial visible "+fff);

                    if (firstVisiblePosition >= 0 || fff >= 0)
                    {
                        if (oldFocusedLayout != null)
                        {
                            System.out.println("only remove "+firstVisiblePosition);
                            if(exoPlayer != null){

                                carsViewHolder.play.setVisibility(View.VISIBLE);
                                carsViewHolder.imgCover.setVisibility(View.VISIBLE);
                                carsViewHolder.mExoPlayer.setVisibility(View.GONE);

                            }
                        }


                    }
                    if(exoPlayer != null){

                        exoPlayer.setPlayWhenReady(false);
                    }

                    currentFocusedLayout = layoutmanager.findViewByPosition(firstVisiblePosition);
                    //VideoView vv_dashboard = (VideoView)currentFocusedLayout.findViewById(R.id.vv_dashboard);
                    ////to play video of selected recylerview, videosData is an array-list which is send to recyclerview adapter to fill the view. Here we getting that specific video which is displayed through recyclerview.
                    //playVideo(videosData.get(positionView));

                    oldFocusedLayout = currentFocusedLayout;




                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });
    }

    ExoAdapterProfile.OnCarItemClickListener listener = new ExoAdapterProfile.OnCarItemClickListener() {
        @Override
        public void onllClicked(final int position, PostItem item, final ExoAdapterProfile.CarsViewHolder holder) {


            System.out.println("only  >>>>>>>>> "+holder.imgCover.getHeight()+" "+holder.imgCover.getWidth());

            /*DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;



            holder.cardmore.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.play.setVisibility(View.GONE);

            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            TrackSelection.Factory videoTrackSelectionFactory =
                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

            if(exoPlayer != null){
                exoPlayer.setPlayWhenReady(false);
                exoPlayer.stop();
            }

            exoPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

            holder.mExoPlayer.setPlayer(exoPlayer);



            exoPlayer.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                    System.out.println("only 1 "+position);
                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                    System.out.println("only 2 "+position);
                }

                @Override
                public void onLoadingChanged(boolean isLoading) {
                    System.out.println("only 3 "+position);
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                    if (playWhenReady && playbackState == Player.STATE_READY) {
                        System.out.println("only  playing  "+position);
                        // media actually playing
                        holder.progressBar.setVisibility(View.GONE);
                        holder.play.setVisibility(View.GONE);
                        holder.imgCover.setVisibility(View.GONE);
                        holder.mExoPlayer.setVisibility(View.VISIBLE);
                        holder.mExoPlayer.getLayoutParams().height = holder.imgCover.getHeight();
                        holder.mExoPlayer.requestFocus();


                    } else if (playWhenReady) {
                        // might be idle (plays after prepare()),
                        // buffering (plays when data available)
                        // or ended (plays when seek away from end)
                        if (playbackState==Player.STATE_ENDED){
                            System.out.println("only  Ended  "+position);
                        }
                        else if (playbackState==Player.STATE_BUFFERING){
                            System.out.println("only Buffering "+position);
                        }
                        else if (playbackState==Player.STATE_IDLE){
                            System.out.println("only Idle "+position);
                        }
                    } else {
                        // player paused in any state
                        System.out.println("only  paused  "+position);



                        //horizontalCarList.get(position).setPlayWhenReady(true);

                    }

                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {

                }

                @Override
                public void onPositionDiscontinuity(int reason) {

                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {

                }
            });


            CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

            MediaSource audioSource = new ExtractorMediaSource(Uri.parse(itemArrayList2.get(position).getVideoUrl()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


            exoPlayer.seekTo(position, 0);

            exoPlayer.prepare(audioSource, true, false);

            System.out.println("only  here  "+itemArrayList2.get(position).getPrepared());

            exoPlayer.setPlayWhenReady(true);


            adapterClickedPos=position;
            carsViewHolder = holder;
*/

            //System.out.println("only ??????????????????????????????????????? select "+position+itemArrayList2.get(position).getPlayWhenReady());
            //adapter.setItemlist(itemArrayList2);
            //adapter.notifyDataSetChanged();

            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                holder.play.setVisibility(View.GONE);
            }else {


                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);


                float views = 0;
                String vvv="";
                views = itemArrayList.get(position).getViews();
                views=views+1;
                itemArrayList.get(position).setViews((int)views);
                if(views<=999){
                    int vvvvv = (int)views;
                    vvv = String.valueOf(vvvvv)+" views";
                }
                else if(views >= 1000){
                    views = views/1000;
                    vvv = String.valueOf(views)+"k views";
                }else if(views >= 1000000){
                    views = views/1000000;
                    vvv = String.valueOf(views)+"M views";
                }

                holder.tvViews.setText(vvv);


            }
        }

        @Override
        public void onCoverClicked(int position, PostItem item, ExoAdapterProfile.CarsViewHolder holder) {


            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                Intent intent = new Intent(getApplicationContext(),ImageDisplay.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("postId", item.getPostid());
                intent.putExtra("imgUrl", item.getPpurl());
                intent.putExtra("userName", item.getUsername());
                intent.putExtra("otherId", item.getUid());
                intent.putExtra("coverUrl", item.getImagelocation());
                intent.putExtra("likes", item.getLikes());
                intent.putExtra("comments", item.getCommentsno());
                intent.putExtra("shares", item.getShares());
                intent.putExtra("downloads", item.getDownloadsno());
                intent.putExtra("txt", item.getText());
                intent.putExtra("views", item.getViews());

                startActivity(intent);
            }else {
                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item.isFollowing());
                            object.put("isLiked",item.isLiked());
                            object.put("views",item.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }

            float views = 0;
            String vvv="";
            views = itemArrayList.get(position).getViews();
            views=views+1;
            itemArrayList.get(position).setViews((int)views);
            if(views<=999){
                int vvvvv = (int)views;
                vvv = String.valueOf(vvvvv)+" views";
            }
            else if(views >= 1000){
                views = views/1000;
                vvv = String.valueOf(views)+"k views";
            }else if(views >= 1000000){
                views = views/1000000;
                vvv = String.valueOf(views)+"M views";
            }

            holder.tvViews.setText(vvv);


        }

        @Override
        public void onWhatsAppClicked(final int position, PostItem item, ExoAdapterProfile.CarsViewHolder viewHolder) {
            if (!itemArrayList2.get(position).getTime().contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(position).getText()+"\n"+whatsAppText);
                }
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Title", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }


                int shar = itemArrayList2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                itemArrayList2.get(position).setShares(shar);
                itemArrayList.get(position).setShares(shar);
                adapter.setItemlist(itemArrayList2);


                StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }

                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("postid", itemArrayList2.get(position).getPostid());

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringrequest);

            }else {


                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                int shar = itemArrayList2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));


                downloadvideoWhatsApp downloadvideo2=new downloadvideoWhatsApp();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList2.get(position).getVideoUrl(),time2,String.valueOf(position));

            }
        }

        @Override
        public void onLikeClicked(final int position, PostItem item, ExoAdapterProfile.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {

            }else {
                viewHolder.imgLike.setSelected(!viewHolder.imgLike.isSelected());
                int count = db_sqliteFavorites.get_check_List_Favorite(itemArrayList2.get(position).getPostid());

                System.out.println("only f   profile  "+itemArrayList2.get(position).getPostid());

                if (count == 0) {
                    viewHolder.imgLike.setImageResource(R.drawable.love2);
                    int lik = itemArrayList2.get(position).getLikes();
                    viewHolder.tvLikes.setText(String.valueOf(lik + 1));
                    itemArrayList2.get(position).setLikes(lik+1);
                    itemArrayList.get(position).setLikes(lik+1);
                    adapter.setItemlist(itemArrayList2);
                    itemArrayList.get(position).setLiked(true);
                    itemArrayList2.get(position).setLiked(true);

                    adapter.setItemlist3(true,position);

                    db_sqliteFavorites.Inserttoalertlist(itemArrayList2.get(position).getPostid());

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/sociallikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                } else {
                    viewHolder.imgLike.setImageResource(R.drawable.dislike);
                    db_sqliteFavorites.Delete(itemArrayList2.get(position).getPostid());

                    int lik = itemArrayList2.get(position).getLikes();
                    viewHolder.tvLikes.setText(String.valueOf(lik - 1));
                    itemArrayList2.get(position).setLikes(lik-1);
                    itemArrayList.get(position).setLikes(lik-1);
                    adapter.setItemlist(itemArrayList2);
                    itemArrayList.get(position).setLiked(false);
                    itemArrayList2.get(position).setLiked(false);

                    adapter.setItemlist3(false,position);

                    if (lik == 0) {
                        viewHolder.tvLikes.setText("Like");
                    }

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialdislikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }
        }

        @Override
        public void onCommentsClicked(int position, PostItem item, ExoAdapterProfile.CarsViewHolder holder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                itemForComment = position;
                carsViewHolder_comments = holder;
                Intent intent=new Intent(getApplicationContext(),Comments.class);
                intent.putExtra("postid",itemArrayList2.get(position).getPostid());
                startActivityForResult(intent,11);
            }
        }

        @Override
        public void onDownloadsClicked(final int position, PostItem item, ExoAdapterProfile.CarsViewHolder viewHolder) {
            viewHolder.progressBar_down.setVisibility(View.VISIBLE);

            if (!itemArrayList2.get(position).getTime().contains("/")){


                try{
                    Bitmap bm = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();

                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Flash/");

                    boolean doSave = true;
                    if (!f.exists()) {
                        doSave = f.mkdirs();
                    }

                    if (doSave) {
                        saveBitmapToFile(f,itemArrayList2.get(position).getPostid()+".png",bm,Bitmap.CompressFormat.PNG,100);
                    }
                    else {
                        Log.e("app","Couldn't create target directory.");
                    }


                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    int down = itemArrayList2.get(position).getDownloadsno();
                    viewHolder.tvDownloads.setText(String.valueOf(down+1));
                    itemArrayList2.get(position).setDownloadsno(down+1);
                    itemArrayList.get(position).setDownloadsno(down+1);
                    adapter.setItemlist(itemArrayList2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {




                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("postid",itemArrayList2.get(position).postid);

                            return params;
                        }
                    };

                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                }catch (NullPointerException e){

                }




            }else {

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }


                int down = itemArrayList2.get(position).getDownloadsno();
                viewHolder.tvDownloads.setText(String.valueOf(down+1));

                downloadvideooo downloadvideo2=new downloadvideooo();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList2.get(position).getVideoUrl(),time2,String.valueOf(position));





            }





        }

        @Override
        public void onMoreClicked(int position, PostItem item, ExoAdapterProfile.CarsViewHolder holder) {

            if(holder.cardmore.getVisibility() == View.VISIBLE){
                holder.cardmore.animate().alpha(0);
                holder.cardmore.setVisibility(View.GONE);
            }else {
                holder.cardmore.setVisibility(View.VISIBLE);
                holder.cardmore.animate().alpha(1);
            }

        }

        @Override
        public void onShareClicked(int position, PostItem item, ExoAdapterProfile.CarsViewHolder viewHolder) {

            if (!itemArrayList2.get(position).time.contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Image", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

            }else {

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                downloadvideoshare downloadvideo=new downloadvideoshare();
                downloadvideo.setdownloadlayout(viewHolder.ll_share);
                downloadvideo.setProgressbar(viewHolder.progressBar_share);
                downloadvideo.execute(itemArrayList2.get(position).getVideoUrl(), time2,String.valueOf(position));

            }

        }

        @Override
        public void onReportClicked(final int position, PostItem item, ExoAdapterProfile.CarsViewHolder viewHolder) {

            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/removepost.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            itemArrayList2.remove(position);
                            itemArrayList.remove(position);
                            adapter.setItemlist(itemArrayList2);
                            adapter.notifyDataSetChanged();
                        }
                    }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();
                    params.put("postid", itemArrayList2.get(position).getPostid());

                    return params;
                }
            };
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    1*2000,
                    2,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            ));

            RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringRequest);
            viewHolder.cardmore.animate().alpha(0);
            viewHolder.cardmore.setVisibility(View.GONE);
        }

        @Override
        public void onAddClicked(final int position, PostItem item, ExoAdapterProfile.CarsViewHolder viewHolder) {

        }

        @Override
        public void onProPicClicked(int position, PostItem item, ExoAdapterProfile.CarsViewHolder holder) {

        }
    };

    public void jsonParse(){
        String url = "http://heyapp.in/showposts.php";

        progressBar.setVisibility(View.VISIBLE);
        System.out.println("rrrrrrrrrrrrrrrrrrr**************");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("eventss");

                            for(int i = 0 ; i < array.length();i++){
                                JSONObject object = array.getJSONObject(i);

                                /*object.getString("language").equals("bengali")||
                                        object.getString("language").equals("hindi")||
                                        object.getString("language").equals("tamil")*/
                                if(!object.getString("uid").isEmpty() && !object.getString("language").isEmpty()){
                                    if(useridd.equals(object.getString("uid")) && lang.equals(object.getString("language"))){
                                        PostItem item = new PostItem(getApplicationContext());
                                        item.setUid(object.getString("uid"));
                                        item.setPostid(object.getString("postid"));
                                        item.setUsername(object.getString("username"));
                                        item.setTime(object.getString("time"));
                                        item.setImagelocation("https://static.heyapp.in/file/app-media/"+object.getString("imagelocation"));

                                        item.setText(object.getString("text"));
                                        item.setDownloadsno(object.getInt("downloads"));
                                        item.setLikes(object.getInt("likes"));
                                        item.setShares(object.getInt("shares"));
                                        item.setCommentsno(object.getInt("comments"));
                                        item.setViews(object.getInt("views"));



                                        String[] splitter =item.getTime().split("/");
                                        String time1 = splitter[0];
                                        String time2="";
                                        String time = "";

                                        try {
                                            time2 = splitter[1];
                                            //time = time1.substring(0, time1.length() - 1);
                                        }catch (ArrayIndexOutOfBoundsException e){

                                        }

                                        if(item.getTime().contains("/")){
                                            time = time1.substring(0, time1.length() - 1);
                                            System.out.println("only  -------     " + object.getString("username")+""+time);

                                        }else {
                                            time = item.getTime();
                                            System.out.println("only  ------->>>>>>>>     " + object.getString("username")+""+time);
                                        }


                                        item.setVideoUrl("https://static.heyapp.in/file/app-media/"+time2);

                                        String temp = object.getString("ppurl");

                                        item.setPpurl(temp);

                                        int count2 = db_sqliteFavorites.get_check_List_Favorite(item.getPostid());
                                        if (count2 == 0) {
                                            item.setLiked(false);
                                        } else {
                                            item.setLiked(true);
                                        }

                                        if (item.getUid().equals(useridd)){

                                        }else {
                                            int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());
                                            if (count3 == 0) {
                                                item.setFollowing(false);
                                            } else {
                                                item.setFollowing(true);
                                            }
                                        }

                                        if(!item.getText().isEmpty()){
                                            String[] www = item.getText().split("==");// i converted string to String[] array


                                            String text = www[0];


                                            System.out.println("only >> hash  Text          ^^^^^^^^^^^^^^    "+text);

                                            try{
                                                String hashes = www[1];

                                                if(!hashes.isEmpty()){
                                                    List<String> word_list=new ArrayList<String>();

                                                    String[] words = hashes.split("#");// i converted string to String[] array

                                                    System.out.println(words.length);
                                                    String fullText="";
                                                    for(int i1=0;i1<words.length;i1++){

                                                        if(!words[i1].isEmpty()){
                                                            word_list.add(words[i1]);
                                                        }

                                                        System.out.println("only >> hash  ^^^^^^^^^^^^^^    "+words.length);
                                                    }


                                                    item.setWord_list(word_list);

                                                }
                                            }catch (ArrayIndexOutOfBoundsException e){
                                                //carsViewHolder.hashText.setVisibility(View.GONE);
                                                String hashes = text;
                                                //System.out.println("only >> hash  ^^^^^^^^^^^^^^    "+hashes);
                                                if(!hashes.isEmpty()){
                                                    List<String> word_list=new ArrayList<String>();

                                                    String[] words = hashes.split("#");// i converted string to String[] array

                                                    System.out.println(words.length);
                                                    String fullText="";
                                                    for(int i1=0;i1<words.length;i1++){
                                                        if(words[i1].startsWith("#")){
                                                            if(!words[i1].isEmpty()){
                                                                word_list.add(words[i1]);
                                                            }

                                                            String next = "<font color='#FC4C21'>"+words[i1]+"</font>";
                                                            fullText = fullText + next;
                                                        }
                                                        else {
                                                            fullText = fullText+words[i1];
                                                            if(!words[i1].isEmpty()){
                                                                word_list.add(words[i1]);
                                                            }
                                                        }

                                                    }


                                                    item.setWord_list(word_list);

                                                }
                                            }


                                            item.setText(text);


                                        }

                                        map.put(time,item);
                                    }
                                }



                            }

                            Set keys = map.keySet();
                            final Iterator it = keys.iterator();

                            //System.out.println("only All keys are: " + keys);

                            ArrayList<PostItem> aaaa = new ArrayList<>();

                            postJSON = new JSONArray();

                            while(it.hasNext()){
                                Object key = it.next();
                                //System.out.println("only "+key + ": " + map.get(key).getUsername());


                                PostItem item = new PostItem(getApplicationContext());

                                item.setUid(map.get(key).getUid());

                                item.setPostid(map.get(key).getPostid());

                                item.setUsername(map.get(key).getUsername());

                                item.setTime(map.get(key).getTime());

                                item.setImagelocation(map.get(key).getImagelocation());

                                item.setVideoUrl(map.get(key).getVideoUrl());

                                item.setPpurl(map.get(key).getPpurl());

                                item.setText(map.get(key).getText());

                                item.setCommentsno(map.get(key).getCommentsno());

                                item.setDownloadsno(map.get(key).getDownloadsno());

                                item.setLikes(map.get(key).getLikes());

                                item.setShares(map.get(key).getShares());

                                item.setPlaying(false);

                                item.setFollowing(map.get(key).isFollowing());

                                item.setLiked(map.get(key).isLiked());

                                item.setWord_list(map.get(key).getWord_list());

                                item.setViews(map.get(key).getViews());

                                aaaa.add(item);


                            }

                            for(int i = aaaa.size()-1;i > -1 ;i--) {
                                itemArrayList.add(aaaa.get(i));
                                JSONObject object = new JSONObject();
                                PostItem item = new PostItem(getApplicationContext());

                                item.setUsername(aaaa.get(i).getUsername());
                                object.put("userName",item.getUsername());

                                item.setUid(aaaa.get(i).getUid());
                                object.put("uid",item.getUid());

                                item.setPostid(aaaa.get(i).getPostid());
                                object.put("postid",item.getPostid());

                                item.setTime(aaaa.get(i).getTime());
                                object.put("time",item.getTime());

                                item.setImagelocation(aaaa.get(i).getImagelocation());
                                object.put("imgUrl",item.getImagelocation());

                                item.setVideoUrl(aaaa.get(i).getVideoUrl());
                                object.put("videoUrl",item.getVideoUrl());

                                item.setPpurl(aaaa.get(i).getPpurl());
                                object.put("ppurl",item.getPpurl());

                                item.setText(aaaa.get(i).getText());
                                object.put("text",item.getText());

                                item.setCommentsno(aaaa.get(i).getCommentsno());
                                object.put("comments",item.getCommentsno());

                                item.setDownloadsno(aaaa.get(i).getDownloadsno());
                                object.put("downloads",item.getDownloadsno());

                                item.setLikes(aaaa.get(i).getLikes());
                                object.put("likes",item.getLikes());

                                item.setShares(aaaa.get(i).getShares());
                                object.put("shares",item.getShares());
                                item.setFollowing(aaaa.get(i).isFollowing());
                                object.put("isFollowing",item.isFollowing());

                                item.setLiked(aaaa.get(i).isLiked());
                                object.put("isLiked",item.isLiked());

                                item.setViews(aaaa.get(i).getViews());
                                object.put("views",item.getViews());

                                postJSON.put(object);

                                if(useridd.equals(item.getUid())){
                                    System.out.println("only " + " @@@@@@@@@@@@@@@@@@@@@ " + aaaa.get(i).getUid()+"   "+useridd);
                                }
                                else {
                                    System.out.println("only " + " @@@@@@@@@@@@@@@@@@@@@ " + aaaa.get(i).getUid()+"   ");
                                }

                               /* if (!aaaa.get(i).getImagelocation().equals("https://static.heyapp.in/file/app-media/")) {
                                    System.out.println("only " + " @@@@@@@@@@@@@@@@@@@@@ " + aaaa.get(i).getImagelocation());
                                }
                                else {
                                    System.out.println("only " + " ##################### " + aaaa.get(i).getImagelocation());
                                }*/

                            }

                            progressBar.setVisibility(View.GONE);



                            System.out.println("only >>>>>>>>>>>> size"+itemArrayList.size());
                            //getImageBitmap();
                            progressBar.setVisibility(View.GONE);
                            if(itemArrayList.size() == 0){
                                imgEmpty.setVisibility(View.VISIBLE);
                            }else {
                                imgEmpty.setVisibility(View.GONE);
                                tvPost.setText(String.valueOf(itemArrayList.size()));
                                populateCarList();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+error.toString());
                    }
                }
        ) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", ACCESS_TOKEN);
                return params;
            }*/

            /*@Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phone);
                params.put("otp", otp);
                params.put("timestamp", timeStamp);
                params.put("fcm_token", fcm_token);


                return params;
            }*/
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                //statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    class downloadvideooo extends AsyncTask<String,Integer,Void> {
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        String videoname;
        int pos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Void doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                pos=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            circleProgressBar.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.VISIBLE);




            int down = itemArrayList2.get(pos).getDownloadsno();
            itemArrayList2.get(pos).setDownloadsno(down+1);
            itemArrayList.get(pos).setDownloadsno(down+1);
            adapter.setItemlist(itemArrayList2);

            StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {




                        }
                    }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();

                    params.put("postid",itemArrayList2.get(pos).postid);

                    return params;
                }
            };

            RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringrequest);


//            ddd ddd=new ddd();
//            ddd.setdownloadlayout(linearLayout);
//            ddd.setProgressbar(circleProgressBar);
//            ddd.execute(videoname);


        }
    }

    class downloadvideoWhatsApp extends AsyncTask<String,Integer,Boolean> {

        int positionn;
        String videoname;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
            progresss=0;
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                positionn=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){


                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);

                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList2.get(positionn).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(positionn).getText()+"\n"+whatsAppText);
                }
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                    int share = itemArrayList2.get(positionn).getShares();
                    itemArrayList2.get(positionn).setShares(share+1);
                    itemArrayList.get(positionn).setShares(share+1);
                    adapter.setItemlist(itemArrayList2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(positionn).getPostid());

                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }






                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.linearLayout.setVisibility(View.VISIBLE);

            }






        }


    }

    class downloadvideoshare extends AsyncTask<String,Integer,Boolean> {

        String videoname;
        int position;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                position=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");

                urlConnection.setRequestProperty("Accept-Encoding","identity");
                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/Flash").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/Flash/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;

                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);
                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){



                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/Flash/" +videoname);
                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                if (!itemArrayList2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(position).getText()+"\n"+whatsAppText);
                }
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);
            }






        }


    }


    public void getFollowers(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://heyapp.in/showfollowings.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObjecct = new JSONObject(response);
                            JSONArray jsonArraay = jsonObjecct.getJSONArray("eventss");

                            followingslist.clear();
                            followerslist.clear();
                            for(int i = 0 ; i < jsonArraay.length(); i++){
                                JSONObject obj = jsonArraay.getJSONObject(i);
                                String uid = obj.getString("uid");
                                String followers = obj.getString("Followers");

                                String followings = obj.getString("Followings");


                                if (uid.equals(useridd)){
                                    if (!followers.isEmpty()) {
                                        String[] followersss = followers.split("=");

                                        for (int j = 0; j < followersss.length; j++) {
                                            if (followersss[j] != null && !followersss[j].equals("")){
                                                followerslist.add(followersss[j]);
                                            }
                                        }
                                    }

                                    if (!followings.isEmpty()) {
                                        String[] followersss = followings.split("=");


                                        for (int j = 0; j < followersss.length; j++) {

                                            if (followersss[j] != null && !followersss[j].equals("")){
                                                followingslist.add(followersss[j]);
                                                System.out.println("only >> fff    "+followersss[j]);
                                            }
                                        }
                                    }


                                }


                            }

                            ///tvFollowers.setText(String.valueOf(followerslist.size()));
                            //tvFollowings.setText(String.valueOf(followingslist.size()));
                            getUser();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Uid",useridd);
                params.put("Following", itemArrayList2.get(position).getUid());


                return params;
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    public void getUser(){
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://heyapp.in/showusers.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObjecct = new JSONObject(response);
                            JSONArray jsonArraay = jsonObjecct.getJSONArray("eventss");

                            for(int i = 0 ; i < jsonArraay.length(); i++){
                                JSONObject obj = jsonArraay.getJSONObject(i);
                                String uid = obj.getString("uid");

                                if (followerslist.contains(uid)){
                                    String ppurll;
                                    String userrname;
                                    String uiddi;
                                    userrname = obj.getString("Username");
                                    uiddi = obj.getString("uid");
                                    ppurll = obj.getString("photourl");

                                    ProfileItem item = new ProfileItem();
                                    item.setPpurl(ppurll);
                                    item.setUid(uiddi);
                                    item.setUserrname(userrname);

                                    followerslistFull.add(item);


                                    System.out.println("only f  <<<<<<<<<<<<<<<<<<<<   "+userrname);

                                }

                                if (followingslist.contains(uid)){
                                    String ppurll;
                                    String userrname;
                                    String uiddi;
                                    userrname = obj.getString("Username");
                                    uiddi = obj.getString("uid");
                                    ppurll = obj.getString("photourl");


                                    ProfileItem item = new ProfileItem();
                                    item.setPpurl(ppurll);
                                    item.setUid(uiddi);
                                    item.setUserrname(userrname);

                                    followingslistFull.add(item);
                                    System.out.println("only >> fff   uid        "+uid);

                                    String first = preference.getString("profile_first","");

                                    if(first.equals("")){
                                        db_sqliteFollowing.Inserttoalertlist(uid);
                                    }
                                }
                            }

                            followersAdapter = new ProfileAdapter(followerslistFull,getApplicationContext());
                            followersAdapter.setOnCarItemClickListener(listenerFollowers);
                            lvFollowers.setAdapter(followersAdapter);

                            followingsAdapter = new ProfileAdapter(followingslistFull,getApplicationContext());
                            followingsAdapter.setOnCarItemClickListener(listenerFollowings);
                            lvFollowings.setAdapter(followingsAdapter);

                            tvFollowings.setText(String.valueOf(followingslistFull.size()));
                            tvFollowers.setText(String.valueOf(followerslistFull.size()));

                            editor.putString("profile_first","ok");
                            editor.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Uid",useridd);
                params.put("Following", itemArrayList2.get(position).getUid());


                return params;
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 11){

            int counter = data.getIntExtra("commentCounter",0);

            carsViewHolder_comments.tvComments.setText(String.valueOf(counter));
            System.out.println("only ff result back "+counter);
        }
    }
}
