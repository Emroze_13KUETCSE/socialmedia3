package com.socialmedia3.fartech.socialmedia3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class HashTagAdapter extends RecyclerView.Adapter<HashTagAdapter.CarsViewHolder>{

    public ArrayList<String> horizontalCarList;
    Context context;
    private OnCarItemClickListener listener;


    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onWhatsAppClicked(int position, String item,CarsViewHolder holder);
    }
    public void setOnCarItemClickListener(OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public HashTagAdapter(ArrayList<String> horizontalGrocderyList, Context context){
        this.horizontalCarList= horizontalGrocderyList;
        this.context = context;


    }

    public void setItemlist(ArrayList<String> list) {
        horizontalCarList = list;
    }


    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.hash_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CarsViewHolder carsViewHolder, final int position) {


        carsViewHolder.tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onWhatsAppClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });


        carsViewHolder.tv.setText(horizontalCarList.get(position));


    }






    @Override
    public void onViewRecycled(@NonNull CarsViewHolder holder) {
        int position = holder.getAdapterPosition();
        System.out.println("only recycled "+position);


        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        if (horizontalCarList != null) {
            return horizontalCarList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {

        TextView tv;


        public CarsViewHolder(View view) {
            super(view);
            tv=view.findViewById(R.id.tv);

        }
    }
}
