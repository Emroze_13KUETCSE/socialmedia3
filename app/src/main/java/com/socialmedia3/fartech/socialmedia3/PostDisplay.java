package com.socialmedia3.fartech.socialmedia3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Base64;
import android.view.Display;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.greenfrvr.hashtagview.HashtagView;
import com.makeramen.roundedimageview.RoundedDrawable;
import com.makeramen.roundedimageview.RoundedImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PostDisplay extends AppCompatActivity {

    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;


    TextView tvPost,tv1,tv2,tv3,tv4,tv5,tv6,tv7;
    LinearLayout rlEmpty;
    RoundedImageView imgDisplay;
    Bitmap bitmap;
    EditText etText;

    String useridd,lang;

    String str1,str2,str3,str4,str5,str6,str7;

    String userid,username,ppurl;

    String language;
    String fileid2;
    String accountId = "0023d17fbbe68840000000001"; // Obtained from your B2 account page.
    String applicationKey = "K002OQPFP320nWvEwH+hRuSZerk2jBI"; // Obtained from your B2 account page.
    HttpURLConnection connectionn = null;
    String headerForAuthorizeAccount = "Basic " + Base64.encodeToString((accountId + ":" + applicationKey).getBytes(),Base64.DEFAULT);

    String apiUrl = ""; // Provided by b2_authorize_account
    String accountAuthorizationToken = ""; // Provided by b2_authorize_account
    String bucketId = ""; // The ID of the bucket you want to upload your file to
    String postParams="";
    byte postData[];
    HttpURLConnection connectionuploadurl = null;
    String currenttime;
    String uploadUrl = ""; // Provided by b2_get_upload_url
    String uploadAuthorizationToken = ""; // Provided by b2_get_upload_url
    String fileName = ""; // The name of the file you are uploading
    String contentType = ""; // The content type of the file
    String sha1 = ""; // SHA1 of the file you are uploading
    byte[] fileData;
    HttpURLConnection connectionupload = null;
    String json = null;
    String jsonResponse;

    String fileid;
    Response.Listener<String> responseListener;

    //String url;


    //ProgressBar circleProgressBar;
    RelativeLayout rl;


    String fileName2;

    int progresss=0;

    String type;

    String video_path="";

    HashtagView hashtagView;

    List<String> word_list=new ArrayList<String>();

    int counter=0;

    RecyclerView hashListView;
    HashTagAdapter hashTagAdapter;
    public ArrayList<String> hashList,hashList2;

    String hashUrl =  "http://heyapp.in/hashtags.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_display);

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        useridd = preference.getString("uid","");
        userid = useridd;
        lang = preference.getString("lang","");
        ppurl = preference.getString("ppurl","");
        username = preference.getString("name","");

        language = lang;

        final Intent intent=getIntent();

        type=intent.getStringExtra("type");



        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv3 = findViewById(R.id.tv3);
        tv4 = findViewById(R.id.tv4);
        tv5 = findViewById(R.id.tv5);
        tv6 = findViewById(R.id.tv6);
        tv7 = findViewById(R.id.tv7);
        etText = findViewById(R.id.et_text);
        tvPost = findViewById(R.id.tv_post);
        rl = findViewById(R.id.rl);
        rlEmpty = findViewById(R.id.ll_empty);
        imgDisplay = findViewById(R.id.img_display);
        hashListView = findViewById(R.id.recycler_view);
        hashtagView = findViewById(R.id.hash_text);


        hashtagView.setData(word_list);
        hashtagView.setDynamicMode(true);

        bitmap = BitmapFactory.decodeResource(getResources(),
                R.drawable.vv4);

        if(type.equals("video")){

            video_path=intent.getStringExtra("uri");
            Bitmap btm= ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Video.Thumbnails.MINI_KIND);
            bitmap = ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Images.Thumbnails.MINI_KIND);

            System.out.println("only path >> 2"+video_path);
        }
        else {
            byte[] byteArray = intent.getByteArrayExtra("Bitmap");
            bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        }


        responseListener= new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    boolean success= jsonObject.getBoolean("success");
                    if (success) {
                        finish();
                        Toast.makeText(PostDisplay.this, "Uploaded", Toast.LENGTH_SHORT).show();
                        Intent intent1=new Intent(PostDisplay.this,Profile.class);
                        startActivity(intent1);

                    } else {
                        Toast.makeText(PostDisplay.this, "Please Retry Again", Toast.LENGTH_SHORT).show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        if (bitmap!=null){
            imgDisplay.setImageBitmap(bitmap);
        }


        if(lang.equals("hindi")){
            tv1.setText(getString(R.string.hindi_1));
            tv2.setText(getString(R.string.hindi_2));
            tv3.setText(getString(R.string.hindi_3));
            tv4.setText(getString(R.string.hindi_4));
            tv5.setText(getString(R.string.hindi_5));
            tv6.setText(getString(R.string.hindi_6));
            tv7.setText(getString(R.string.hindi_7));
        }
        else if(lang.equals("tamil")){
            tv1.setText(getString(R.string.tamil_1));
            tv2.setText(getString(R.string.tamil_2));
            tv3.setText(getString(R.string.tamil_3));
            tv4.setText(getString(R.string.tamil_4));
            tv5.setText(getString(R.string.tamil_5));
            tv6.setText(getString(R.string.tamil_6));
            tv7.setText(getString(R.string.tamil_7));

        }
        else if(lang.equals("malayalam")){
            tv1.setText(getString(R.string.mala_1));
            tv2.setText(getString(R.string.mala_2));
            tv3.setText(getString(R.string.mala_3));
            tv4.setText(getString(R.string.mala_4));
            tv5.setText(getString(R.string.mala_5));
            tv6.setText(getString(R.string.mala_6));
            tv7.setText(getString(R.string.mala_7));

        }
        else if(lang.equals("telugu")){
            tv1.setText(getString(R.string.telegu_1));
            tv2.setText(getString(R.string.telegu_2));
            tv3.setText(getString(R.string.telegu_3));
            tv4.setText(getString(R.string.telegu_4));
            tv5.setText(getString(R.string.telegu_5));
            tv6.setText(getString(R.string.telegu_6));
            tv7.setText(getString(R.string.telegu_7));

        }
        else if(lang.equals("kannada")){
            tv1.setText(getString(R.string.kannada_1));
            tv2.setText(getString(R.string.kannada_2));
            tv3.setText(getString(R.string.kannada_3));
            tv4.setText(getString(R.string.kannada_4));
            tv5.setText(getString(R.string.kannada_5));
            tv6.setText(getString(R.string.kannada_6));
            tv7.setText(getString(R.string.kannada_7));

        }
        else if(lang.equals("bengali")){
            tv1.setText(getString(R.string.bangla_1));
            tv2.setText(getString(R.string.bangla_2));
            tv3.setText(getString(R.string.bangla_3));
            tv4.setText(getString(R.string.bangla_4));
            tv5.setText(getString(R.string.bangla_5));
            tv6.setText(getString(R.string.bangla_6));
            tv7.setText(getString(R.string.bangla_7));

        }

        hashtagView.addOnTagClickListener(new HashtagView.TagsClickListener() {
            @Override
            public void onItemClicked(Object item) {
                String ss = item.toString();
                word_list.remove(ss);
                hashtagView.removeItem(ss);
                //hashtagView.setData(word_list);
            }
        });

        getHash();

        tv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = "";

                ss = "#"+tv1.getText().toString();

                //setTextHash(ss,etText);

                word_list.add(ss);

                hashtagView.addItem(ss);

            }
        });

        //hashtagView.notify();

        tv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = etText.getText().toString();

                word_list.add("#"+tv2.getText().toString());

                hashtagView.addItem("#"+tv2.getText().toString());
            }
        });

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = etText.getText().toString();

                word_list.add("#"+tv3.getText().toString());

                hashtagView.addItem("#"+tv3.getText().toString());
            }
        });


        tv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = etText.getText().toString();

                word_list.add("#"+tv4.getText().toString());

                hashtagView.addItem("#"+tv4.getText().toString());
            }
        });

        tv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = etText.getText().toString();

                word_list.add("#"+tv5.getText().toString());

                hashtagView.addItem("#"+tv5.getText().toString());
            }
        });

        tv6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = etText.getText().toString();

                word_list.add("#"+tv6.getText().toString());

                hashtagView.addItem("#"+tv6.getText().toString());
            }
        });

        tv7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ss = etText.getText().toString();

                word_list.add("#"+tv7.getText().toString());

                hashtagView.addItem("#"+tv7.getText().toString());
            }
        });


        tvPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String txt = etText.getText().toString();

                if(word_list.size() != 0){
                    tvPost.setEnabled(false);
                    if (type.equals("textPhoto") || type.equals("cameraPhoto")){
                        if (bitmap!=null) {
                            ByteArrayOutputStream byteArrayOutputStream2= new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG,75,byteArrayOutputStream2);

                            fileData=byteArrayOutputStream2.toByteArray();
                            contentType="image/jpeg";


                            SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
                            currenttime=sdf.format(new Date());

                            fileName=userid+currenttime+".jpg";

                            rl.setVisibility(View.VISIBLE);

                            new connecttt().execute();
                        }
                    }
                    else if(type.equals("galleryPhoto")){

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                        byte[] byteArray = stream.toByteArray();


                        CaptureImage captureImage = new CaptureImage();
                        captureImage.setByte(byteArray);
                        captureImage.execute();
                    }
                    else if(type.equals("video")){
                        contentType="video/mp4";


                        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
                        currenttime=sdf.format(new Date());

                        fileName=userid+currenttime+".mp4";

                        new connecttt().execute();
                    }
                }else {
                    Toast.makeText(PostDisplay.this, "Please select HashTag", Toast.LENGTH_SHORT).show();
                }
            }
        });


        imgDisplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
                byte[] byteArray = stream.toByteArray();


                Intent intent = new Intent(PostDisplay.this,PostPreDisplay.class);
                intent.putExtra("Bitmap",byteArray);
                startActivityForResult(intent,99);

            }
        });

    }

    public void setTextHash(String txt,EditText etText){
        String[] words = txt.split("\\s+");// i converted string to String[] array
        List<String> list=new ArrayList<String>();
        System.out.println(words.length);
        String fullText="";
        for(int i=0;i<words.length;i++){
            if(words[i].startsWith("#")){
                list.add(words[i]);
                String ss = words[i];
                String next = "<font color='#FC4C21'>"+ss+"</font>";
                fullText = fullText +"&#032;"+next+"&#032;";
            }
            else {
                fullText = fullText+words[i];
            }

        }


        etText.setText(Html.fromHtml(fullText));
        etText.setSelection(etText.getText().length());
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 99){

            String action = data.getStringExtra("action");

            if(action.equals("del")){

                if(type.equals("cameraPhoto")){
                    Intent intent = new Intent(PostDisplay.this,CameraPost.class);
                    intent.putExtra("type","reCapture");
                    startActivityForResult(intent,98);
                }
                else if(type.equals("textPhoto")){
                    Intent intent = new Intent(PostDisplay.this,TextPost.class);
                    intent.putExtra("type","reCapture");
                    startActivityForResult(intent,97);
                }
                else if(type.equals("galleryPhoto")){
                    Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                    photoPickerIntent.setType("image/*");
                    startActivityForResult(photoPickerIntent, 96);
                }
                else if(type.equals("video")){
                    Intent intent = new Intent();
                    intent.setType("video/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent,"Select Video"),95);
                }



            }


        }
        if(requestCode == 98){

            byte[] byteArray = data.getByteArrayExtra("Bitmap");
            bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            if (bitmap!=null){
                imgDisplay.setImageBitmap(bitmap);
            }

        }
        if(requestCode == 97){

            byte[] byteArray = data.getByteArrayExtra("Bitmap");
            bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            if (bitmap!=null){
                imgDisplay.setImageBitmap(bitmap);
            }

        }

        if(requestCode == 96){
            if(resultCode == RESULT_OK){
                Uri selectedImageURI = data.getData();
                InputStream imageStream = null;
                try {
                    imageStream = getContentResolver().openInputStream(selectedImageURI);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                bitmap = BitmapFactory.decodeStream(imageStream);

                if (bitmap!=null){
                    imgDisplay.setImageBitmap(bitmap);
                }

            }
        }

        if(requestCode == 95){
            if(resultCode == RESULT_OK){
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
                    Uri uri = data.getData();
                    System.out.println("only path >> "+uri.getPath());
                    File file = new File(uri.getPath());//create path from uri
                    final String[] split = uri.toString().split(":");//split the path.
                    String filePath = split[1];//assign it to a string(your choice).

                    video_path=filePath;
                    Bitmap btm= ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Video.Thumbnails.MINI_KIND);
                    bitmap = ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Images.Thumbnails.MINI_KIND);

                    if (bitmap!=null){
                        imgDisplay.setImageBitmap(bitmap);
                    }
                }
                else {
                    Uri uri_video = data.getData();
                    try {
                        String filePath=PathUtil.getPath(getApplicationContext(),uri_video);
                        video_path=filePath;
                        Bitmap btm= ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Video.Thumbnails.MINI_KIND);
                        bitmap = ThumbnailUtils.createVideoThumbnail(video_path, MediaStore.Images.Thumbnails.MINI_KIND);

                        if (bitmap!=null){
                            imgDisplay.setImageBitmap(bitmap);
                        }

                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }

                }


            }
        }

    }

    public Bitmap compressImage( final byte[] data ) {


        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

        //options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;

        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);


        int actualHeight = bmp.getHeight();
        int actualWidth = bmp.getHeight();

//      max Height and width values of the compressed image is taken as 816x612

        float maxHeight = 1280.0f;
        float maxWidth = 720.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];


        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ByteArrayOutputStream out1 = new ByteArrayOutputStream();
        scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out1);


        return bmp;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        System.out.println(inSampleSize);

        return inSampleSize;
    }

    protected int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else {
            return data.getByteCount();
        }
    }

    class CaptureImage extends AsyncTask<Void,Void,Void> {
        byte[] pic;
        Bitmap bmp;

        public void setByte(byte[] bytes){
            this.pic=bytes;
        }


        @Override
        protected Void doInBackground(Void... voids) {




            Bitmap decoded2;
            decoded2 = compressImage(pic);

            Bitmap bitmapImage = decoded2;
            int nh = (int) ( bitmapImage.getHeight() * (720.0 / bitmapImage.getWidth()) );
            Bitmap scaled = Bitmap.createScaledBitmap(bitmapImage, 720, nh, true);
            System.out.println("only e step1   "+sizeOf(decoded2)/(1024));

            ByteArrayOutputStream out = new ByteArrayOutputStream();

            scaled.compress(Bitmap.CompressFormat.JPEG, 100, out);

            bitmap = scaled;



            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            rl.setVisibility(View.VISIBLE);
        }




        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            ByteArrayOutputStream byteArrayOutputStream2= new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG,75,byteArrayOutputStream2);

            fileData=byteArrayOutputStream2.toByteArray();
            contentType="image/jpeg";


            SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
            currenttime=sdf.format(new Date());

            fileName=userid+currenttime+".jpg";

            rl.setVisibility(View.VISIBLE);

            new connecttt().execute();

        }
    }


    class connecttt extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            new connecttt2().execute();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            rl.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL urll = new URL("https://api.backblazeb2.com/b2api/v2/b2_authorize_account");
                connectionn = (HttpURLConnection)urll.openConnection();
                connectionn.setRequestMethod("GET");
                connectionn.setRequestProperty("Authorization", headerForAuthorizeAccount);
                InputStream in = new BufferedInputStream(connectionn.getInputStream());
                jsonResponse = myInputStreamReader(in);
                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("bucketId")){
                        String[] splitter=s.split("\"");
                        bucketId=splitter[3];
                        postParams = "{\"bucketId\":\"" + bucketId + "\"}";
                        postData= postParams.getBytes(StandardCharsets.UTF_8);


                    }else if (s.contains("apiUrl")){
                        String[] splitter=s.split("\"");
                        apiUrl=splitter[3];
                    }else if (s.contains("authorizationToken")){
                        String[] splitter=s.split("\"");
                        accountAuthorizationToken=splitter[3];
                    }
                }





            } catch (Exception e) {
                e.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });

            } finally {
                connectionn.disconnect();
            }

            return null;
        }
    }


    class connecttt2 extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            if(type.equals("video")){
                new connecttt4().execute();
            }else {
                new connecttt3().execute();
            }

        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL(apiUrl + "/b2api/v2/b2_get_upload_url");
                connectionuploadurl = (HttpURLConnection)url.openConnection();
                connectionuploadurl.setRequestMethod("POST");
                connectionuploadurl.setRequestProperty("Authorization", accountAuthorizationToken);
                connectionuploadurl.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                connectionuploadurl.setRequestProperty("charset", "utf-8");
                connectionuploadurl.setRequestProperty("Content-Length", Integer.toString(postData.length));
                connectionuploadurl.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connectionuploadurl.getOutputStream());
                writer.write(postData);
                String jsonResponse = myInputStreamReader(connectionuploadurl.getInputStream());


                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("uploadUrl")){
                        String[] splitter=s.split("\"");
                        uploadUrl=splitter[3];


                    }else if (s.contains("authorizationToken")){
                        String[] splitter=s.split("\"");
                        uploadAuthorizationToken=splitter[3];
                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connectionuploadurl.disconnect();
            }

            return null;
        }
    }


    class connecttt3 extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            rl.setVisibility(View.GONE);

            String hash="";
            for (int i = 0 ; i < word_list.size(); i++){
                hash = hash+word_list.get(i);
            }

            String tttt = etText.getText().toString()+"=="+hash;
            uploadphotopost send_data=new uploadphotopost( username,userid,fileName,currenttime,userid+currenttime,tttt,ppurl,language,responseListener);
            RequestQueue requestQueue= Volley.newRequestQueue(PostDisplay.this);
            requestQueue.add(send_data);


        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL(uploadUrl);
                connectionupload = (HttpURLConnection)url.openConnection();
                connectionupload.setRequestMethod("POST");
                connectionupload.setRequestProperty("Authorization", uploadAuthorizationToken);
                connectionupload.setRequestProperty("Content-Type", contentType);
                connectionupload.setRequestProperty("X-Bz-File-Name", fileName);
                connectionupload.setRequestProperty("X-Bz-Content-Sha1", "do_not_verify");
                connectionupload.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connectionupload.getOutputStream());


                writer.write(fileData);
                String jsonResponse = myInputStreamReader(connectionupload.getInputStream());


                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("fileId")){
                        String[] splitter=s.split("\"");
                        fileid=splitter[3];


                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connectionupload.disconnect();
            }

            return null;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }
    }


    class connecttt4 extends AsyncTask<Void,Integer,Void>{

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            try{
                if(bitmap == null){
                    bitmap = BitmapFactory.decodeResource(getResources(),
                            R.drawable.vv4);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    fileData=byteArray;
                    contentType="image/jpeg";
                    fileName2=userid+currenttime+".jpg";

                }
                else {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    byte[] byteArray = stream.toByteArray();

                    fileData=byteArray;
                    contentType="image/jpeg";
                    fileName2=userid+currenttime+".jpg";

                }

                new connecttt5().execute();
            }catch (NullPointerException e){
                bitmap = BitmapFactory.decodeResource(getResources(),
                        R.drawable.vv4);
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                byte[] byteArray = stream.toByteArray();

                fileData=byteArray;
                contentType="image/jpeg";
                fileName2=userid+currenttime+".jpg";

                new connecttt5().execute();
            }



        }



        @Override
        protected void onPreExecute() {
            super.onPreExecute();


        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL urll = new URL(uploadUrl);
                connectionupload = (HttpURLConnection)urll.openConnection();
                connectionupload.setRequestMethod("POST");
                connectionupload.setRequestProperty("Authorization", uploadAuthorizationToken);
                connectionupload.setRequestProperty("Content-Type", contentType);
                connectionupload.setRequestProperty("X-Bz-File-Name", fileName);
                connectionupload.setRequestProperty("X-Bz-Content-Sha1", "do_not_verify");
                connectionupload.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connectionupload.getOutputStream());


                int buffersize=512;
                byte[] buffer=new byte[buffersize];
                File file = new File(video_path);
                FileInputStream fileInputStream=new FileInputStream(file);
                int bytesread=fileInputStream.read(buffer,0,buffersize);
                int progresss=0;

                int maxbuffersize=20*1024*1024;
                while (bytesread>0){
                    progresss+=bytesread;
                    writer.write(buffer,0,bytesread);
                    int bytesavailabe=fileInputStream.available();

                    buffersize=Math.min(bytesavailabe,maxbuffersize);
                    buffer=new byte[buffersize];
                    bytesread=fileInputStream.read(buffer,0,buffersize);


                }
                fileInputStream.close();

                writer.flush();
                writer.close();






                String jsonResponse = myInputStreamReader(connectionupload.getInputStream());


                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("fileId")){
                        String[] splitter=s.split("\"");
                        fileid=splitter[3];


                    }else if (s.contains("contentLength")){
                        String[] splitter=s.split("\"");
                        String aaa=splitter[3];
                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connectionupload.disconnect();
            }

            return null;
        }


    }


    class connecttt5 extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            rl.setVisibility(View.GONE);

            String hash="";
            for (int i = 0 ; i < word_list.size(); i++){
                hash = hash+word_list.get(i);
            }

            String tttt = etText.getText().toString()+"=="+hash;


            uploadphotopost send_data=new uploadphotopost( username,userid,fileName2,currenttime+"/"+fileName,userid+currenttime,tttt,ppurl,language,responseListener);
            RequestQueue requestQueue= Volley.newRequestQueue(PostDisplay.this);
            requestQueue.add(send_data);

        }

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL url = new URL(uploadUrl);
                connectionupload = (HttpURLConnection)url.openConnection();
                connectionupload.setRequestMethod("POST");
                connectionupload.setRequestProperty("Authorization", uploadAuthorizationToken);
                connectionupload.setRequestProperty("Content-Type", contentType);
                connectionupload.setRequestProperty("X-Bz-File-Name", fileName2);
                connectionupload.setRequestProperty("X-Bz-Content-Sha1", "do_not_verify");
                connectionupload.setDoOutput(true);
                DataOutputStream writer = new DataOutputStream(connectionupload.getOutputStream());
                writer.write(fileData);
                String jsonResponse = myInputStreamReader(connectionupload.getInputStream());


                String[] lines = jsonResponse.split("\\n");
                for(String s: lines){
                    if (s.contains("fileId")){
                        String[] splitter=s.split("\"");
                        fileid2=splitter[3];


                    }
                }



            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                connectionupload.disconnect();
            }

            return null;
        }


    }


    public void populateHash(){
        hashListView.setAdapter(null);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        hashListView.setLayoutManager(linearLayoutManager);

        hashTagAdapter = new HashTagAdapter(hashList,getApplicationContext());
        hashTagAdapter.setOnCarItemClickListener(listener);
        hashListView.setAdapter(hashTagAdapter);

    }

    HashTagAdapter.OnCarItemClickListener listener = new HashTagAdapter.OnCarItemClickListener() {
        @Override
        public void onWhatsAppClicked(int position, String item, HashTagAdapter.CarsViewHolder holder) {

            word_list.add("#"+item);
            hashtagView.addItem("#"+item);
        }
    };


    public void getHash(){
        hashList = new ArrayList<>();
        hashList2 = new ArrayList<>();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, hashUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        hashList.clear();
                        hashList2.clear();

                        try {
                            JSONObject jsonObjecct = new JSONObject(response);
                            JSONArray jsonArraay = jsonObjecct.getJSONArray("eventss");

                            for(int i = 0 ; i < jsonArraay.length(); i++){
                                JSONObject obj = jsonArraay.getJSONObject(i);

                                hashList.add(obj.getString("hashtag1"));
                                hashList.add(obj.getString("hashtag2"));
                                hashList.add(obj.getString("hashtag3"));
                                hashList.add(obj.getString("hashtag4"));
                                hashList.add(obj.getString("hashtag5"));
                                hashList.add(obj.getString("hashtag6"));



                            }

                            populateHash();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }

                , new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {

            /*@Override
            protected Map<String, String> getParams() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Uid",useridd);
                params.put("Following", itemArrayList2.get(position).getUid());


                return params;
            }*/
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }




    public static String myInputStreamReader(InputStream in) throws IOException {
        InputStreamReader reader = new InputStreamReader(in);
        StringBuilder sb = new StringBuilder();
        int c = reader.read();
        while (c != -1) {
            sb.append((char)c);
            c = reader.read();
        }
        reader.close();


        return sb.toString();
    }
}
