package com.socialmedia3.fartech.socialmedia3;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.google.android.exoplayer2.ui.PlayerView;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class WhatsAppAdpaterLarge extends RecyclerView.Adapter<WhatsAppAdpaterLarge.CarsViewHolder>{

    public ArrayList<WhatsAppItem> horizontalCarList;
    Context context;
    private OnCarItemClickListener listener;


    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onWhatsAppClicked(int position, WhatsAppItem item,CarsViewHolder holder);
        public void onWhatsAppllClicked(int position, WhatsAppItem item,CarsViewHolder holder);

    }
    public void setOnCarItemClickListener(OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public WhatsAppAdpaterLarge(ArrayList<WhatsAppItem> horizontalGrocderyList, Context context){
        this.horizontalCarList= horizontalGrocderyList;
        this.context = context;


    }

    public void setItemlist(ArrayList<WhatsAppItem> list) {
        horizontalCarList = list;
    }


    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.whats_app_full_view_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CarsViewHolder carsViewHolder, final int position) {


        carsViewHolder.rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onWhatsAppClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });

        carsViewHolder.ll_downloads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onWhatsAppllClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });


        carsViewHolder.imgCover.setImageBitmap(horizontalCarList.get(position).getBitmap());


        if(horizontalCarList.get(position).getFileType().equals("video")){
            carsViewHolder.play.setVisibility(View.VISIBLE);
        }else {
            carsViewHolder.play.setVisibility(View.GONE);
        }

    }






    @Override
    public void onViewRecycled(@NonNull CarsViewHolder holder) {
        int position = holder.getAdapterPosition();
        System.out.println("only recycled "+position);


        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        if (horizontalCarList != null) {
            return horizontalCarList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {

        public PlayerView mExoPlayer;
        LinearLayout ll_downloads;
        RelativeLayout rl;
        ImageView imgCover,play;
        ProgressBar progressBar;

        public CarsViewHolder(View view) {
            super(view);

            mExoPlayer = view.findViewById(R.id.video_view);
            imgCover = view.findViewById(R.id.cover);
            play = view.findViewById(R.id.play);
            rl = view.findViewById(R.id.rl);
            ll_downloads = view.findViewById(R.id.ll_downloads);
            progressBar = view.findViewById(R.id.progress);
        }
    }
}
