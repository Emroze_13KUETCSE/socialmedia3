package com.socialmedia3.fartech.socialmedia3;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.ui.PlayerView;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingProgressListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.content.Context.MODE_PRIVATE;

public class WhatsAppAdapterSmall extends RecyclerView.Adapter<WhatsAppAdapterSmall.CarsViewHolder>{

    public ArrayList<WhatsAppItem> horizontalCarList;
    Context context;
    private OnCarItemClickListener listener;


    public interface OnCarItemClickListener {

        /**
         * The method declaration for user selected. This method will be fired
         * when user click on check/uncheck the checkbox on the list item.
         *
         * @param position
         * @param item
         */
        public void onWhatsAppClicked(int position, WhatsAppItem item,CarsViewHolder holder);
    }
    public void setOnCarItemClickListener(OnCarItemClickListener listener) {
        this.listener = listener;
    }

    public WhatsAppAdapterSmall(ArrayList<WhatsAppItem> horizontalGrocderyList, Context context){
        this.horizontalCarList= horizontalGrocderyList;
        this.context = context;


    }

    public void setItemlist(ArrayList<WhatsAppItem> list) {
        horizontalCarList = list;
    }


    @Override
    public CarsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflate the layout file
        View groceryProductView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview_item, parent, false);
        CarsViewHolder gvh = new CarsViewHolder(groceryProductView);
        return gvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final CarsViewHolder carsViewHolder, final int position) {


        carsViewHolder.proPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onWhatsAppClicked(position,horizontalCarList.get(position),carsViewHolder);
                System.out.println("only ======================== select "+position);

            }
        });


        carsViewHolder.proPic.setImageBitmap(horizontalCarList.get(position).getBitmap());


    }






    @Override
    public void onViewRecycled(@NonNull CarsViewHolder holder) {
        int position = holder.getAdapterPosition();
        System.out.println("only recycled "+position);


        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        if (horizontalCarList != null) {
            return horizontalCarList.size();
        } else {
            return 0;
        }
    }

    public class CarsViewHolder extends RecyclerView.ViewHolder {

        CircleImageView proPic;


        public CarsViewHolder(View view) {
            super(view);
            proPic=view.findViewById(R.id.circle_friend2);

        }
    }
}
