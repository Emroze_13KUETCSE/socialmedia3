package com.socialmedia3.fartech.socialmedia3;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Feed.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Feed#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Feed extends Fragment {

    String lang;
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    public ArrayList<PostItem> itemArrayList = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList2 = new ArrayList<>();

    Map<String, PostItem> map = new TreeMap<>();
    private LinearLayoutManager mLayoutManager;
    int counter = 0;
    int REQUEST_STORAGE_PERMISSIONS_CODE = 1;
    private View currentFocusedLayout, oldFocusedLayout;
    private LinearLayoutManager layoutmanager;
    public SimpleExoPlayer exoPlayer;
    int adapterClickedPos = 0;
    ExoAdapter.CarsViewHolder carsViewHolder;
    private int height;
    ExoAdapter adapter;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    JSONArray postJSON;
    int progresss;

    DB_SqliteFollowing db_sqliteFollowing;
    String useridd;
    DB_SqliteFavorites db_sqliteFavorites;

    SwipeRefreshLayout mSwipeRefreshLayout;

    static int itemPosition = 0;

    static int itemForComment = 0;
    ExoAdapter.CarsViewHolder carsViewHolder_comments;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public Feed() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Feed.
     */
    // TODO: Rename and change types and number of parameters
    public static Feed newInstance(String param1, String param2) {
        Feed fragment = new Feed();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preference = getContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;

        db_sqliteFollowing = new DB_SqliteFollowing(getContext());
        db_sqliteFavorites = new DB_SqliteFavorites(getContext());

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feed, container, false);

        System.out.println("only f feed <<<<<<<<<<<<<<<<<<<< onCreateView feed "+itemPosition);

        progressBar = view.findViewById(R.id.progress);

        recyclerView =view.findViewById(R.id.recycler_view);

        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);


        useridd = preference.getString("uid","");
        lang = preference.getString("lang","");



        requestCameraPermission();

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                jsonParse();

            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    boolean saveBitmapToFile(File dir, String fileName, Bitmap bm,
                             Bitmap.CompressFormat format, int quality) {

        File imageFile = new File(dir,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);

            bm.compress(format,quality,fos);

            fos.close();

            return true;
        }
        catch (IOException e) {
            Log.e("app",e.getMessage());
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        System.out.println("only f feed <<<<<<<<<<<<<<<<<<<< activity created "+itemPosition);
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("only f  feed <<<<<<<<<<<<<<<<<<<< onStart "+itemPosition);
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("only f feed <<<<<<<<<<<<<<<<<<<< onResume "+itemPosition);

        if(!itemArrayList2.isEmpty()){
            mLayoutManager.scrollToPosition(itemPosition);
        }
    }

    public void requestCameraPermission() {
        System.out.println("only f >>>>>>>>>>>>>>>  feed");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && getContext().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_STORAGE_PERMISSIONS_CODE);
        } else {
            if(itemArrayList.size() != 0){

                for (int i = 0; i < itemArrayList.size();i++){
                    PostItem item = itemArrayList.get(i);
                    int count2 = db_sqliteFavorites.get_check_List_Favorite(item.getPostid());
                    if (count2 == 0) {
                        itemArrayList.get(i).setLiked(false);
                    } else {
                        itemArrayList.get(i).setLiked(true);
                    }

                    if (item.getUid().equals(useridd)){

                    }else {
                        int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());
                        if (count3 == 0) {
                            itemArrayList.get(i).setFollowing(false);
                        } else {
                            itemArrayList.get(i).setFollowing(true);
                        }
                    }

                }


                populateCarList();
            }
            else {
                jsonParse();
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int reqCode, String[] perms, int[] results) {
        if (reqCode == REQUEST_STORAGE_PERMISSIONS_CODE) {
            if (results.length > 0 && results[0] == PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    System.out.println("here");

                    jsonParse();
                }
                else {

                }

            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        System.out.println("only feed <<<<<<<<<<<<<<<<<<<< onPause "+itemPosition);

        if(!itemArrayList2.isEmpty()){

            if (exoPlayer != null){
                exoPlayer.setPlayWhenReady(false);
                carsViewHolder.play.setVisibility(View.VISIBLE);
                carsViewHolder.imgCover.setVisibility(View.VISIBLE);
                carsViewHolder.mExoPlayer.setVisibility(View.GONE);
            }
        }


    }


    public void populateCarList(){
        progressBar.setVisibility(View.GONE);
        if(itemArrayList2.size() == 0){
            counter = 0;
            for(int i = counter ; i < counter+10; i++){
                if(i == itemArrayList.size()){
                    break;
                }
                itemArrayList.get(i).setPrepared(true);
                itemArrayList2.add(itemArrayList.get(i));
            }

            counter=counter+10;
        }

        adapter = new ExoAdapter(itemArrayList2,getContext());
        adapter.setOnCarItemClickListener(listener);

        recyclerView.setAdapter(adapter);

        mLayoutManager.scrollToPosition(itemPosition);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(final RecyclerView recyclerView, int scrollState) {

                if (!recyclerView.canScrollVertically(1)) {

                    progressBar.setVisibility(View.VISIBLE);
                    final Handler handler=new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            progressBar.setVisibility(View.GONE);


                            if(counter <= itemArrayList.size()){
                                for(int i = counter ; i < counter+10; i++){
                                    if(i == itemArrayList.size()){
                                        System.out.println("only size"+itemArrayList.size());
                                        break;
                                    }
                                    else {
                                        itemArrayList2.add(itemArrayList.get(i));
                                    }

                                }

                                counter=counter+10;

                                adapter.setItemlist(itemArrayList2);
                                adapter.notifyDataSetChanged();
                            }


                        }
                    },500);
                }

                if (scrollState == RecyclerView.SCROLL_STATE_IDLE) {

                    layoutmanager = (LinearLayoutManager)recyclerView.getLayoutManager();

                    int firstVisiblePosition = layoutmanager.findFirstCompletelyVisibleItemPosition();

                    int ddd = layoutmanager.findFirstVisibleItemPosition();
                    int eee = layoutmanager.findLastCompletelyVisibleItemPosition();
                    int fff = layoutmanager.findLastVisibleItemPosition();

                    //System.out.println("only f ======================================= "+firstVisiblePosition);
                    //System.out.println("only f 1st full visible "+firstVisiblePosition);
                    //System.out.println("only f last full visible "+eee);
                    //System.out.println("only f last partial visible "+fff);

                    if(ddd >= 0){
                        itemPosition = ddd;
                        System.out.println("only f 1st partial visible "+itemPosition);

                    }
                    else {
                        itemPosition=0;
                    }
                    if (firstVisiblePosition >= 0 || fff >= 0)
                    {

                        if (oldFocusedLayout != null)
                        {
                            System.out.println("only remove "+firstVisiblePosition);
                            if(exoPlayer != null){

                                carsViewHolder.play.setVisibility(View.VISIBLE);
                                carsViewHolder.imgCover.setVisibility(View.VISIBLE);
                                carsViewHolder.mExoPlayer.setVisibility(View.GONE);

                            }
                        }


                    }
                    if(exoPlayer != null){

                        exoPlayer.setPlayWhenReady(false);
                    }

                    currentFocusedLayout = layoutmanager.findViewByPosition(firstVisiblePosition);
                    //VideoView vv_dashboard = (VideoView)currentFocusedLayout.findViewById(R.id.vv_dashboard);
                    ////to play video of selected recylerview, videosData is an array-list which is send to recyclerview adapter to fill the view. Here we getting that specific video which is displayed through recyclerview.
                    //playVideo(videosData.get(positionView));

                    oldFocusedLayout = currentFocusedLayout;





                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

            }
        });
    }


    ExoAdapter.OnCarItemClickListener listener = new ExoAdapter.OnCarItemClickListener() {
        @Override
        public void onllClicked(final int position, PostItem item, final ExoAdapter.CarsViewHolder holder) {


            System.out.println("only  >>>>>>>>> "+holder.imgCover.getHeight()+" "+holder.imgCover.getWidth());

            if (!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;



                holder.cardmore.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.play.setVisibility(View.GONE);

                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelection.Factory videoTrackSelectionFactory =
                        new AdaptiveTrackSelection.Factory(bandwidthMeter);
                TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                if(exoPlayer != null){
                    exoPlayer.setPlayWhenReady(false);
                    exoPlayer.stop();
                }

                exoPlayer = ExoPlayerFactory.newSimpleInstance(getContext(), trackSelector);

                holder.mExoPlayer.setPlayer(exoPlayer);



                exoPlayer.addListener(new Player.EventListener() {
                    @Override
                    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                        System.out.println("only 1 "+position);
                    }

                    @Override
                    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                        System.out.println("only 2 "+position);
                    }

                    @Override
                    public void onLoadingChanged(boolean isLoading) {
                        System.out.println("only 3 "+position);
                    }

                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                        if (playWhenReady && playbackState == Player.STATE_READY) {
                            System.out.println("only  playing  "+position);
                            // media actually playing
                            holder.progressBar.setVisibility(View.GONE);
                            holder.play.setVisibility(View.GONE);
                            holder.imgCover.setVisibility(View.GONE);
                            holder.mExoPlayer.setVisibility(View.VISIBLE);
                            holder.mExoPlayer.getLayoutParams().height = holder.imgCover.getHeight();
                            holder.mExoPlayer.requestFocus();


                        } else if (playWhenReady) {
                            // might be idle (plays after prepare()),
                            // buffering (plays when data available)
                            // or ended (plays when seek away from end)
                            if (playbackState==Player.STATE_ENDED){
                                System.out.println("only  Ended  "+position);
                            }
                            else if (playbackState==Player.STATE_BUFFERING){
                                System.out.println("only Buffering "+position);
                            }
                            else if (playbackState==Player.STATE_IDLE){
                                System.out.println("only Idle "+position);
                            }
                        } else {
                            // player paused in any state
                            System.out.println("only  paused  "+position);



                            //horizontalCarList.get(position).setPlayWhenReady(true);

                        }

                    }

                    @Override
                    public void onRepeatModeChanged(int repeatMode) {

                    }

                    @Override
                    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                    }

                    @Override
                    public void onPlayerError(ExoPlaybackException error) {

                    }

                    @Override
                    public void onPositionDiscontinuity(int reason) {

                    }

                    @Override
                    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                    }

                    @Override
                    public void onSeekProcessed() {

                    }
                });


                CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

                MediaSource audioSource = new ExtractorMediaSource(Uri.parse(itemArrayList2.get(position).getVideoUrl()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


                exoPlayer.seekTo(position, 0);

                exoPlayer.prepare(audioSource, true, false);

                System.out.println("only  here  "+itemArrayList2.get(position).getPrepared());

                exoPlayer.setPlayWhenReady(true);


                adapterClickedPos=position;
                carsViewHolder = holder;

            }else {
                holder.play.setVisibility(View.GONE);

            }



            //System.out.println("only ??????????????????????????????????????? select "+position+itemArrayList2.get(position).getPlayWhenReady());
            //adapter.setItemlist(itemArrayList2);
            //adapter.notifyDataSetChanged();
        }

        @Override
        public void onCoverClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            System.out.println("only ??????????????????????????????????????? select "+position);




        }

        @Override
        public void onWhatsAppClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            if (!itemArrayList2.get(position).getTime().contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(position).getText());
                }
                String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(),
                        bittt, "Title", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }


                int shar = itemArrayList2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                itemArrayList2.get(position).setShares(shar);
                itemArrayList.get(position).setShares(shar);
                adapter.setItemlist(itemArrayList2);


                StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }

                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("postid", itemArrayList2.get(position).getPostid());

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                requestQueue.add(stringrequest);

            }else {


                int shar = itemArrayList2.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }


                downloadvideoWhatsApp downloadvideo2=new downloadvideoWhatsApp();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList2.get(position).getVideoUrl(),time2,String.valueOf(position));

            }
        }

        @Override
        public void onLikeClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

        }

        @Override
        public void onCommentsClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

        }

        @Override
        public void onDownloadsClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            viewHolder.progressBar_down.setVisibility(View.VISIBLE);

            if (!itemArrayList2.get(position).getTime().contains("/")){


                try{
                    Bitmap bm = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();

                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "SocialTest/");

                    boolean doSave = true;
                    if (!f.exists()) {
                        doSave = f.mkdirs();
                    }

                    if (doSave) {
                        saveBitmapToFile(f,itemArrayList2.get(position).getPostid()+".png",bm,Bitmap.CompressFormat.PNG,100);
                    }
                    else {
                        Log.e("app","Couldn't create target directory.");
                    }


                    Toast.makeText(getContext(), "Saved", Toast.LENGTH_SHORT).show();
                    int down = itemArrayList2.get(position).getDownloadsno();
                    itemArrayList2.get(position).setDownloadsno(down+1);
                    itemArrayList.get(position).setDownloadsno(down+1);
                    viewHolder.tvDownloads.setText(String.valueOf(down+1));
                    adapter.setItemlist(itemArrayList2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {




                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("postid",itemArrayList2.get(position).postid);

                            return params;
                        }
                    };

                    RequestQueue requestQueue= Volley.newRequestQueue(getContext());
                    requestQueue.add(stringrequest);
                }catch (NullPointerException e){

                }




            }else {

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                int down = itemArrayList2.get(position).getDownloadsno();
                viewHolder.tvDownloads.setText(String.valueOf(down+1));

                downloadvideooo downloadvideo2=new downloadvideooo();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList2.get(position).getVideoUrl(),time2,String.valueOf(position));





            }





        }

        @Override
        public void onMoreClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

            if(holder.cardmore.getVisibility() == View.VISIBLE){
                holder.cardmore.animate().alpha(0);
                holder.cardmore.setVisibility(View.GONE);
            }else {
                holder.cardmore.setVisibility(View.VISIBLE);
                holder.cardmore.animate().alpha(1);
            }

        }

        @Override
        public void onShareClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            if (!itemArrayList2.get(position).time.contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String path = MediaStore.Images.Media.insertImage(getContext().getContentResolver(),
                        bittt, "Image", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

            }else {

                String[] splitter =itemArrayList2.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                downloadvideoshare downloadvideo=new downloadvideoshare();
                downloadvideo.setdownloadlayout(viewHolder.ll_share);
                downloadvideo.setProgressbar(viewHolder.progressBar_share);
                downloadvideo.execute(itemArrayList2.get(position).getVideoUrl(), time2,String.valueOf(position));

            }

        }

        @Override
        public void onReportClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            Toast.makeText(getContext(), "Report Sent", Toast.LENGTH_SHORT).show();
            viewHolder.cardmore.animate().alpha(0);
            viewHolder.cardmore.setVisibility(View.GONE);
        }

        @Override
        public void onAddClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

        }

        @Override
        public void onProPicClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            

        }
    };


    public void jsonParse(){
        String url = "http://heyapp.in/showposts.php";

        System.out.println("rrrrrrrrrrrrrrrrrrr**************");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("eventss");

                            map.clear();
                            for(int i = 0 ; i < array.length();i++){
                                JSONObject object = array.getJSONObject(i);

                                /*object.getString("language").equals("bengali")||
                                        object.getString("language").equals("hindi")||
                                        object.getString("language").equals("tamil")*/
                                //language
                                if(!object.getString("uid").isEmpty() && !object.getString("language").isEmpty()){
                                    if(lang.equals(object.getString("language"))){
                                        PostItem item = new PostItem(getContext());
                                        item.setUid(object.getString("uid"));
                                        item.setPostid(object.getString("postid"));
                                        item.setUsername(object.getString("username"));
                                        item.setTime(object.getString("time"));
                                        item.setImagelocation("https://static.heyapp.in/file/app-media/"+object.getString("imagelocation"));

                                        item.setText(object.getString("text"));
                                        item.setDownloadsno(object.getInt("downloads"));
                                        item.setLikes(object.getInt("likes"));
                                        item.setShares(object.getInt("shares"));
                                        item.setCommentsno(object.getInt("comments"));




                                        String[] splitter =item.getTime().split("/");
                                        String time1 = splitter[0];
                                        String time2="";
                                        String time = "";

                                        // https:\/\/static.heyapp.in\/file\/app-media\/MArzuAxcATgdP3IuQkr2f248hL7220181114142546.jpg
                                        // https://static.heyapp.in/file/app-media/MArzuAxcATgdP3IuQkr2f248hL7220181114142546.jpg
                                        try {
                                            time2 = splitter[1];
                                            //time = time1.substring(0, time1.length() - 1);
                                        }catch (ArrayIndexOutOfBoundsException e){

                                        }

                                        if(item.getTime().contains("/")){
                                            time = time1.substring(0, time1.length() - 1);
                                            System.out.println("only  -------     " + object.getString("username")+""+time);

                                        }else {
                                            time = item.getTime();
                                        }
                                        System.out.println("only ff  ------->>>>>>>>     " + object.getString("username")+"   "+object.getString("language"));

                                        item.setVideoUrl("https://static.heyapp.in/file/app-media/"+time2);

                                        String temp = object.getString("ppurl");


                                        item.setPpurl(temp);

                                        int count2 = db_sqliteFavorites.get_check_List_Favorite(item.getPostid());
                                        if (count2 == 0) {
                                            item.setLiked(false);
                                        } else {
                                            item.setLiked(true);
                                        }

                                        if (item.getUid().equals(useridd)){

                                        }else {
                                            int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());
                                            if (count3 == 0) {
                                                item.setFollowing(false);
                                            } else {
                                                item.setFollowing(true);
                                            }
                                        }

                                        map.put(time,item);
                                    }
                                }



                            }

                            Set keys = map.keySet();
                            final Iterator it = keys.iterator();

                            System.out.println("only All keys are: " + keys);

                            ArrayList<PostItem> aaaa = new ArrayList<>();

                            postJSON = new JSONArray();

                            while(it.hasNext()){
                                Object key = it.next();

                                System.out.println("only "+key + ": " + map.get(key).getUsername());

                                PostItem item = new PostItem(getContext());

                                item.setUid(map.get(key).getUid());

                                item.setPostid(map.get(key).getPostid());

                                item.setUsername(map.get(key).getUsername());

                                item.setTime(map.get(key).getTime());

                                item.setImagelocation(map.get(key).getImagelocation());

                                item.setVideoUrl(map.get(key).getVideoUrl());

                                item.setPpurl(map.get(key).getPpurl());

                                item.setText(map.get(key).getText());

                                item.setCommentsno(map.get(key).getCommentsno());

                                item.setDownloadsno(map.get(key).getDownloadsno());

                                item.setLikes(map.get(key).getLikes());

                                item.setShares(map.get(key).getShares());

                                item.setPlaying(false);

                                item.setFollowing(map.get(key).isFollowing());

                                item.setLiked(map.get(key).isLiked());

                                aaaa.add(item);


                            }
                            itemArrayList.clear();
                            itemArrayList2.clear();

                            for(int i = aaaa.size()-1;i > -1 ;i--) {
                                itemArrayList.add(aaaa.get(i));

                                JSONObject object = new JSONObject();
                                PostItem item = new PostItem(getContext());

                                item.setUsername(aaaa.get(i).getUsername());
                                object.put("userName",item.getUsername());

                                item.setUid(aaaa.get(i).getUid());
                                object.put("uid",item.getUid());

                                item.setPostid(aaaa.get(i).getPostid());
                                object.put("postid",item.getPostid());

                                item.setTime(aaaa.get(i).getTime());
                                object.put("time",item.getTime());

                                item.setImagelocation(aaaa.get(i).getImagelocation());
                                object.put("imgUrl",item.getImagelocation());

                                item.setVideoUrl(aaaa.get(i).getVideoUrl());
                                object.put("videoUrl",item.getVideoUrl());

                                item.setPpurl(aaaa.get(i).getPpurl());
                                object.put("ppurl",item.getPpurl());

                                item.setText(aaaa.get(i).getText());
                                object.put("text",item.getText());

                                item.setCommentsno(aaaa.get(i).getCommentsno());
                                object.put("comments",item.getCommentsno());

                                item.setDownloadsno(aaaa.get(i).getDownloadsno());
                                object.put("downloads",item.getDownloadsno());

                                item.setLikes(aaaa.get(i).getLikes());
                                object.put("likes",item.getLikes());

                                item.setShares(aaaa.get(i).getShares());
                                object.put("shares",item.getShares());

                                item.setFollowing(aaaa.get(i).isFollowing());
                                object.put("isFollowing",item.isFollowing());

                                item.setLiked(aaaa.get(i).isLiked());
                                object.put("isLiked",item.isLiked());

                                postJSON.put(object);


                            }

                            progressBar.setVisibility(View.GONE);

                            mSwipeRefreshLayout.setRefreshing(false);

                            System.out.println("only >>>>>>>>>>>> size"+itemArrayList.size());
                            //getImageBitmap();
                            populateCarList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+error.toString());
                    }
                }
        ) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", ACCESS_TOKEN);
                return params;
            }*/

            /*@Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phone);
                params.put("otp", otp);
                params.put("timestamp", timeStamp);
                params.put("fcm_token", fcm_token);


                return params;
            }*/
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                //statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }



    class downloadvideooo extends AsyncTask<String,Integer,Void> {
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        String videoname;
        int pos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Void doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                pos=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/SocialTest").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/SocialTest/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            Toast.makeText(getContext(), "Saved", Toast.LENGTH_SHORT).show();
            circleProgressBar.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.VISIBLE);





            int down = itemArrayList2.get(pos).getDownloadsno();
            itemArrayList2.get(pos).setDownloadsno(down+1);
            itemArrayList.get(pos).setDownloadsno(down+1);
            adapter.setItemlist(itemArrayList2);

            StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {




                        }
                    }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();

                    params.put("postid",itemArrayList2.get(pos).postid);

                    return params;
                }
            };

            RequestQueue requestQueue= Volley.newRequestQueue(getContext());
            requestQueue.add(stringrequest);


//            ddd ddd=new ddd();
//            ddd.setdownloadlayout(linearLayout);
//            ddd.setProgressbar(circleProgressBar);
//            ddd.execute(videoname);


        }
    }


    class downloadvideoWhatsApp extends AsyncTask<String,Integer,Boolean> {

        int positionn;
        String videoname;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
            progresss=0;
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                positionn=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/SocialTest").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/SocialTest/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){


                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/SocialTest/" +videoname);

                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList2.get(positionn).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(positionn).getText());
                }
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    getContext().startActivity(shareIntent);
                    int share = itemArrayList2.get(positionn).getShares();
                    itemArrayList2.get(positionn).setShares(share+1);
                    itemArrayList.get(positionn).setShares(share+1);
                    adapter.setItemlist(itemArrayList2);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList2.get(positionn).getPostid());

                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getContext());
                    requestQueue.add(stringrequest);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }






                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.linearLayout.setVisibility(View.VISIBLE);

            }






        }


    }

    class downloadvideoshare extends AsyncTask<String,Integer,Boolean> {

        String videoname;
        int position;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                position=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");

                urlConnection.setRequestProperty("Accept-Encoding","identity");
                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/SocialTest").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/SocialTest/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;

                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);
                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){



                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/SocialTest/" +videoname);
                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                if (!itemArrayList2.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList2.get(position).getText());
                }
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                getContext().startActivity(Intent.createChooser(shareIntent, "Share Using"));

                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);
            }






        }


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 11){

            int counter = data.getIntExtra("commentCounter",0);

            carsViewHolder_comments.tvComments.setText(String.valueOf(counter));
            System.out.println("only ff result back "+counter);
        }
    }

}
