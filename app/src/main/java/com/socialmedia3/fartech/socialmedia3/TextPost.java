package com.socialmedia3.fartech.socialmedia3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.ByteArrayOutputStream;

import ja.burhanrashid52.photoeditor.OnPhotoEditorListener;
import ja.burhanrashid52.photoeditor.OnSaveBitmap;
import ja.burhanrashid52.photoeditor.PhotoEditor;
import ja.burhanrashid52.photoeditor.PhotoEditorView;
import ja.burhanrashid52.photoeditor.ViewType;

public class TextPost extends AppCompatActivity implements OnPhotoEditorListener {

    TextView tvText;
    private PhotoEditor mPhotoEditor;
    private PhotoEditorView mPhotoEditorView;
    private int mColorCode;

    TextView tvNext;

    Bitmap bitmap;

    RelativeLayout relativeLayout;
    ProgressBar progressBar;

    String type="capture";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_post);

        Intent intent = getIntent();

        if(intent.getStringExtra("type") != null){
            type = intent.getStringExtra("type");
        }


        tvText = findViewById(R.id.add_text);
        mPhotoEditorView = findViewById(R.id.photoEditorView);
        relativeLayout = findViewById(R.id.rl);
        progressBar = findViewById(R.id.progress);
        tvNext = findViewById(R.id.tv_next);

        mPhotoEditor = new PhotoEditor.Builder(this, mPhotoEditorView)
                .setPinchTextScalable(true) // set flag to make text scalable when pinch
                //.setDefaultTextTypeface(mTextRobotoTf)
                //.setDefaultEmojiTypeface(mEmojiTypeFace)
                .build(); // build photo editor sdk

        mPhotoEditor.setOnPhotoEditorListener(this);


        relativeLayout.setVisibility(View.GONE);

        tvText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextEditorDialogFragment textEditorDialogFragment = TextEditorDialogFragment.show(TextPost.this);
                textEditorDialogFragment.setOnTextEditorListener(new TextEditorDialogFragment.TextEditor() {
                    @Override
                    public void onDone(String inputText, int colorCode) {
                        mPhotoEditor.addText(inputText, colorCode);
                    }
                });
            }
        });

        int color = Color.parseColor("#FDBA48");
        mPhotoEditorView.getSource().setColorFilter(color);
        //Setup the color picker for text color
        RecyclerView addTextColorPickerRecyclerView = findViewById(R.id.add_text_color_picker_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        addTextColorPickerRecyclerView.setLayoutManager(layoutManager);
        addTextColorPickerRecyclerView.setHasFixedSize(true);
        ColorPickerAdapter colorPickerAdapter = new ColorPickerAdapter(getApplicationContext());
        //This listener will change the text color when clicked on any color from picker
        colorPickerAdapter.setOnColorPickerClickListener(new ColorPickerAdapter.OnColorPickerClickListener() {
            @Override
            public void onColorPickerClickListener(int colorCode) {
                mColorCode = colorCode;
                mPhotoEditorView.getSource().setColorFilter(mColorCode);
            }
        });
        addTextColorPickerRecyclerView.setAdapter(colorPickerAdapter);


        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                relativeLayout.setVisibility(View.VISIBLE);
                OnSaveBitmap onSaveBitmap = new OnSaveBitmap() {
                    @Override
                    public void onBitmapReady(Bitmap saveBitmap) {
                        relativeLayout.setVisibility(View.GONE);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        saveBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        byte[] byteArray = stream.toByteArray();

                        System.out.println("only "+ (saveBitmap.getByteCount()/1024));


                        if(type.equals("capture")){
                            Intent intent = new Intent(TextPost.this,PostDisplay.class);
                            intent.putExtra("Bitmap",byteArray);
                            intent.putExtra("type","textPhoto");
                            startActivity(intent);


                        }else if(type.equals("reCapture")){
                            Intent intent = new Intent();
                            intent.putExtra("Bitmap",byteArray);
                            intent.putExtra("type","textPhoto");
                            setResult(97 , intent);
                        }


                        finish();
                    }

                    @Override
                    public void onFailure(Exception e) {
                        System.out.println("Post bitmap eeeee");
                    }
                };
                mPhotoEditor.saveAsBitmap(onSaveBitmap);

            }
        });

    }

    @Override
    public void onEditTextChangeListener(View rootView, String text, int colorCode) {

    }

    @Override
    public void onAddViewListener(ViewType viewType, int numberOfAddedViews) {

    }

    @Override
    public void onRemoveViewListener(int numberOfAddedViews) {

    }

    @Override
    public void onRemoveViewListener(ViewType viewType, int numberOfAddedViews) {

    }

    @Override
    public void onStartViewChangeListener(ViewType viewType) {

    }

    @Override
    public void onStopViewChangeListener(ViewType viewType) {

    }
}
