package com.socialmedia3.fartech.socialmedia3;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class WhatsAppDisplay extends AppCompatActivity {

    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    ArrayList<WhatsAppItem > whatsAppItemArrayList = new ArrayList<>();
    RecyclerView recyclerView_whatsApp;
    WhatsAppAdpaterLarge whatsAppAdpaterLarge;
    RecyclerView.LayoutManager mLayoutManager;
    int posOfItem=0;
    SnapHelper snapHelper;
    public SimpleExoPlayer exoPlayer;


    private static final String WHATSAPP_STATUSES_LOCATION = "/WhatsApp/Media/.Statuses";
    //public static RecyclerView friendslist2;
    ArrayList<statuseslistitem> f = new ArrayList<statuseslistitem>();// list of file paths
    File[] listFile;

    File imagefile,storagedir,file;

    public static AppBarLayout appBarLayout;
    String statusexists="no";

    int offset=8;
    int countoffset=0;
    int countofffset=0;
    int countoffset2=0;
    String showmore="yes";

    boolean isPlaying=false;

    boolean isReady = false;

    int adapterClickedPos = 0;
    WhatsAppAdpaterLarge.CarsViewHolder carsViewHolder;

    Context context;

    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_app_display);

        context = getApplicationContext();

        Intent intent = getIntent();
        posOfItem = intent.getIntExtra("pos",0);


        progressBar = findViewById(R.id.progress);
        recyclerView_whatsApp = findViewById(R.id.recycler_view);

        recyclerView_whatsApp.setAdapter(null);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false);
        recyclerView_whatsApp.setLayoutManager(linearLayoutManager);

        recyclerView_whatsApp = (RecyclerView) findViewById(R.id.recycler_view);

        snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerView_whatsApp);

        mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        mLayoutManager.scrollToPosition(posOfItem);
        recyclerView_whatsApp.setLayoutManager(mLayoutManager);
        recyclerView_whatsApp.setItemAnimator(new DefaultItemAnimator());

        new BackgroundTask().execute();

    }

    @Override
    public void onPause() {
        super.onPause();

        if(!whatsAppItemArrayList.isEmpty()){

            if (exoPlayer != null){
                exoPlayer.setPlayWhenReady(false);
                carsViewHolder.play.setVisibility(View.VISIBLE);
                carsViewHolder.imgCover.setVisibility(View.VISIBLE);
                carsViewHolder.mExoPlayer.setVisibility(View.GONE);
            }
        }


    }

    class BackgroundTask extends AsyncTask<Void,Void,String> {

        String json_url;

        @Override
        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);

        }


        @Override
        protected String doInBackground(Void... params) {


            file= new File(Environment.getExternalStorageDirectory()+WHATSAPP_STATUSES_LOCATION);

            Date currentdate=new Date();
            SimpleDateFormat dateFormat=new SimpleDateFormat("MMM");
            SimpleDateFormat dayformat=new SimpleDateFormat("dd");
            String currentmonth=dateFormat.format(currentdate);
            String currentday=dayformat.format(currentdate);

            showmore="no";

            if (file.isDirectory())
            {
                listFile = file.listFiles();

                if (listFile!=null){

                    while (countoffset<listFile.length)
                    {
                        Date date=new Date(listFile[countoffset].lastModified());
                        SimpleDateFormat dateFormat2=new SimpleDateFormat("MMddHHmm");
                        SimpleDateFormat dayformat2=new SimpleDateFormat("dd");

                        int currentmonth2=Integer.parseInt(dateFormat2.format(date));


                        showmore="yes";

                        statusexists="yes";



                        if (listFile[countoffset].getName().contains(".mp4")||listFile[countoffset].getName().contains(".MP4")
                                ||listFile[countoffset].getName().contains(".3gp")||listFile[countoffset].getName().contains(".3GP")||
                                listFile[countoffset].getName().contains(".jpg")||listFile[countoffset].getName().contains(".png")||listFile[countoffset].getName().contains(".jpeg")) {

                            WhatsAppItem item = new WhatsAppItem();
                            item.setFilePath(listFile[countoffset].getAbsolutePath());

                            if(listFile[countoffset].getName().contains(".mp4")||listFile[countoffset].getName().contains(".MP4")
                                    ||listFile[countoffset].getName().contains(".3gp")||listFile[countoffset].getName().contains(".3GP")){
                                item.setFileType("video");
                                Bitmap bitmap= ThumbnailUtils.createVideoThumbnail(item.getFilePath(), MediaStore.Video.Thumbnails.MICRO_KIND);
                                if (bitmap!=null){
                                    item.setBitmap(bitmap);
                                }
                            }
                            else if(listFile[countoffset].getName().contains(".jpg")||listFile[countoffset].getName().contains(".png")||listFile[countoffset].getName().contains(".jpeg")){
                                item.setFileType("image");
                                BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                                Bitmap bitmap = BitmapFactory.decodeFile(item.getFilePath(),bmOptions);
                                item.setBitmap(bitmap);
                            }

                            whatsAppItemArrayList.add(item);
                        }


                        countoffset++;

                    }

                }


            }


            return null;
        }



        @Override
        protected void onProgressUpdate(Void...values) {
            super.onProgressUpdate();
        }


        @Override
        public void onPostExecute (String result) {

            progressBar.setVisibility(View.GONE);
            whatsAppAdpaterLarge = new WhatsAppAdpaterLarge(whatsAppItemArrayList,getApplicationContext());
            whatsAppAdpaterLarge.setOnCarItemClickListener(listener_whatsApp);
            recyclerView_whatsApp.setAdapter(whatsAppAdpaterLarge);

            recyclerView_whatsApp.addOnScrollListener(new RecyclerView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(final RecyclerView recyclerView, int scrollState) {

                    if (!recyclerView.canScrollVertically(1)) {


                    }

                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE){
                        View centerView = snapHelper.findSnapView(mLayoutManager);
                        final int position = mLayoutManager.getPosition(centerView);

                        final WhatsAppAdpaterLarge.CarsViewHolder holder = (WhatsAppAdpaterLarge.CarsViewHolder) recyclerView.findViewHolderForAdapterPosition(position);

                        System.out.println("only  >>>>>>>>> "+holder.imgCover.getHeight()+" "+holder.imgCover.getWidth());
                        isPlaying = false;

                        if(whatsAppItemArrayList.get(position).getFileType().equals("video")){
                            holder.progressBar.setVisibility(View.VISIBLE);
                            holder.play.setVisibility(View.GONE);
                            holder.imgCover.setVisibility(View.VISIBLE);

                            isReady = false;

                            BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                            TrackSelection.Factory videoTrackSelectionFactory =
                                    new AdaptiveTrackSelection.Factory(bandwidthMeter);
                            TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                            if(exoPlayer != null){
                                exoPlayer.setPlayWhenReady(false);
                                exoPlayer.stop();
                            }

                            exoPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

                            holder.mExoPlayer.setPlayer(exoPlayer);



                            exoPlayer.addListener(new Player.EventListener() {
                                @Override
                                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                                    System.out.println("only 1 "+position);
                                }

                                @Override
                                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                                    System.out.println("only 2 "+position);
                                }

                                @Override
                                public void onLoadingChanged(boolean isLoading) {
                                    System.out.println("only 3 "+position);
                                }

                                @Override
                                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                                    if (playWhenReady && playbackState == Player.STATE_READY) {
                                        System.out.println("only  playing  "+position);
                                        // media actually playing
                                        holder.progressBar.setVisibility(View.GONE);
                                        holder.play.setVisibility(View.GONE);
                                        holder.imgCover.setVisibility(View.GONE);
                                        holder.mExoPlayer.setVisibility(View.VISIBLE);

                                        isPlaying = true;

                                        isReady = true;
                                    } else if (playWhenReady) {
                                        // might be idle (plays after prepare()),
                                        // buffering (plays when data available)
                                        // or ended (plays when seek away from end)
                                        if (playbackState==Player.STATE_ENDED){
                                            System.out.println("only  Ended  "+position);
                                        }
                                        else if (playbackState==Player.STATE_BUFFERING){
                                            System.out.println("only Buffering "+position);
                                        }
                                        else if (playbackState==Player.STATE_IDLE){
                                            System.out.println("only Idle "+position);
                                        }
                                    } else {
                                        // player paused in any state
                                        System.out.println("only  paused  "+position);



                                        //horizontalCarList.get(position).setPlayWhenReady(true);

                                    }

                                }

                                @Override
                                public void onRepeatModeChanged(int repeatMode) {

                                }

                                @Override
                                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                                }

                                @Override
                                public void onPlayerError(ExoPlaybackException error) {

                                }

                                @Override
                                public void onPositionDiscontinuity(int reason) {

                                }

                                @Override
                                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                                }

                                @Override
                                public void onSeekProcessed() {

                                }
                            });


                            CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

                            MediaSource audioSource = new ExtractorMediaSource(Uri.parse(whatsAppItemArrayList.get(position).getFilePath()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


                            exoPlayer.seekTo(position, 0);

                            exoPlayer.prepare(audioSource, true, false);

                            exoPlayer.setPlayWhenReady(true);
                            exoPlayer.getPlaybackState();
                            adapterClickedPos=position;
                            carsViewHolder = holder;

                        }else {
                            holder.progressBar.setVisibility(View.GONE);
                            holder.play.setVisibility(View.GONE);
                            holder.imgCover.setVisibility(View.VISIBLE);
                            if(exoPlayer != null){
                                exoPlayer.setPlayWhenReady(false);
                                exoPlayer.stop();
                            }
                        }

                    }


                }

                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                }
            });

            final Handler handler=new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {

                    if(whatsAppItemArrayList.get(posOfItem).getFileType().equals("video")){
                        View centerView = snapHelper.findSnapView(mLayoutManager);
                        final int position = mLayoutManager.getPosition(centerView);

                        final WhatsAppAdpaterLarge.CarsViewHolder holder = (WhatsAppAdpaterLarge.CarsViewHolder) recyclerView_whatsApp.findViewHolderForAdapterPosition(position);


                        holder.progressBar.setVisibility(View.VISIBLE);
                        holder.play.setVisibility(View.GONE);


                        isReady = false;

                        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                        TrackSelection.Factory videoTrackSelectionFactory =
                                new AdaptiveTrackSelection.Factory(bandwidthMeter);
                        TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                        if(exoPlayer != null){
                            exoPlayer.setPlayWhenReady(false);
                            exoPlayer.stop();
                        }

                        exoPlayer = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

                        holder.mExoPlayer.setPlayer(exoPlayer);



                        exoPlayer.addListener(new Player.EventListener() {
                            @Override
                            public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                                System.out.println("only 1 "+posOfItem);
                            }

                            @Override
                            public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                                System.out.println("only 2 "+posOfItem);
                            }

                            @Override
                            public void onLoadingChanged(boolean isLoading) {
                                System.out.println("only 3 "+posOfItem);
                            }

                            @Override
                            public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                                if (playWhenReady && playbackState == Player.STATE_READY) {
                                    System.out.println("only  playing  "+posOfItem);
                                    // media actually playing
                                    holder.progressBar.setVisibility(View.GONE);
                                    holder.play.setVisibility(View.GONE);
                                    holder.imgCover.setVisibility(View.GONE);
                                    holder.mExoPlayer.setVisibility(View.VISIBLE);

                                    isPlaying = true;

                                    isReady = true;
                                } else if (playWhenReady) {
                                    // might be idle (plays after prepare()),
                                    // buffering (plays when data available)
                                    // or ended (plays when seek away from end)
                                    if (playbackState==Player.STATE_ENDED){
                                        System.out.println("only  Ended  "+posOfItem);
                                    }
                                    else if (playbackState==Player.STATE_BUFFERING){
                                        System.out.println("only Buffering "+posOfItem);
                                    }
                                    else if (playbackState==Player.STATE_IDLE){
                                        System.out.println("only Idle "+posOfItem);
                                    }
                                } else {
                                    // player paused in any state
                                    System.out.println("only  paused  "+posOfItem);



                                    //horizontalCarList.get(position).setPlayWhenReady(true);

                                }

                            }

                            @Override
                            public void onRepeatModeChanged(int repeatMode) {

                            }

                            @Override
                            public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                            }

                            @Override
                            public void onPlayerError(ExoPlaybackException error) {

                            }

                            @Override
                            public void onPositionDiscontinuity(int reason) {

                            }

                            @Override
                            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                            }

                            @Override
                            public void onSeekProcessed() {

                            }
                        });


                        CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

                        MediaSource audioSource = new ExtractorMediaSource(Uri.parse(whatsAppItemArrayList.get(posOfItem).getFilePath()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


                        exoPlayer.seekTo(posOfItem, 0);

                        exoPlayer.prepare(audioSource, true, false);


                        exoPlayer.setPlayWhenReady(true);
                        exoPlayer.getPlaybackState();
                        adapterClickedPos=posOfItem;
                        carsViewHolder = holder;

                    }else {

                        if(exoPlayer != null){
                            exoPlayer.setPlayWhenReady(false);
                            exoPlayer.stop();
                        }
                    }

                }
            },1000);
        }


    }

    WhatsAppAdpaterLarge.OnCarItemClickListener listener_whatsApp = new WhatsAppAdpaterLarge.OnCarItemClickListener() {
        @Override
        public void onWhatsAppClicked(int position, WhatsAppItem item, WhatsAppAdpaterLarge.CarsViewHolder holder) {
            if(exoPlayer != null){
                if(isPlaying){
                    isPlaying = false;
                    exoPlayer.setPlayWhenReady(false);
                    exoPlayer.getPlaybackState();
                    holder.play.setVisibility(View.VISIBLE);
                }else {
                    if(isReady){
                        exoPlayer.setPlayWhenReady(true);
                        exoPlayer.getPlaybackState();
                        isPlaying = true;
                        holder.play.setVisibility(View.GONE);
                    }
                }


            }
        }

        @Override
        public void onWhatsAppllClicked(int position, WhatsAppItem item, WhatsAppAdpaterLarge.CarsViewHolder holder) {
            File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "Flash/");

            boolean doSave = true;
            if (!f.exists()) {
                doSave = f.mkdirs();
            }

            copyFileOrDirectory(item.getFilePath(),f.getAbsolutePath());

            Toast.makeText(WhatsAppDisplay.this, "Saved", Toast.LENGTH_SHORT).show();
        }
    };


    public static void copyFileOrDirectory(String srcDir, String dstDir) {

        try {
            File src = new File(srcDir);
            File dst = new File(dstDir, src.getName());

            if (src.isDirectory()) {

                String files[] = src.list();
                int filesLength = files.length;
                for (int i = 0; i < filesLength; i++) {
                    String src1 = (new File(src, files[i]).getPath());
                    String dst1 = dst.getPath();
                    copyFileOrDirectory(src1, dst1);

                }
            } else {
                copyFile(src, dst);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(File sourceFile, File destFile) throws IOException {
        if (!destFile.getParentFile().exists())
            destFile.getParentFile().mkdirs();

        if (!destFile.exists()) {
            destFile.createNewFile();
        }

        FileChannel source = null;
        FileChannel destination = null;

        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }

        }
    }

}
