package com.socialmedia3.fartech.socialmedia3;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItem;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.socialmedia3.fartech.socialmedia3.TrendFragment.trend_1;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import static android.content.Context.MODE_PRIVATE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Trend.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Trend#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Trend extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    String lang;
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    public ArrayList<PostItem> itemArrayList = new ArrayList<>();
    public ArrayList<PostItem> itemArrayList2 = new ArrayList<>();

    Map<String, PostItem> map = new TreeMap<>();

    JSONArray postJSON;

    String useridd;

    SwipeRefreshLayout mSwipeRefreshLayout;
    DB_SqliteFollowing db_sqliteFollowing;
    DB_SqliteFavorites db_sqliteFavorites;


    GridView gridView;
    int counter = 0;

    DB_Post db_post;

    TrendingAdapter2 adapter;
    private LinearLayoutManager mLayoutManager;
    public ArrayList<PostItem2> postItem2ArrayList = new ArrayList<>();
    RecyclerView recyclerView;
    ViewPager viewPager;
    SmartTabLayout viewPagerTab;
    ProgressBar progressBar;


    public Trend() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Trend.
     */
    // TODO: Rename and change types and number of parameters
    public static Trend newInstance(String param1, String param2) {
        Trend fragment = new Trend();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    FragmentPagerItemAdapter fragmentPagerItemAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preference = getContext().getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();
        db_sqliteFollowing = new DB_SqliteFollowing(getContext());
        db_sqliteFavorites = new DB_SqliteFavorites(getContext());

        db_post = new DB_Post(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vvv = inflater.inflate(R.layout.fragment_trend, container, false);

        System.out.println("only f trending <<<<<<<<<<<<<<<<<<<< onCreateView trending");


        useridd = preference.getString("uid","");
        lang = preference.getString("lang","");


        progressBar = vvv.findViewById(R.id.progress);

        progressBar.setVisibility(View.VISIBLE);
        viewPager =  vvv.findViewById(R.id.viewpager);


        if(lang.equals("hindi")){
            FragmentPagerItems pages = new FragmentPagerItems(getContext());
            pages.add(FragmentPagerItem.of(getString(R.string.hindi_1), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.hindi_2), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.hindi_3), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.hindi_4), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.hindi_5), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.hindi_6), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.hindi_7), trend_1.class));

            fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                    getChildFragmentManager(), pages);


        }
        else if(lang.equals("tamil")){
            FragmentPagerItems pages = new FragmentPagerItems(getContext());
            pages.add(FragmentPagerItem.of(getString(R.string.tamil_1), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.tamil_2), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.tamil_3), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.tamil_4), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.tamil_5), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.tamil_6), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.tamil_7), trend_1.class));


            fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                    getChildFragmentManager(), pages);


        }
        else if(lang.equals("malayalam")){
            FragmentPagerItems pages = new FragmentPagerItems(getContext());
            pages.add(FragmentPagerItem.of(getString(R.string.mala_1), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.mala_2), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.mala_3), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.mala_4), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.mala_5), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.mala_6), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.mala_7), trend_1.class));


            fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                    getChildFragmentManager(), pages);



        }
        else if(lang.equals("telugu")){
            FragmentPagerItems pages = new FragmentPagerItems(getContext());
            pages.add(FragmentPagerItem.of(getString(R.string.telegu_1), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.telegu_2), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.telegu_3), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.telegu_4), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.telegu_5), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.telegu_6), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.telegu_7), trend_1.class));


            fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                    getChildFragmentManager(), pages);



        }
        else if(lang.equals("kannada")){
            FragmentPagerItems pages = new FragmentPagerItems(getContext());
            pages.add(FragmentPagerItem.of(getString(R.string.kannada_1), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.kannada_2), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.kannada_3), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.kannada_4), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.kannada_5), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.kannada_6), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.kannada_7), trend_1.class));


            fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                    getChildFragmentManager(), pages);


        }
        else if(lang.equals("bengali")){
            FragmentPagerItems pages = new FragmentPagerItems(getContext());
            pages.add(FragmentPagerItem.of(getString(R.string.bangla_1), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.bangla_2), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.bangla_3), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.bangla_4), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.bangla_5), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.bangla_6), trend_1.class));
            pages.add(FragmentPagerItem.of(getString(R.string.bangla_7), trend_1.class));

            fragmentPagerItemAdapter = new FragmentPagerItemAdapter(
                    getChildFragmentManager(), pages);


            System.out.println("only fff   trend");

        }


        jsonParse();

        return vvv;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public void jsonParse(){
        String url = "http://heyapp.in/showposts.php";

        System.out.println("rrrrrrrrrrrrrrrrrrr**************");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("eventss");

                            map.clear();
                            for(int i = 0 ; i < array.length();i++){
                                JSONObject object = array.getJSONObject(i);

                                /*object.getString("language").equals("bengali")||
                                        object.getString("language").equals("hindi")||
                                        object.getString("language").equals("tamil")*/
                                //language
                                if(!object.getString("uid").isEmpty() && !object.getString("language").isEmpty()){
                                    if(true){
                                        PostItem item = new PostItem(getContext());
                                        item.setUid(object.getString("uid"));
                                        item.setPostid(object.getString("postid"));
                                        item.setUsername(object.getString("username"));
                                        item.setTime(object.getString("time"));
                                        item.setImagelocation("https://static.heyapp.in/file/app-media/"+object.getString("imagelocation"));

                                        item.setText(object.getString("text"));
                                        item.setDownloadsno(object.getInt("downloads"));
                                        item.setLikes(object.getInt("likes"));
                                        item.setShares(object.getInt("shares"));
                                        item.setCommentsno(object.getInt("comments"));



                                        String[] splitter =item.getTime().split("/");
                                        String time1 = splitter[0];
                                        String time2="";
                                        String time = "";

                                        // https:\/\/static.heyapp.in\/file\/app-media\/MArzuAxcATgdP3IuQkr2f248hL7220181114142546.jpg
                                        // https://static.heyapp.in/file/app-media/MArzuAxcATgdP3IuQkr2f248hL7220181114142546.jpg
                                        try {
                                            time2 = splitter[1];
                                            //time = time1.substring(0, time1.length() - 1);
                                        }catch (ArrayIndexOutOfBoundsException e){

                                        }

                                        if(item.getTime().contains("/")){
                                            time = time1.substring(0, time1.length() - 1);
                                            System.out.println("only  -------     " + object.getString("username")+""+time);

                                        }else {
                                            time = item.getTime();

                                        }
                                        System.out.println("only ff  ------->>>>>>>>     " + object.getString("username")+"   "+object.getString("language"));

                                        item.setVideoUrl("https://static.heyapp.in/file/app-media/"+time2);

                                        String temp = object.getString("ppurl");


                                        item.setPpurl(temp);

                                        /*int count2 = db_sqliteFavorites.get_check_List_Favorite(item.getPostid());
                                        if (count2 == 0) {
                                            item.setLiked(false);
                                        } else {
                                            item.setLiked(true);
                                        }

                                        if (item.getUid().equals(useridd)){

                                        }else {
                                            int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());
                                            if (count3 == 0) {
                                                item.setFollowing(false);
                                            } else {
                                                item.setFollowing(true);
                                            }
                                        }*/


                                        map.put(time,item);
                                    }
                                }



                            }

                            Set keys = map.keySet();
                            final Iterator it = keys.iterator();

                            System.out.println("only All keys are: " + keys);

                            ArrayList<PostItem> aaaa = new ArrayList<>();

                            postJSON = new JSONArray();

                            while(it.hasNext()){
                                Object key = it.next();
                                System.out.println("only "+key + ": " + map.get(key).getUsername());


                                PostItem item = new PostItem(getContext());

                                item.setUid(map.get(key).getUid());

                                item.setPostid(map.get(key).getPostid());

                                item.setUsername(map.get(key).getUsername());

                                item.setTime(map.get(key).getTime());

                                item.setImagelocation(map.get(key).getImagelocation());

                                item.setVideoUrl(map.get(key).getVideoUrl());

                                item.setPpurl(map.get(key).getPpurl());

                                item.setText(map.get(key).getText());

                                item.setCommentsno(map.get(key).getCommentsno());

                                item.setDownloadsno(map.get(key).getDownloadsno());

                                item.setLikes(map.get(key).getLikes());

                                item.setShares(map.get(key).getShares());

                                item.setPlaying(false);
                                item.setFollowing(map.get(key).isFollowing());

                                item.setFollowing(map.get(key).isLiked());

                                aaaa.add(item);


                            }
                            itemArrayList.clear();
                            itemArrayList2.clear();

                            for(int i = aaaa.size()-1;i > -1 ;i--) {
                                itemArrayList.add(aaaa.get(i));
                                JSONObject object = new JSONObject();
                                PostItem item = new PostItem(getContext());

                                item.setUsername(aaaa.get(i).getUsername());
                                object.put("userName",item.getUsername());

                                item.setUid(aaaa.get(i).getUid());
                                object.put("uid",item.getUid());

                                item.setPostid(aaaa.get(i).getPostid());
                                object.put("postid",item.getPostid());

                                item.setTime(aaaa.get(i).getTime());
                                object.put("time",item.getTime());

                                item.setImagelocation(aaaa.get(i).getImagelocation());
                                object.put("imgUrl",item.getImagelocation());

                                item.setVideoUrl(aaaa.get(i).getVideoUrl());
                                object.put("videoUrl",item.getVideoUrl());

                                item.setPpurl(aaaa.get(i).getPpurl());
                                object.put("ppurl",item.getPpurl());

                                item.setText(aaaa.get(i).getText());
                                object.put("text",item.getText());

                                item.setCommentsno(aaaa.get(i).getCommentsno());
                                object.put("comments",item.getCommentsno());

                                item.setDownloadsno(aaaa.get(i).getDownloadsno());
                                object.put("downloads",item.getDownloadsno());

                                item.setLikes(aaaa.get(i).getLikes());
                                object.put("likes",item.getLikes());

                                item.setShares(aaaa.get(i).getShares());
                                object.put("shares",item.getShares());
                                item.setFollowing(aaaa.get(i).isFollowing());
                                object.put("isFollowing",item.isFollowing());

                                item.setLiked(aaaa.get(i).isLiked());
                                object.put("isLiked",item.isLiked());

                                postJSON.put(object);

                                if(useridd.equals(item.getUid())){
                                    System.out.println("only " + " @@@@@@@@@@@@@@@@@@@@@ " + aaaa.get(i).getUid()+"   "+useridd);
                                }
                                else {
                                    System.out.println("only " + " @@@@@@@@@@@@@@@@@@@@@ " + aaaa.get(i).getUid()+"   ");
                                }

                                if (!aaaa.get(i).getImagelocation().equals("https://static.heyapp.in/file/app-media/")) {
                                    System.out.println("only " + " @@@@@@@@@@@@@@@@@@@@@ " + aaaa.get(i).getImagelocation());
                                }
                                else {
                                    System.out.println("only " + " ##################### " + aaaa.get(i).getImagelocation());
                                }

                            }


                            db_post.Insertlist(postJSON.toString(),"0");

                            viewPager.setAdapter(fragmentPagerItemAdapter);
                            viewPagerTab.setViewPager(viewPager);
                            progressBar.setVisibility(View.GONE);
                            System.out.println("only >>>>>>>>>>>> size"+itemArrayList.size());
                            //getImageBitmap();
                            //populateCarList();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+error.toString());
                    }
                }
        ) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", ACCESS_TOKEN);
                return params;
            }

            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phone);
                params.put("otp", otp);
                params.put("timestamp", timeStamp);
                params.put("fcm_token", fcm_token);


                return params;
            }*/
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                //statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }
}
