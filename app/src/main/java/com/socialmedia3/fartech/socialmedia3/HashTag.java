package com.socialmedia3.fartech.socialmedia3;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class HashTag extends AppCompatActivity {
    public static String PREFS_NAME = "FarTech";
    SharedPreferences preference;
    SharedPreferences.Editor editor;

    public ArrayList<PostItem> itemArrayList_search = new ArrayList<>();
    ExoAdapter adapter_search;
    RecyclerView recyclerView_search;
    String search_text="";
    private LinearLayoutManager layoutmanager_search,layoutmanager_search2;
    public SimpleExoPlayer exoPlayer_search;
    int adapterClickedPos_search = 0;
    ExoAdapter.CarsViewHolder carsViewHolder_search;
    static int itemPosSearch=0;

    JSONArray postJSON;
    DB_SqliteFollowing db_sqliteFollowing;
    String useridd;
    DB_SqliteFavorites db_sqliteFavorites;

    String lang="";
    Map<String, PostItem> map = new TreeMap<>();

    int progresss=0;

    ProgressBar progressBar;

    SwipeRefreshLayout mSwipeRefreshLayout;

    TextView tv;

    String whatsAppText="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hash_tag);

        Intent intent = getIntent();

        search_text = intent.getStringExtra("text");

        preference = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        editor = preference.edit();

        useridd = preference.getString("uid","");
        lang = preference.getString("lang","");

        if(lang.equals("hindi")){

            whatsAppText = getString(R.string.w_hindi);
        }
        else if(lang.equals("tamil")){

            whatsAppText = getString(R.string.w_tamil);
        }
        else if(lang.equals("malayalam")){
            whatsAppText = getString(R.string.w_mala);
        }
        else if(lang.equals("telugu")){
            whatsAppText = getString(R.string.w_telegu);
        }
        else if(lang.equals("kannada")){
            whatsAppText = getString(R.string.w_kannada);
        }
        else if(lang.equals("bengali")){
            whatsAppText = getString(R.string.w_bangla);
        }

        db_sqliteFollowing = new DB_SqliteFollowing(getApplicationContext());
        db_sqliteFavorites = new DB_SqliteFavorites(getApplicationContext());

        recyclerView_search = findViewById(R.id.recycler_view);
        layoutmanager_search = new LinearLayoutManager(getApplicationContext());
        recyclerView_search.setLayoutManager(layoutmanager_search);

        tv = findViewById(R.id.hash_text);
        progressBar = findViewById(R.id.progress);
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                jsonParse();
            }
        });
        jsonParse();

        tv.setText(search_text);
    }

    public void populate(){
        recyclerView_search.setVisibility(View.VISIBLE);

        adapter_search = new ExoAdapter(itemArrayList_search,getApplicationContext());
        adapter_search.setOnCarItemClickListener(listener_search);
        recyclerView_search.setAdapter(adapter_search);

    }

    boolean saveBitmapToFile(File dir, String fileName, Bitmap bm, Bitmap.CompressFormat format, int quality) {

        File imageFile = new File(dir,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);

            bm.compress(format,quality,fos);

            fos.close();

            return true;
        }
        catch (IOException e) {
            Log.e("app",e.getMessage());
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }


    ExoAdapter.OnCarItemClickListener listener_search = new ExoAdapter.OnCarItemClickListener() {
        @Override
        public void onllClicked(final int position, PostItem item, final ExoAdapter.CarsViewHolder holder) {


            System.out.println("only  >>>>>>>>> "+holder.imgCover.getHeight()+" "+holder.imgCover.getWidth());

            /*if (!item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                DisplayMetrics displayMetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                int height = displayMetrics.heightPixels;
                int width = displayMetrics.widthPixels;



                holder.cardmore.setVisibility(View.GONE);
                holder.progressBar.setVisibility(View.VISIBLE);
                holder.play.setVisibility(View.GONE);

                BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
                TrackSelection.Factory videoTrackSelectionFactory =
                        new AdaptiveTrackSelection.Factory(bandwidthMeter);
                TrackSelector trackSelector = new DefaultTrackSelector(videoTrackSelectionFactory);

                if(exoPlayer != null){
                    exoPlayer.setPlayWhenReady(false);
                    exoPlayer.stop();
                }

                exoPlayer_search = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);

                holder.mExoPlayer.setPlayer(exoPlayer_search);



                exoPlayer_search.addListener(new Player.EventListener() {
                    @Override
                    public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {
                        System.out.println("only 1 "+position);
                    }

                    @Override
                    public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {
                        System.out.println("only 2 "+position);
                    }

                    @Override
                    public void onLoadingChanged(boolean isLoading) {
                        System.out.println("only 3 "+position);
                    }

                    @Override
                    public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {

                        if (playWhenReady && playbackState == Player.STATE_READY) {
                            System.out.println("only  playing  "+position);
                            // media actually playing
                            holder.progressBar.setVisibility(View.GONE);
                            holder.play.setVisibility(View.GONE);
                            holder.imgCover.setVisibility(View.GONE);
                            holder.mExoPlayer.setVisibility(View.VISIBLE);
                            holder.mExoPlayer.getLayoutParams().height = holder.imgCover.getHeight();
                            holder.mExoPlayer.requestFocus();


                        } else if (playWhenReady) {
                            // might be idle (plays after prepare()),
                            // buffering (plays when data available)
                            // or ended (plays when seek away from end)
                            if (playbackState==Player.STATE_ENDED){
                                System.out.println("only  Ended  "+position);
                            }
                            else if (playbackState==Player.STATE_BUFFERING){
                                System.out.println("only Buffering "+position);
                            }
                            else if (playbackState==Player.STATE_IDLE){
                                System.out.println("only Idle "+position);
                            }
                        } else {
                            // player paused in any state
                            System.out.println("only  paused  "+position);



                            //horizontalCarList.get(position).setPlayWhenReady(true);

                        }

                    }

                    @Override
                    public void onRepeatModeChanged(int repeatMode) {

                    }

                    @Override
                    public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                    }

                    @Override
                    public void onPlayerError(ExoPlaybackException error) {

                    }

                    @Override
                    public void onPositionDiscontinuity(int reason) {

                    }

                    @Override
                    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                    }

                    @Override
                    public void onSeekProcessed() {

                    }
                });


                CacheDataSourceFactory2 cacheDataSourceFactory = new CacheDataSourceFactory2(getApplicationContext(), 100 * 1024 * 1024, 5 * 1024 * 1024);

                MediaSource audioSource = new ExtractorMediaSource(Uri.parse(itemArrayList_search.get(position).getVideoUrl()),cacheDataSourceFactory, new DefaultExtractorsFactory(), null, null);


                exoPlayer_search.seekTo(position, 0);

                exoPlayer_search.prepare(audioSource, true, false);


                exoPlayer_search.setPlayWhenReady(true);


                adapterClickedPos_search=position;
                carsViewHolder_search = holder;

            }else {
                holder.play.setVisibility(View.GONE);

            }*/



            //System.out.println("only ??????????????????????????????????????? select "+position+itemArrayList2.get(position).getPlayWhenReady());
            //adapter.setItemlist(itemArrayList2);
            //adapter.notifyDataSetChanged();

            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                holder.play.setVisibility(View.GONE);
            }else {
                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_search.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_search.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }

            float views = 0;
            String vvv="";
            views = itemArrayList_search.get(position).getViews();
            views=views+1;
            itemArrayList_search.get(position).setViews((int)views);
            if(views<=999){
                int vvvvv = (int)views;
                vvv = String.valueOf(vvvvv)+" views";
            }
            else if(views >= 1000){
                views = views/1000;
                vvv = String.valueOf(views)+"k views";
            }else if(views >= 1000000){
                views = views/1000000;
                vvv = String.valueOf(views)+"M views";
            }

            holder.tvViews.setText(vvv);

            itemPosSearch=position;
        }

        @Override
        public void onCoverClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            System.out.println("only ??????????????????????????????????????? select "+position);


            if(item.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                Intent intent = new Intent(getApplicationContext(),ImageDisplay.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);

                intent.putExtra("postId", item.getPostid());
                intent.putExtra("imgUrl", item.getPpurl());
                intent.putExtra("userName", item.getUsername());
                intent.putExtra("otherId", item.getUid());
                intent.putExtra("coverUrl", item.getImagelocation());
                intent.putExtra("likes", item.getLikes());
                intent.putExtra("comments", item.getCommentsno());
                intent.putExtra("shares", item.getShares());
                intent.putExtra("downloads", item.getDownloadsno());
                intent.putExtra("txt", item.getText());
                intent.putExtra("views", item.getViews());

                startActivity(intent);
            }else {
                postJSON = new JSONArray();

                int pooos=0;
                int counter = 0;
                for(int i = 0;i < itemArrayList_search.size() ;i++) {

                    JSONObject object = new JSONObject();
                    PostItem item1 = itemArrayList_search.get(i);

                    try {

                        if(!item1.getVideoUrl().equals("https://static.heyapp.in/file/app-media/")){
                            if(item.getPostid().equals(item1.getPostid())){
                                pooos = counter;
                            }
                            object.put("userName",item1.getUsername());
                            object.put("uid",item1.getUid());
                            object.put("postid",item1.getPostid());
                            object.put("time",item1.getTime());
                            object.put("imgUrl",item1.getImagelocation());
                            object.put("videoUrl",item1.getVideoUrl());
                            object.put("ppurl",item1.getPpurl());
                            object.put("text",item1.getText());
                            object.put("comments",item1.getCommentsno());
                            object.put("downloads",item1.getDownloadsno());
                            object.put("likes",item1.getLikes());
                            object.put("shares",item1.getShares());
                            object.put("isFollowing",item1.isFollowing());
                            object.put("isLiked",item1.isLiked());
                            object.put("views",item1.getViews());

                            counter++;
                            postJSON.put(object);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }






                }


                Intent intent = new Intent(getApplicationContext(),VideoFullView.class);
                //intent.putParcelableArrayListExtra("items",itemArrayList);
                System.out.println("only e  >>>>>>> size "+postJSON.length());

                intent.putExtra("items", postJSON.toString());
                intent.putExtra("pos",pooos);
                intent.putExtra("original_pos",position);
                startActivity(intent);
            }


            float views = 0;
            String vvv="";
            views = itemArrayList_search.get(position).getViews();
            views=views+1;
            itemArrayList_search.get(position).setViews((int)views);
            if(views<=999){
                int vvvvv = (int)views;
                vvv = String.valueOf(vvvvv)+" views";
            }
            else if(views >= 1000){
                views = views/1000;
                vvv = String.valueOf(views)+"k views";
            }else if(views >= 1000000){
                views = views/1000000;
                vvv = String.valueOf(views)+"M views";
            }

            holder.tvViews.setText(vvv);

        }

        @Override
        public void onWhatsAppClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            if (!itemArrayList_search.get(position).getTime().contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");
                if (!itemArrayList_search.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_search.get(position).getText()+"\n"+whatsAppText);
                }
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Title", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }


                int shar = itemArrayList_search.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                itemArrayList_search.get(position).setShares(shar);

                adapter_search.setItemlist(itemArrayList_search);


                StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {


                            }
                        }

                        , new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {

                        Map<String, String> params = new HashMap<String, String>();
                        params.put("postid", itemArrayList_search.get(position).getPostid());

                        return params;
                    }
                };
                RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                requestQueue.add(stringrequest);

            }else {


                int shar = itemArrayList_search.get(position).getShares();
                viewHolder.tvWhatsApp.setText(String.valueOf(shar + 1));

                String[] splitter =itemArrayList_search.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }


                downloadvideoWhatsApp downloadvideo2=new downloadvideoWhatsApp();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList_search.get(position).getVideoUrl(),time2,String.valueOf(position));

            }
        }

        @Override
        public void onLikeClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }else {
                viewHolder.imgLike.setSelected(!viewHolder.imgLike.isSelected());
                int count = db_sqliteFavorites.get_check_List_Favorite(itemArrayList_search.get(position).getPostid());
                if (count == 0) {
                    viewHolder.imgLike.setImageResource(R.drawable.love2);
                    int lik = itemArrayList_search.get(position).getLikes();

                    itemArrayList_search.get(position).setLikes(lik+1);

                    adapter_search.setItemlist(itemArrayList_search);

                    itemArrayList_search.get(position).setLiked(true);

                    adapter_search.setItemlist3(true,position);

                    db_sqliteFavorites.Inserttoalertlist(itemArrayList_search.get(position).getPostid());

                    viewHolder.tvLikes.setText(String.valueOf(lik + 1));

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/sociallikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_search.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);
                } else {
                    viewHolder.imgLike.setImageResource(R.drawable.dislike);
                    db_sqliteFavorites.Delete(itemArrayList_search.get(position).getPostid());

                    int lik = itemArrayList_search.get(position).getLikes();

                    itemArrayList_search.get(position).setLikes(lik-1);

                    adapter_search.setItemlist(itemArrayList_search);

                    itemArrayList_search.get(position).setLiked(false);

                    adapter_search.setItemlist3(false,position);

                    if (lik == 0) {
                        viewHolder.tvLikes.setText("Like");
                    }

                    viewHolder.tvLikes.setText(String.valueOf(lik - 1));

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialdislikes.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_search.get(position).getPostid());

                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }
            }
        }

        @Override
        public void onCommentsClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();
            FirebaseUser currentUser = mAuth.getCurrentUser();
            if (currentUser==null) {
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {

                Intent intent=new Intent(getApplicationContext(),Comments.class);
                intent.putExtra("postid",itemArrayList_search.get(position).getPostid());
                startActivityForResult(intent,11);
            }
        }

        @Override
        public void onDownloadsClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            viewHolder.progressBar_down.setVisibility(View.VISIBLE);

            if (!itemArrayList_search.get(position).getTime().contains("/")){


                try{
                    Bitmap bm = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();

                    File f = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + File.separator + "SocialTest/");

                    boolean doSave = true;
                    if (!f.exists()) {
                        doSave = f.mkdirs();
                    }

                    if (doSave) {
                        saveBitmapToFile(f,itemArrayList_search.get(position).getPostid()+".png",bm,Bitmap.CompressFormat.PNG,100);
                    }
                    else {
                        Log.e("app","Couldn't create target directory.");
                    }


                    Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
                    int down = itemArrayList_search.get(position).getDownloadsno();
                    itemArrayList_search.get(position).setDownloadsno(down+1);
                    viewHolder.tvDownloads.setText(String.valueOf(down+1));
                    adapter_search.setItemlist(itemArrayList_search);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {




                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();

                            params.put("postid",itemArrayList_search.get(position).postid);

                            return params;
                        }
                    };

                    RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                }catch (NullPointerException e){

                }




            }else {

                String[] splitter =itemArrayList_search.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                int down = itemArrayList_search.get(position).getDownloadsno();
                viewHolder.tvDownloads.setText(String.valueOf(down+1));

                downloadvideooo downloadvideo2=new downloadvideooo();
                downloadvideo2.setdownloadlayout(viewHolder.ll_downloads);
                downloadvideo2.setProgressbar(viewHolder.progressBar_down);
                downloadvideo2.execute(itemArrayList_search.get(position).getVideoUrl(),time2,String.valueOf(position));





            }





        }

        @Override
        public void onMoreClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

            if(holder.cardmore.getVisibility() == View.VISIBLE){
                holder.cardmore.animate().alpha(0);
                holder.cardmore.setVisibility(View.GONE);
            }else {
                holder.cardmore.setVisibility(View.VISIBLE);
                holder.cardmore.animate().alpha(1);
            }

        }

        @Override
        public void onShareClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            if (!itemArrayList_search.get(position).time.contains("/")){

                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                Bitmap bittt = ((BitmapDrawable)viewHolder.imgCover.getDrawable()).getBitmap();
                bittt.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                String path = MediaStore.Images.Media.insertImage(getContentResolver(),
                        bittt, "Image", null);
                shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
                shareIntent.setType("image/jpeg");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

            }else {

                String[] splitter =itemArrayList_search.get(position).getTime().split("/");
                String time1 = splitter[0];
                String time2="";
                String time = "";

                try {
                    time2 = splitter[1];
                    //time = time1.substring(0, time1.length() - 1);
                }catch (ArrayIndexOutOfBoundsException e){

                }

                downloadvideoshare downloadvideo=new downloadvideoshare();
                downloadvideo.setdownloadlayout(viewHolder.ll_share);
                downloadvideo.setProgressbar(viewHolder.progressBar_share);
                downloadvideo.execute(itemArrayList_search.get(position).getVideoUrl(), time2,String.valueOf(position));

            }

        }

        @Override
        public void onReportClicked(int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {

            Toast.makeText(getApplicationContext(), "Report Sent", Toast.LENGTH_SHORT).show();
            viewHolder.cardmore.animate().alpha(0);
            viewHolder.cardmore.setVisibility(View.GONE);
        }

        @Override
        public void onAddClicked(final int position, PostItem item, ExoAdapter.CarsViewHolder viewHolder) {
            FirebaseAuth mAuth = FirebaseAuth.getInstance();

            FirebaseUser mUser = mAuth.getCurrentUser();

            if(mUser == null){
                LogInBottomSheet logInBottomSheet = LogInBottomSheet.getInstance();
                logInBottomSheet.show(getSupportFragmentManager(),"main");
            }
            else {
                if (viewHolder.tvAdd.getText().toString().equals("Follow")) {
                    db_sqliteFollowing.Inserttoalertlist(itemArrayList_search.get(position).getUid());
                    viewHolder.tvAdd.setText("Unfollow");

                    adapter_search.setItemlist2(itemArrayList_search.get(position).getUid(),true);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/followings.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList_search.get(position).getUid());


                            return params;
                        }
                    };

                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }else {
                    db_sqliteFollowing.Delete(itemArrayList_search.get(position).uid);
                    viewHolder.tvAdd.setText("Follow");

                    adapter_search.setItemlist2(itemArrayList_search.get(position).getUid(),false);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://heyapp.in/unfollow.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {



                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("Uid",useridd);
                            params.put("Following", itemArrayList_search.get(position).getUid());


                            return params;
                        }
                    };
                    stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                            1*2000,
                            2,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
                    ));

                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringRequest);

                }


            }
        }

        @Override
        public void onProPicClicked(int position, PostItem item, ExoAdapter.CarsViewHolder holder) {

            if(useridd.equals(item.getUid())){
                Intent intent = new Intent(getApplicationContext(),Profile.class);
                startActivity(intent);

            }else {
                Intent intent = new Intent(getApplicationContext(),OtherUserProfile.class);
                intent.putExtra("uid",item.getUid());
                intent.putExtra("userName",item.getUsername());
                intent.putExtra("imgUrl",item.getPpurl());
                startActivity(intent);
            }
        }
    };

    public void jsonParse(){
        String url = "http://heyapp.in/showposts.php";

        progressBar.setVisibility(View.VISIBLE);

        System.out.println("rrrrrrrrrrrrrrrrrrr**************");
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        // response

                        itemArrayList_search.clear();

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+response);
                        try {
                            JSONObject obj = new JSONObject(response);
                            JSONArray array = obj.getJSONArray("eventss");

                            map.clear();
                            for(int i = 0 ; i < array.length();i++){
                                JSONObject object = array.getJSONObject(i);

                                //language
                                if(!object.getString("uid").isEmpty() && !object.getString("language").isEmpty()){
                                    String txt = object.getString("text");
                                    if(lang.equals(object.getString("language")) && txt.contains(search_text)){
                                        PostItem item = new PostItem(getApplicationContext());
                                        item.setUid(object.getString("uid"));
                                        item.setPostid(object.getString("postid"));
                                        item.setUsername(object.getString("username"));
                                        item.setTime(object.getString("time"));
                                        item.setImagelocation("https://static.heyapp.in/file/app-media/"+object.getString("imagelocation"));

                                        item.setText(object.getString("text"));
                                        item.setDownloadsno(object.getInt("downloads"));
                                        item.setLikes(object.getInt("likes"));
                                        item.setShares(object.getInt("shares"));
                                        item.setCommentsno(object.getInt("comments"));

                                        item.setViews(object.getInt("views"));



                                        String[] splitter =item.getTime().split("/");
                                        String time1 = splitter[0];
                                        String time2="";
                                        String time = "";

                                        // https:\/\/static.heyapp.in\/file\/app-media\/MArzuAxcATgdP3IuQkr2f248hL7220181114142546.jpg
                                        // https://static.heyapp.in/file/app-media/MArzuAxcATgdP3IuQkr2f248hL7220181114142546.jpg
                                        try {
                                            time2 = splitter[1];
                                            //time = time1.substring(0, time1.length() - 1);
                                        }catch (ArrayIndexOutOfBoundsException e){

                                        }

                                        if(item.getTime().contains("/")){
                                            time = time1.substring(0, time1.length() - 1);
                                            System.out.println("only  -------     " + object.getString("username")+""+time);

                                        }else {
                                            time = item.getTime();
                                        }
                                        System.out.println("only ff  ------->>>>>>>>     " + object.getString("username")+"   "+object.getString("language"));

                                        item.setVideoUrl("https://static.heyapp.in/file/app-media/"+time2);

                                        String temp = object.getString("ppurl");


                                        item.setPpurl(temp);

                                        int count2 = db_sqliteFavorites.get_check_List_Favorite(item.getPostid());
                                        if (count2 == 0) {
                                            item.setLiked(false);
                                        } else {
                                            item.setLiked(true);
                                        }

                                        if (item.getUid().equals(useridd)){

                                        }else {
                                            int count3 = db_sqliteFollowing.get_check_List_Favorite(item.getUid());
                                            if (count3 == 0) {
                                                item.setFollowing(false);
                                            } else {
                                                item.setFollowing(true);
                                            }
                                        }

                                        if(!item.getText().isEmpty()){
                                            String[] www = item.getText().split("==");// i converted string to String[] array


                                            String text = www[0];


                                            //System.out.println("only >> hash  Text          ^^^^^^^^^^^^^^    "+text);

                                            try{
                                                String hashes = www[1];
                                                System.out.println("only >> hash  ^^^^^^^^^^^^^^    "+hashes);
                                                if(!hashes.isEmpty()){
                                                    List<String> word_list=new ArrayList<String>();

                                                    String[] words = hashes.split("#");// i converted string to String[] array

                                                    System.out.println(words.length);
                                                    String fullText="";
                                                    for(int i1=0;i1<words.length;i1++){

                                                        if(!words[i1].isEmpty()){
                                                            word_list.add(words[i1]);
                                                        }

                                                    }


                                                    item.setWord_list(word_list);

                                                }
                                            }catch (ArrayIndexOutOfBoundsException e){
                                                //carsViewHolder.hashText.setVisibility(View.GONE);
                                                String hashes = text;
                                                //System.out.println("only >> hash  ^^^^^^^^^^^^^^    "+hashes);
                                                if(!hashes.isEmpty()){
                                                    List<String> word_list=new ArrayList<String>();

                                                    String[] words = hashes.split("#");// i converted string to String[] array

                                                    System.out.println(words.length);
                                                    String fullText="";
                                                    for(int i1=0;i1<words.length;i1++){
                                                        if(words[i1].startsWith("#")){

                                                            if(!words[i1].isEmpty()){
                                                                word_list.add(words[i1]);
                                                            }
                                                            String next = "<font color='#FC4C21'>"+words[i1]+"</font>";
                                                            fullText = fullText + next;
                                                        }
                                                        else {
                                                            fullText = fullText+words[i1];
                                                            if(!words[i1].isEmpty()){
                                                                word_list.add(words[i1]);
                                                            }
                                                        }

                                                    }


                                                    item.setWord_list(word_list);

                                                }
                                            }


                                            item.setText(text);


                                        }


                                        map.put(time,item);
                                    }
                                }



                            }

                            Set keys = map.keySet();
                            final Iterator it = keys.iterator();

                            System.out.println("only All keys are: " + keys);

                            //ArrayList<PostItem> aaaa = new ArrayList<>();

                            postJSON = new JSONArray();

                            while(it.hasNext()){
                                Object key = it.next();

                                System.out.println("only "+key + ": " + map.get(key).getUsername());

                                PostItem item = new PostItem(getApplicationContext());

                                item.setUid(map.get(key).getUid());

                                item.setPostid(map.get(key).getPostid());

                                item.setUsername(map.get(key).getUsername());

                                item.setTime(map.get(key).getTime());

                                item.setImagelocation(map.get(key).getImagelocation());

                                item.setVideoUrl(map.get(key).getVideoUrl());

                                item.setPpurl(map.get(key).getPpurl());

                                item.setText(map.get(key).getText());

                                item.setCommentsno(map.get(key).getCommentsno());

                                item.setDownloadsno(map.get(key).getDownloadsno());

                                item.setLikes(map.get(key).getLikes());

                                item.setShares(map.get(key).getShares());

                                item.setPlaying(false);

                                item.setFollowing(map.get(key).isFollowing());

                                item.setLiked(map.get(key).isLiked());

                                item.setWord_list(map.get(key).getWord_list());

                                item.setViews(map.get(key).getViews());

                                itemArrayList_search.add(item);
                                //aaaa.add(item);


                            }
                            Collections.shuffle(itemArrayList_search);

                            ArrayList<PostItem> aaaa = new ArrayList<>();

                            aaaa = itemArrayList_search;

                            for(int i = 0;i < itemArrayList_search.size() ;i++) {
                                //itemArrayList.add(aaaa.get(i));

                                JSONObject object = new JSONObject();
                                PostItem item = new PostItem(getApplicationContext());

                                item.setUsername(aaaa.get(i).getUsername());
                                object.put("userName",item.getUsername());

                                item.setUid(aaaa.get(i).getUid());
                                object.put("uid",item.getUid());

                                item.setPostid(aaaa.get(i).getPostid());
                                object.put("postid",item.getPostid());

                                item.setTime(aaaa.get(i).getTime());
                                object.put("time",item.getTime());

                                item.setImagelocation(aaaa.get(i).getImagelocation());
                                object.put("imgUrl",item.getImagelocation());

                                item.setVideoUrl(aaaa.get(i).getVideoUrl());
                                object.put("videoUrl",item.getVideoUrl());

                                item.setPpurl(aaaa.get(i).getPpurl());
                                object.put("ppurl",item.getPpurl());

                                item.setText(aaaa.get(i).getText());
                                object.put("text",item.getText());

                                item.setCommentsno(aaaa.get(i).getCommentsno());
                                object.put("comments",item.getCommentsno());

                                item.setDownloadsno(aaaa.get(i).getDownloadsno());
                                object.put("downloads",item.getDownloadsno());

                                item.setLikes(aaaa.get(i).getLikes());
                                object.put("likes",item.getLikes());

                                item.setShares(aaaa.get(i).getShares());
                                object.put("shares",item.getShares());

                                item.setFollowing(aaaa.get(i).isFollowing());
                                object.put("isFollowing",item.isFollowing());

                                item.setLiked(aaaa.get(i).isLiked());
                                object.put("isLiked",item.isLiked());

                                item.setViews(aaaa.get(i).getViews());
                                object.put("views",item.getViews());

                                postJSON.put(object);


                            }

                            mSwipeRefreshLayout.setRefreshing(false);

                            progressBar.setVisibility(View.GONE);


                            populate();


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Toast.makeText(emailAuth.this,response,Toast.LENGTH_LONG).show();
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        System.out.println("rrrrrrrrrrrrrrrrrrr**************"+error.toString());
                    }
                }
        ) {
            /*@Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("token", ACCESS_TOKEN);
                return params;
            }*/

            /*@Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("phone", phone);
                params.put("otp", otp);
                params.put("timestamp", timeStamp);
                params.put("fcm_token", fcm_token);


                return params;
            }*/
            @Override
            protected Response<String> parseNetworkResponse(NetworkResponse response) {
                //statusCode = response.statusCode;
                return super.parseNetworkResponse(response);
            }

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                1*2000,
                2,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));

        RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
        requestQueue.add(stringRequest);
    }


    class downloadvideooo extends AsyncTask<String,Integer,Void> {
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        String videoname;
        int pos;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Void doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                pos=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/SocialTest").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/SocialTest/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            Toast.makeText(getApplicationContext(), "Saved", Toast.LENGTH_SHORT).show();
            circleProgressBar.setVisibility(View.INVISIBLE);
            linearLayout.setVisibility(View.VISIBLE);





            int down = itemArrayList_search.get(pos).getDownloadsno();
            itemArrayList_search.get(pos).setDownloadsno(down+1);
            adapter_search.setItemlist(itemArrayList_search);

            StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/countdownloads.php",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {




                        }
                    }

                    , new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            }) {

                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    Map<String, String> params = new HashMap<String, String>();

                    params.put("postid",itemArrayList_search.get(pos).postid);

                    return params;
                }
            };

            RequestQueue requestQueue= Volley.newRequestQueue(getApplicationContext());
            requestQueue.add(stringrequest);


//            ddd ddd=new ddd();
//            ddd.setdownloadlayout(linearLayout);
//            ddd.setProgressbar(circleProgressBar);
//            ddd.execute(videoname);


        }
    }

    class downloadvideoWhatsApp extends AsyncTask<String,Integer,Boolean> {

        int positionn;
        String videoname;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
            progresss=0;
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                positionn=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");
//                urlConnection.setDoOutput(true);
                urlConnection.setRequestProperty("Accept-Encoding","identity");

                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/SocialTest").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/SocialTest/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;


                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);

                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){


                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/SocialTest/" +videoname);

                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setPackage("com.whatsapp");

                if (!itemArrayList_search.get(positionn).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_search.get(positionn).getText()+"\n"+whatsAppText);
                }
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

                try {
                    startActivity(shareIntent);
                    int share = itemArrayList_search.get(positionn).getShares();
                    itemArrayList_search.get(positionn).setShares(share+1);

                    adapter_search.setItemlist(itemArrayList_search);

                    StringRequest stringrequest = new StringRequest(Request.Method.POST, "http://heyapp.in/socialshares.php",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {


                                }
                            }

                            , new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }) {

                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {

                            Map<String, String> params = new HashMap<String, String>();
                            params.put("postid", itemArrayList_search.get(positionn).getPostid());

                            return params;
                        }
                    };
                    RequestQueue requestQueue = Volley.newRequestQueue(getApplicationContext());
                    requestQueue.add(stringrequest);
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(getApplicationContext(), "Whatsapp have not been installed", Toast.LENGTH_SHORT).show();
                }






                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.linearLayout.setVisibility(View.VISIBLE);

            }






        }


    }

    class downloadvideoshare extends AsyncTask<String,Integer,Boolean> {

        String videoname;
        int position;
        ProgressBar circleProgressBar;
        LinearLayout linearLayout;
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.circleProgressBar.setVisibility(View.VISIBLE);
            this.circleProgressBar.setProgress(0);
            this.circleProgressBar.setSecondaryProgress(100);
            this.circleProgressBar.setMax(100);
            this.linearLayout.setVisibility(View.INVISIBLE);
        }

        public void  setProgressbar(ProgressBar circleProgressBar){
            this.circleProgressBar=circleProgressBar;
        }

        public void setdownloadlayout(LinearLayout linearLayout){
            this.linearLayout=linearLayout;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            this.circleProgressBar.setProgress(values[0]);

        }

        @Override
        protected Boolean doInBackground(String... string) {

            try {
                String uri=string[0];
                videoname=string[1];
                position=Integer.parseInt(string[2]);
                URL url = new URL(uri);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                urlConnection.setRequestMethod("GET");

                urlConnection.setRequestProperty("Accept-Encoding","identity");
                //connect
                urlConnection.connect();

                //set the path where we want to save the file

                //create a new file, to save the downloaded file


                File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
                new File(file + "/SocialTest").mkdirs();


                FileOutputStream fileOutput = new FileOutputStream(file+ "/SocialTest/" +videoname);

                //Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                //this is the total size of the file which we are downloading
                int totalSize = urlConnection.getContentLength();


                //create a buffer...
                byte[] buffer = new byte[1024];
                int bufferLength = 0;
                long downloadedsize=0;

                while ((bufferLength=inputStream.read(buffer))!=-1){
                    downloadedsize += bufferLength;
                    progresss=(int)(downloadedsize*100/totalSize);
                    publishProgress(progresss);
                    fileOutput.write(buffer, 0, bufferLength);
                }
                //close the output stream when complete //
                fileOutput.close();

                return true;

            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
            catch (Exception e) {
                return false;
            }

        }

        @Override
        protected void onPostExecute(Boolean success) {

            if (success){



                File file=new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/SocialTest/" +videoname);
                Uri uri=Uri.fromFile(file);
                StrictMode.VmPolicy.Builder builder=new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
                Intent shareIntent = new Intent();
                if (!itemArrayList_search.get(position).getText().equals("")) {
                    shareIntent.putExtra(Intent.EXTRA_TEXT, itemArrayList_search.get(position).getText()+"\n"+whatsAppText);
                }
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
                shareIntent.setType("*/*");
                shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                startActivity(Intent.createChooser(shareIntent, "Share Using"));

                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);


            }else {
                Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                this.circleProgressBar.setVisibility(View.INVISIBLE);
                this.linearLayout.setVisibility(View.VISIBLE);
            }






        }


    }

}
